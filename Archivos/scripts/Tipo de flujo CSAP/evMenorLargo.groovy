import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade
@WithPlugin("com.atlassian.servicedesk")

//usuario que ejecuta las modificaciones
def userManager = ComponentAccessor.getUserManager()
def currentUser = userManager.getUserByName("admin");

def issueManager = ComponentAccessor.getIssueManager()
//def issue = ComponentAccessor.getIssueManager().getIssueObject("CRDXAMSSAP-1")
def issueType = issue.getIssueType().getName()
def currentProject = issue.projectObject.name

def flujoevomenor
def proyecto

if(currentProject == "IBM/Softinsa - Colaboraciones" || currentProject == "Preventa - Colaboraciones" || currentProject == "SAP-MTO.LICENCIAS" || currentProject == "VN - Colaboraciones"){
    proyectoIN = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Proyecto").getValue(issue)
    proyecto = proyectoIN
}else{
    proyecto = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = \"Proyecto\" AND \"Name\" = \"" + currentProject + "\" ")
}

if(proyecto[0] != null){
    log.warn("El proyecto es: " + proyecto[0])
    log.warn("La issue es de tipo: " + issueType)
    flujoevomenor = objectFacade.loadObjectAttributeBean(proyecto[0].getId(), 17686)    	
    if(flujoevomenor!=null){
        flujoevomenor = flujoevomenor.getObjectAttributeValueBeans()[0];
        flujoevomenor = flujoevomenor.getValue()
        log.warn("El flujo de Soporte es de tipo: " + flujoevomenor)
        if(flujoevomenor == "Flujo largo"){
            log.warn("TRUE -- La issue es de tipo: " + issueType + " y el flujo de Evolutivo menor es de tipo: " + flujoevomenor)
            passesCondition = true
        }else{
            log.warn("FALSE -- La issue es de tipo: " + issueType + " y el flujo de Evolutivo menor es de tipo: " + flujoevomenor)
            passesCondition = false
        }
    }
    
}