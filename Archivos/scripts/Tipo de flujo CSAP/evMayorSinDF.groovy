import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade
@WithPlugin("com.atlassian.servicedesk")

//usuario que ejecuta las modificaciones
def userManager = ComponentAccessor.getUserManager()
def currentUser = userManager.getUserByName("admin");

def issueManager = ComponentAccessor.getIssueManager()
//def issue = ComponentAccessor.getIssueManager().getIssueObject("CRDXAMSSAP-1")
def issueType = issue.getIssueType().getName()
def currentProject = issue.projectObject.name

def flujoevomayor
def proyecto

if(currentProject == "IBM/Softinsa - Colaboraciones" || currentProject == "Preventa - Colaboraciones" || currentProject == "SAP-MTO.LICENCIAS" || currentProject == "VN - Colaboraciones"){
    proyectoIN = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Proyecto").getValue(issue)
    proyecto = proyectoIN
}else{
    proyecto = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = \"Proyecto\" AND \"Name\" = \"" + currentProject + "\" ")
}

if(proyecto[0] != null){
    log.warn("El proyecto es: " + proyecto[0])
    log.warn("La issue es de tipo: " + issueType)
    flujoevomayor = objectFacade.loadObjectAttributeBean(proyecto[0].getId(), 17687)    	
    if(flujoevomayor!=null){
        flujoevomayor = flujoevomayor.getObjectAttributeValueBeans()[0];
        flujoevomayor = flujoevomayor.getValue()
        log.warn("El flujo de Evolutivo Mayor es de tipo: " + flujoevomayor)
        if(flujoevomayor == "No requiere DF/DP"){
            log.warn("TRUE -- La issue es de tipo: " + issueType + " y el flujo de Evolutivo Mayor es de tipo: " + flujoevomayor)
            passesCondition = true
        }else{
            log.warn("FALSE -- La issue es de tipo: " + issueType + " y el flujo de Evolutivo Mayor es de tipo: " + flujoevomayor)
            passesCondition = false
        }
    }
    
}