import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.event.type.EventDispatchOption

def workflow = ComponentAccessor.getWorkflowManager().getWorkflow(issue)

def issueManager = ComponentAccessor.getIssueManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def mi = (MutableIssue) issue
def userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");

//log.warn("Current action name: $actionName")

def solProp = customFieldManager.getCustomFieldObject(11839)
mi.setCustomFieldValue(solProp, null)
def CausaOrigen = customFieldManager.getCustomFieldObject(12202)
mi.setCustomFieldValue(CausaOrigen, null)
issueManager.updateIssue(admin, mi, EventDispatchOption.DO_NOT_DISPATCH, false)