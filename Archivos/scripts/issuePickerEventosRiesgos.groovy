import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.link.IssueLinkType
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.link.LinkCollection
import com.atlassian.jira.issue.link.IssueLink;
@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade
@WithPlugin("com.atlassian.servicedesk")

def issue = (MutableIssue) issue
def issueActual = issue
def userManager = ComponentAccessor.getUserManager()
def currentUser = userManager.getUserByName("admin");
def issueManager = ComponentAccessor.getIssueManager()
IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager();
def issueService = ComponentAccessor.getIssueService();
def tipoIssue = issue.getIssueType().getId()
def eventosRiesgos
def issueDestino
def issues = []
def issueEnlazada
//campos de cada objeto
def plan = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Plan de acción").getValue(issue);
def planCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Plan de acción")
def eventosRiesgosCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Eventos/Riesgos")
if(plan != null){
    issueDestino = (MutableIssue) ComponentAccessor.getIssueManager().getIssueObject(plan.toString())
    eventosRiesgos = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Eventos/Riesgos").getValue(issueDestino);    
}else{
    LinkCollection links = issueLinkManager.getLinkCollection(issue, currentUser);
    def linkList =links.getOutwardIssues("Relates");
    if(linkList != null){
       log.warn("los links son" + linkList)
        if(linkList[0] != null){
            log.warn(linkList[0])
            issueEnlazada = linkList[0]
            issueDestino = (MutableIssue) linkList[0]
            eventosRiesgos = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Eventos/Riesgos").getValue(issueDestino);
            issues.add(issue)
            issueDestino.setCustomFieldValue(eventosRiesgosCF,issues)
            issue.setCustomFieldValue(planCF,linkList[0])
            issueManager.updateIssue(currentUser, issue, EventDispatchOption.ISSUE_UPDATED, false)
            issueManager.updateIssue(currentUser, issueDestino, EventDispatchOption.ISSUE_UPDATED, false)
        }
    }else{
        log.warn("no esta cogiendo los links") 
    }
}



if (eventosRiesgos == null){
    eventosRiesgos = issues
}else{

    eventosRiesgos.add(issueActual)
    //eventosRiesgos.add(issueEnlazada)
    log.warn("El plan es: " + plan)
    log.warn("la issue actual es: " + issueActual)
    log.warn("la issue destino es: " + issueDestino)
    log.warn("El tipoIssue es: " + tipoIssue)

    //log.warn(issues)
    issueDestino.setCustomFieldValue(eventosRiesgosCF,eventosRiesgos)
    issueManager.updateIssue(currentUser, issueDestino, EventDispatchOption.ISSUE_UPDATED, false)    
}