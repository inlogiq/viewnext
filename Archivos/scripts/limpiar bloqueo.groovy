import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.event.type.EventDispatchOption

def workflow = ComponentAccessor.getWorkflowManager().getWorkflow(issue)

def issueManager = ComponentAccessor.getIssueManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def mi = (MutableIssue) issue
def userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");

//log.warn("Current action name: $actionName")

def tipoBloq = customFieldManager.getCustomFieldObject(11831)
mi.setCustomFieldValue(tipoBloq, null)
issueManager.updateIssue(admin, mi, EventDispatchOption.DO_NOT_DISPATCH, false)