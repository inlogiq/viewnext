import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.event.type.EventDispatchOption;
def issueManager = ComponentAccessor.getIssueManager()
def mi = (MutableIssue) issue
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def cField = customFieldManager.getCustomFieldObjectByName("Rechazos")
def cFieldValue = issue.getCustomFieldValue(cField)
log.warn("ANTES: " + cFieldValue)
if(cFieldValue==null){
    cFieldValue = (Double) 1;
}else{
    cFieldValue = cFieldValue + 1
}
log.warn("DESPUES: " + cFieldValue)
mi.setCustomFieldValue(cField, cFieldValue)
issueManager.updateIssue(user, mi, EventDispatchOption.DO_NOT_DISPATCH, false)