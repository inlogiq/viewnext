import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.util.json.JSONObject;
import java.io.*;
import java.util.*;
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.user.util.*;
import java.util.Arrays;
//jql
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.Issue; 
import java.lang.Math;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.Timestamp

def issueManager = ComponentAccessor.getIssueManager()

UserManager userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");
//cambiar la ruta
def ruta= "/var/atlassian/application-data/jira/import"

def file = new File(ruta).path;
def convert = new File(file,"SlaGBAbiertos.JSON")
String json;

convert.eachLine {  
    line -> json= "$line";
}

//Campo para sabaer si la incencia es de migración
//def TicketMigrado=ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13212') 
//********cambiar esto por el nombre que este definido en el JSON de SLA para los nuevos clientes.
//SARAS
//def NombreSlaRechazados="SARAS - Solicitudes Rechazadas"
//GB
def NombreSlaRechazados="GB - Solicitudes Rechazadas"
//obtenemos el campo Rechazos
def Rechazados= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_10900')
double TotalRechazos=1
//***********************FIN BLOQUE RECHAZADOS************************************************


def CadenaJsonCode = new JSONObject(json);
//SARAS
//def json_array= CadenaJsonCode.getJSONArray("_--30155SLA_--20Tickets_--20Abiertos2_json")
//GB
def json_array= CadenaJsonCode.getJSONArray("File_SLA")
int si=0
int no=0
String[] solicitud= new String[json_array.length()];
String[] semaforo= new String[json_array.length()];
String[] SLA_Texto= new String[json_array.length()];
Double[] SLA_Tiempo= new Double[json_array.length()];
String[] SLA_FechaIni= new String[json_array.length()];
String[] SLA_FechaFin= new String[json_array.length()];

for(int i=0; i<json_array.length();i++){
   
    solicitud[i]=json_array.get(i)["SOLICITUD"]
    semaforo[i]=json_array.get(i)["SEMAFORO"]
    SLA_Texto[i]=json_array.get(i)["SLA_TEXTO"]
    SLA_Tiempo[i]=json_array.get(i)["TIEMPO"]
    SLA_FechaIni[i]=json_array.get(i)["FECHA_INI"]
    SLA_FechaFin[i]=json_array.get(i)["FECHA_FIN"]
}
String[] total_key= new String[solicitud.size()];
String[] existe= new String[solicitud.size()];

for(int g=0;g<solicitud.size();g++){
    int total=0 
    String semaforos=""
    if(existe.size()==0){
        for(int k=0;k<solicitud.size();k++){
            if(solicitud[k]==solicitud[g]){
                total++
                if(semaforo[k]==""){
                    semaforos+="EnCurso_"+SLA_Tiempo[k]+"_"+SLA_Texto[k]+"_"+SLA_FechaIni[k]+"_"+SLA_FechaFin[k]+"!"
                }else{
                    semaforos+=semaforo[k]+"_"+SLA_Tiempo[k]+"_"+SLA_Texto[k]+"_"+SLA_FechaIni[k]+"_"+SLA_FechaFin[k]+"!"
                }
                
            }
        }
        total_key[g]=solicitud[g]+"!"+total+"!"+semaforo[g]
        existe[g]=solicitud[g]
    }else{
        int cont=0
        for(int f=0;f<existe.size();f++){
            if(solicitud[g]==existe[f]){
                cont++
                if(semaforo[f]==""){
                    semaforos+="EnCurso_"+SLA_Tiempo[f]+"_"+SLA_Texto[f]+"_"+SLA_FechaIni[f]+"_"+SLA_FechaFin[f]+"!"
                }else{
                    semaforos+=semaforo[f]+"_"+SLA_Tiempo[f]+"_"+SLA_Texto[f]+"_"+SLA_FechaIni[f]+"_"+SLA_FechaFin[f]+"!"
                }
                
            }
        }
        if(cont==0){
            for(int k=0;k<solicitud.size();k++){
                if(solicitud[k]==solicitud[g]){
                    total++
                    if(semaforo[k]==""){
                        semaforos+="EnCurso_"+SLA_Tiempo[k]+"_"+SLA_Texto[k]+"_"+SLA_FechaIni[k]+"_"+SLA_FechaFin[k]+"!"
                    }else{
                        semaforos+=semaforo[k]+"_"+SLA_Tiempo[k]+"_"+SLA_Texto[k]+"_"+SLA_FechaIni[k]+"_"+SLA_FechaFin[k]+"!"
                    }
                }
            }
            total_key[g]=solicitud[g]+"!"+total+"!"+semaforos
            existe[g]=solicitud[g]
        }
        
    }
        
}

int keys=0;
String[] solicitud_key= new String[solicitud.size()];
for(int t=0;t<total_key.size();t++){
    if(total_key[t]!=null){
        solicitud_key[t-keys]=total_key[t]
    }else{
        keys++
    }
   
}
for(int c=0;c<solicitud_key.size();c++){
    
    if(solicitud_key[c]!=null){
        def valor= solicitud_key[c].split("!")
        int totalIterar = Integer.parseInt(valor[1]);
        final String jqlSearch = "project ='GBAMSSAP' AND 'Código ZEUS'~'"+valor[0]+"'"
        def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
        def searchService = ComponentAccessor.getComponentOfType(SearchService)
        SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
        def ComponentIssue= ComponentAccessor.getIssueManager()
        if (parseResult.isValid()) {
            try {
            
                def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
                def issues = results.results    
                
                issues.each {
                    Issue issue= ComponentIssue.getIssueObject(it.key)
                    issueManager = ComponentAccessor.getIssueManager()
                    def issueObject = issueManager.getIssueObject(issue.toString())
                    
                    def total_sla= totalSla(valor[0],solicitud_key)
                    int cont_t=1
                    int sla_si=1
                    int sla_no=1
                    int sla_encurso=1
                    for(int i=0;i<totalIterar;i++){
                        
                        if(i==0){
                            def Texto= valor[2].split("_")
                            
                            if(Texto[2]!=NombreSlaRechazados){                                
                                
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso NO"),"0m 1s");
                                	//guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="SI"){
                                    if(Texto[1]=="0.0"){
                                    	issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),formatearHoras(Texto[1]));
                                    }
                                    //guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="EnCurso"){                             
                                    if(Texto[1]=="0.0"){
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),formatearHoras(Texto[1]));
                                    }
                                    
                              }
                                //consulto que la fecha no sea 0000-00-00
                                /*if(Texto[3]!="0000-00-00" && Texto[0]=="EnCurso"){
                                    
                                    //Establezo la fecha de inicio del SLA para cuando esta en curso
                                    def NombreCampo=fechaIniEncursoCampo(Texto[2])
                                    def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                    def FechaFile= FormatoFecha(Texto[3])
                                    def FechaSeteada= new Timestamp(FechaFile.getTime())
                                    issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                }*/
                             	
                            //RECHAZADOS
                            }else{
                                
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                                
                            }
                            
                        }
                        if(i==1){
                            def Texto= valor[3].split("_")
                            if(Texto[2]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso NO"),"0h");
                                	//guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="SI"){     
                                    if(Texto[1]=="0.0"){
                                    	issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),formatearHoras(Texto[1]));
                                    }
                                    
                                	//guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="EnCurso"){                             
                                    if(Texto[1]=="0.0"){
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),formatearHoras(Texto[1]));
                                    }                                    
                                }
                                //consulto que la fecha no sea 0000-00-00
                                /*if(Texto[3]!="0000-00-00" && Texto[0]=="EnCurso"){
                                    //Establezo la fecha de inicio del SLA para cuando esta en curso
                                    def NombreCampo=fechaIniEncursoCampo(Texto[2])
                                    def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                    def FechaFile= FormatoFecha(Texto[3])
                                    def FechaSeteada= new Timestamp(FechaFile.getTime())
                                    issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                }*/
                            	
                            //RECHAZOS
                            }else{
                                
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                                
                            }
                        }
                        if(i==2){
                            def Texto= valor[4].split("_")
                            if(Texto[2]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso NO"),"0m 1s");
                                	/*if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="SI"){  
                                    if(Texto[1]=="0.0"){
                                    	issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),formatearHoras(Texto[1]));
                                    }
                                    
                                	//guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="EnCurso"){                             
                                    if(Texto[1]=="0.0"){
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),formatearHoras(Texto[1]));
                                    }
                                    
                                }
                                //consulto que la fecha no sea 0000-00-00
                                /*if(Texto[3]!="0000-00-00" && Texto[0]=="EnCurso"){
                                    //Establezo la fecha de inicio del SLA para cuando esta en curso
                                    def NombreCampo=fechaIniEncursoCampo(Texto[2])
                                    def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                    def FechaFile= FormatoFecha(Texto[3])
                                    def FechaSeteada= new Timestamp(FechaFile.getTime())
                                    issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                }*/
                             	
                            //RECHAZOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        if(i==3){
                            def Texto= valor[5].split("_")
                            if(Texto[2]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso NO"),"0m 1s");
                                	//guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="SI"){  
                                    if(Texto[1]=="0.0"){
                                    	issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),formatearHoras(Texto[1]));
                                    }
                                    
									//guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="EnCurso"){                             
                                    if(Texto[1]=="0.0"){
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),formatearHoras(Texto[1]));
                                    }                                    
                                }
                                //consulto que la fecha no sea 0000-00-00
                               /* if(Texto[3]!="0000-00-00" && Texto[0]=="EnCurso"){
                                    //Establezo la fecha de inicio del SLA para cuando esta en curso
                                    def NombreCampo=fechaIniEncursoCampo(Texto[2])
                                    def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                    def FechaFile= FormatoFecha(Texto[3])
                                    def FechaSeteada= new Timestamp(FechaFile.getTime())
                                    issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                }*/
                                
                            //RECHAZOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        if(i==4){
                            def Texto= valor[6].split("_")
                            if(Texto[2]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso NO"),"0m 1s");
                                	//guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="NO"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="SI"){                                    
                                    
									if(Texto[1]=="0.0"){
                                    	issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso SI"),formatearHoras(Texto[1]));
                                    }
                                    //guardo la fecha de inicio del mismo
                                    /*if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaIniEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[3])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }
                                    //guardo la fecha de fin del mismo
                                    if(Texto[3]!="0000-00-00" && Texto[0]=="SI"){
                                        //Establezo la fecha de inicio del SLA para cuando esta en curso
                                        def NombreCampo=fechaFinEncursoSIoNOCampo(Texto[2])
                                        def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                        def FechaFile= FormatoFecha(Texto[4])
                                        def FechaSeteada= new Timestamp(FechaFile.getTime())
                                        issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                    }*/
                                }
                                if(Texto[0]=="EnCurso"){
                                    if(Texto[1]=="0.0"){
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),"0m 1s");
                                    }else{
                                        issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[2]+" EnCurso"),formatearHoras(Texto[1]));
                                    }

                                }
                                //consulto que la fecha no sea 0000-00-00
                                /*if(Texto[3]!="0000-00-00" && Texto[0]=="EnCurso"){
                                    //Establezo la fecha de inicio del SLA para cuando esta en curso
                                    def NombreCampo=fechaIniEncursoCampo(Texto[2])
                                    def CampoFecha=ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(NombreCampo)
                                    def FechaFile= FormatoFecha(Texto[3])
                                    def FechaSeteada= new Timestamp(FechaFile.getTime())
                                    issue.setCustomFieldValue(CampoFecha,FechaSeteada)
                                }*/
                                
                            //RECHAZOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        //issue.setCustomFieldValue(TicketMigrado,"SI");
                        ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    }
                        
               }
           
            } catch (SearchException e) {
                log.warn(e.printStackTrace())
            }
        } else {
        
            return null
        }
        
        
    }
}

public String formatearHoras(String time){
    
    def valor= time.split("\\.")    
    if(valor.size()>1){
        int min = Integer.parseInt(valor[1]);
        log.warn('min '+min)
        if(min>0){
            BigDecimal multiplicacion= Math.pow(10, valor[1].size());
            def minutos = (min*60)/multiplicacion
            return valor[0]+"h"+" "+Math.round(minutos)+"m"
        }else{

            return valor[0]+"h";
        }
    }else{
        return time+"h";
    }
}
//ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
public String totalSla(String solicitud,String[] sla){
   
    for(int p=0;p<sla.size();p++){
        def valor= sla[p].split("!")        
        if(solicitud==valor[0]){
            return valor[1]
        }
        
    }
    return 0
    
}

public Date FormatoFecha(String fecha){
    
    SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    Date fechaSeteada = objSDF.parse(fecha);  
     
    return fechaSeteada
}
public String fechaIniEncursoCampo(String indicador){
    
    
    return "FechaSlaMigrado"
    
    
}
public String fechaFinEncursoSIoNOCampo(String indicador){
    
    //se comenta la forma como se estaba tomando el campo fecha de inicio en el sla. Esto porque ahora el inicio de los sla no dependerá de la fecha ini enviada en el json, si no de la fecha y hora de carga de cada ticket migrado.
    String campo
    def valor= indicador.split("-")
    switch(cleanTextContent(valor[1])){
        
        case "Entrega de solicitud":
        	campo= "Migración Fin Entrega de solicitud"
        break
        
        case "Entrega de valoración y planific":
        	campo= "Migración Fin Entrega de valoración y planific"
        break
        
        case "Tiempo de resolución":
        	campo= "Migración Fin Tiempo de resolución"
        break
        
        case "Tiempo de respuesta":
        	campo= "Migración Fin Tiempo de respuesta"
        break
        
        case "Entrega DF/DP":
        	campo= "Migración Fin Entrega DF/DP"
        break
        
        default:
            campo= "Migración Fin Entrega de solicitud"
        
    }
    return campo
}
public String fechaIniEncursoSIoNOCampo(String indicador){
    String campo
    def valor= indicador.split("-")
    switch(cleanTextContent(valor[1])){
        
        case "Entrega de solicitud":
        	campo= "Migración Entrega de solicitud"
        break
        
        case "Entrega de valoración y planific":
        	campo= "Migración Entrega de valoración y planific"
        break
        
        case "Tiempo de resolución":
        	campo= "Migración Tiempo de resolución"
        break
        
        case "Tiempo de respuesta":
        	campo= "Migración Tiempo de respuesta"
        break
        
        case "Entrega DF/DP":
        	campo= "Migración Entrega DF/DP"
        break
        
        default:
            campo= "Migración Entrega de solicitud"
        
    }
    return campo
}

public String cleanTextContent(String text){
    
    text = text.replaceAll("á", "a");
    text = text.replaceAll("Á", "A");    
    text = text.replaceAll("é", "e");
    text = text.replaceAll("É", "E");
    text = text.replaceAll("í", "i");
    text = text.replaceAll("Í", "I");
    text = text.replaceAll("ó", "o");
    text = text.replaceAll("Ó", "O");
    text = text.replaceAll("ú", "u");
    text = text.replaceAll("Ú", "U");
    text = text.replaceAll("ñ", "n");
    text = text.replaceAll("Ñ", "N");
    text = text.replaceAll("\"", "'");
    text = text.replaceAll("\\\\", "/");

    // strips off all non-ASCII characters
    text = text.replaceAll("[^\\x00-\\x7F]", "");
 
    // erases all the ASCII control characters
    text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");
     
    // removes non-printable characters from Unicode
    text = text.replaceAll("\\p{C}", "");

    return text.trim();
}
