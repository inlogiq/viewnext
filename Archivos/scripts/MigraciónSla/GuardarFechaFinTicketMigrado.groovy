import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.util.json.JSONObject;
import java.io.*;
import java.util.*;
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.user.util.*;
import java.util.Arrays;
//jql
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.Issue; 
import java.lang.Math;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.Timestamp

def issue = issue
def issueManager = ComponentAccessor.getIssueManager()
UserManager userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");
def CodZeus = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Código ZEUS").getValue(issue);
log.warn("Codigo zeus "+CodZeus)
if(CodZeus!=null){
    Date fecha= new Date()
    def dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
    def fechaFinalSeteada= dateFormat.format(fecha)    
    def FechaActual=FormatoFecha(fechaFinalSeteada.toString())
    //def FechaActual=FormatoFecha("2021-02-18")
    def FechaSeteada= new Timestamp(FechaActual.getTime())    
    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("FechaSlaMigradoFin"),FechaSeteada);
    ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
}

                    
public Date FormatoFecha(String fecha){
    
    SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    Date fechaSeteada = objSDF.parse(fecha+" 19:00:00");  
     
    return fechaSeteada
}