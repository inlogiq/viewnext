import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.util.json.JSONObject;
import java.io.*;
import java.util.*;
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.user.util.*;
//jql
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.IssueManager;
import java.lang.Math;

def issueManager = ComponentAccessor.getIssueManager()
//def issueObject = issueManager.getIssueObject("N3-22")

//MutableIssue issue = issueObject
//reemplazar por el campo de semaforo de la instancia de PRE
def semaforoJira= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_12223');
//sla SI CUMPLEN
def tts1_si= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_12230');
def tts1_si2= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13105');
def tts1_si3= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13106');
def tts1_si4= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13107');
def tts1_si5= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13108');

//saras
//def sarasMig1= ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName('Migracion Entrega de solicitud NO');

//SLA NO CUMPLEN
def tts2_no= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_12231');
def tts2_no2= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13101');
def tts2_no3= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13102');
def tts2_no4= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13103');
def tts2_no5= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13104');

UserManager userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("inlogiq");
//cambiar la ruta
def ruta= "/var/atlassian/application-data/jira/import"
//def ruta= "/home/dpradapa"

def file = new File(ruta).path;
def convert = new File(file,"0155SLA_Tickets_CerradosUltimo.JSON")
def json;
convert.eachLine {  
    line -> json= "$line";
}
//Campo para sabaer si la incencia es de migración
def TicketMigrado=ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13212')
//********cambiar esto por el nombre que este definido en el JSON de SLA para los nuevos clientes.
def NombreSlaRechazados="SARAS - Solicitudes Rechazadas"
//obtenemos el campo Rechazos
def Rechazados= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_10900')
double TotalRechazos=1
//***********************FIN BLOQUE RECHAZADOS************************************************

def CadenaJsonCode = new JSONObject(json);
def json_array= CadenaJsonCode.getJSONArray("SLA")

int si=0
int no=0
String[] solicitud= new String[json_array.length()];
String[] semaforo= new String[json_array.length()];
String[] SLA_Texto= new String[json_array.length()];
for(int i=0; i<json_array.length();i++){
   
    solicitud[i]=json_array.get(i)["SOLICITUD"]
    semaforo[i]=json_array.get(i)["SEMAFORO"]
    SLA_Texto[i]=json_array.get(i)["SLA_TEXTO"]
}
String[] total_key= new String[solicitud.size()];
String[] existe= new String[solicitud.size()];

for(int g=0;g<solicitud.size();g++){
    //log.warn("Solicitud "+solicitud[g])
    int total=0
    String semaforos=""
    if(existe.size()==0){
        for(int k=0;k<solicitud.size();k++){
            if(solicitud[k]==solicitud[g]){
                total++
                semaforos+=semaforo[k]+"_"+SLA_Texto[k]+"!"
            }
        }
        total_key[g]=solicitud[g]+"!"+total+"!"+semaforo[g]
        existe[g]=solicitud[g]
    }else{
        int cont=0
        for(int f=0;f<existe.size();f++){
            if(solicitud[g]==existe[f]){
                cont++
                semaforos+=semaforo[f]+"_"+SLA_Texto[f]+"!"
            }
        }
        if(cont==0){
            for(int k=0;k<solicitud.size();k++){
                if(solicitud[k]==solicitud[g]){
                    total++
                    semaforos+=semaforo[k]+"_"+SLA_Texto[k]+"!"
                }
            }
            total_key[g]=solicitud[g]+"!"+total+"!"+semaforos
            existe[g]=solicitud[g]
        }
        
    }
        
}
int keys=0;
String[] solicitud_key= new String[solicitud.size()];
for(int t=0;t<total_key.size();t++){
    if(total_key[t]!=null){
        solicitud_key[t-keys]=total_key[t]
    }else{
        keys++
    }
   
}

for(int c=0;c<solicitud_key.size();c++){
    
    if(solicitud_key[c]!=null){
        def valor= solicitud_key[c].split("!")        
        int totalIterar = Integer.parseInt(valor[1]); 
        final String jqlSearch = "project ='SAMSSAP' AND 'Código ZEUS'~'"+valor[0]+"'"
        def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
        def searchService = ComponentAccessor.getComponentOfType(SearchService)
        SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
        def ComponentIssue= ComponentAccessor.getIssueManager()
        if (parseResult.isValid()) {
            try {
            
                def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
                def issues = results.results 
                
                issues.each {
                    Issue issue= ComponentIssue.getIssueObject(it.key)
                    issueManager = ComponentAccessor.getIssueManager()
                    def issueObject = issueManager.getIssueObject(issue.toString())
                    
                    def total_sla= totalSla(valor[0],solicitud_key)
                    int cont_t=1                    
                   
                    String [] sumaTotal= new String[json_array.length()]
                    int[] totales= new int[json_array.length()]
                    
                    for(int i=0;i<totalIterar;i++){
                        
                        if(i==0){
                            def Texto= valor[2].split("_")
                            if(Texto[1]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" NO"),"0m 1s");
                                }else{                                
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" SI"),"10d");
                                    //issue.setCustomFieldValue(tts1_si,"1000d");
                                }
                            //RECHAZADOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        if(i==1){
                            def Texto= valor[3].split("_")
                            if(Texto[1]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" NO"),"0m 1s");
                                    //issue.setCustomFieldValue(tts2_no2,"0h");
                                }else{                                
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" SI"),"10d");
                                    //issue.setCustomFieldValue(tts1_si2,"1000d");
                                }
                            //RECHAZADOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        if(i==2){
                            def Texto= valor[4].split("_")
                            if(Texto[1]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" NO"),"0m 1s");
                                    //issue.setCustomFieldValue(tts2_no3,"0h");
                                }else{
                                    //issue.setCustomFieldValue(tts1_si3,"1000d");
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" SI"),"10d");
                                }
                            //RECHAZADOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        if(i==3){
                            def Texto= valor[5].split("_")
                            if(Texto[1]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" NO"),"0m 1s");
                                    //issue.setCustomFieldValue(tts2_no4,"0h");
                                }else{
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" SI"),"10d");
                                    //issue.setCustomFieldValue(tts1_si4,"1000d");
                                }
                            //RECHAZADOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        if(i==4){
                            def Texto= valor[6].split("_")
                            if(Texto[1]!=NombreSlaRechazados){
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" NO"),"0m 1s");
                                    //issue.setCustomFieldValue(tts2_no4,"0h");
                                }else{
                                    issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName('Migración '+Texto[1]+" SI"),"10d");
                                    //issue.setCustomFieldValue(tts1_si4,"1000d");
                                }
                            //RECHAZADOS
                            }else{
                                if(Texto[0]=="NO"){                             
                                    issue.setCustomFieldValue(Rechazados,TotalRechazos);
                                }
                            }
                        }
                        //para los SLA cerrados
                        issue.setCustomFieldValue(TicketMigrado,"SI");
                        ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
                    }
                    
                  }
       
           
            } catch (e) {
                log.warn(e.printStackTrace())
            }
        } else {
        
            return null
        }
        
        
    }
}

public String formatearHoras(String time){
    
    def valor= time.split("\\.")    
    if(valor.size()>1){
        int min = Integer.parseInt(valor[1]);
        log.warn('min '+min)
        if(min>0){
            BigDecimal multiplicacion= Math.pow(10, valor[1].size());
            def minutos = (min*60)/multiplicacion
            return valor[0]+"h"+" "+Math.round(minutos)+"m"
        }else{

            return valor[0]+"h";
        }
    }else{
        return time+"h";
    }
}
public String totalSla(String solicitud,String[] sla){
   
    for(int p=0;p<sla.size();p++){
        def valor= sla[p].split("!")        
        if(solicitud==valor[0]){
            return valor[1]
        }
        
    }
    return 0
    
}