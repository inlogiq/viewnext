import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.*
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import static java.lang.Math.*
import java.sql.Timestamp


def mIssue= (MutableIssue) issue;
def issueManager = ComponentAccessor.getIssueManager()
def userManager = ComponentAccessor.getUserManager();
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
// guardamos el CF
def ppaCompleto = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("PPACompleto")



//guardamos los valores
if (ppaCompleto.getValue(issue) != "Si"){
	mIssue.setCustomFieldValue(ppaCompleto , "Si");
}else{
    log.warn("no es null")
}
//actualizamos la issue
ComponentAccessor.getIssueManager().updateIssue(currentUser, mIssue, EventDispatchOption.ISSUE_UPDATED, false)