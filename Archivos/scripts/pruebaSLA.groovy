import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade
@WithPlugin("com.atlassian.servicedesk")
import java.util.concurrent.TimeUnit
import com.onresolve.scriptrunner.canned.jira.admin.CopyProject
import  java.util.Date.*
import com.atlassian.jira.issue.index.IssueIndexingService
//import com.atlassian.jira.issue.Issue
import com.atlassian.jira.util.ImportUtils




/*Thread executorThread = new Thread(new Runnable() {
    void run(){
        TimeUnit.SECONDS.sleep(60);
        
    }
})
log.warn("hola hola")
executorThread.start()
*/

//Migración GB - Entrega DF/DP EnCurso

//usuario que ejecuta las modificaciones
        def userManager = ComponentAccessor.getUserManager()
        def currentUser = userManager.getUserByName("admin");

        def issueManager = ComponentAccessor.getIssueManager()
    	def issueA = (MutableIssue) ComponentAccessor.getIssueManager().getIssueObject("AMCAMSSAP-1003")
        def issue = (MutableIssue) ComponentAccessor.getIssueManager().getIssueObject("AMCAMSSAP-1012")
        def issueType = issue.getIssueType().getName()
        def currentProject = issue.projectObject.name
        log.warn(currentProject)
        def flujoevomayor
        def proyecto
        def proyectoIN = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Fin Pre Entrega").getValue(issueA)
		def mills = proyectoIN.getTime()
		log.warn("AYER: " + mills)
		def fecha = new Date()
		log.warn("HOY: " + fecha.getTime())
		def resta = fecha.getTime() - mills
		log.warn("RESTA: " + resta)
		def duration = resta*1.1
		log.warn("RESTA + porcentaje: " + duration)
		duration = ((duration/1000)/60)/60
		log.warn("HORAS: " + duration)
		
		log.warn("horas finales: " + formatearHoras(duration.toString()))
		issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Migración GB - Entrega DF/DP EnCurso"),formatearHoras(duration.toString()));
		ComponentAccessor.getIssueManager().updateIssue(currentUser, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
    //Reindexo la issue
        def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
        boolean wasIndexing = ImportUtils.isIndexIssues();
        ImportUtils.setIndexIssues(true);
        issueIndexingService.reIndex(issueManager.getIssueObject(issue.id));
        ImportUtils.setIndexIssues(wasIndexing);
public String formatearHoras(String time){
    def valor= time.split("\\.")
    log.warn("valor: " + valor)
    if(valor.size()>1){
        int min = Integer.parseInt(valor[1]);
        log.warn('min '+min)
        if(min>0){
            BigDecimal multiplicacion= Math.pow(10, valor[1].size());
            def minutos = (min*60)/multiplicacion
            return valor[0]+"h"+" "+Math.round(minutos)+"m"
        }else{
            return valor[0]+"h";
        }
    }else{
        return time+"h";
    }
}