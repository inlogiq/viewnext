import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue

def log = Logger.getLogger(getClass())

def versionManager = ComponentAccessor.getVersionManager()
def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();

//(MutableIssue) issue = (MutableIssue) issueManager.getIssueObject("VN-9") //

//def issue = issueManager.getIssueObject("VN-9")

// obememos los propietarios de la issue
def propietario = customFieldManager.getCustomFieldObjectByName("VN: Propietario"); // Propietario

def propietarioValue = propietario.getValue(issue);

/* Get Insight Object Facade from plugin accessor */
 Class objectFacadeClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade");
 def objectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectFacadeClass);

log.error(propietarioValue.getAt(0).class.name);

def myCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario"); // Propietario

//si quiero el propietario propietario
def objectBean = myCF.getValue(issue)[0]
//si quiero el segundo propietario
def objectBean2 = myCF.getValue(issue)[1]
 //return objectBean
def valor = objectFacade.loadObjectAttributeBean(objectBean.getId(), "Usuario").getObjectAttributeValueBeans()[0].getValue() //attribute id

def user = ComponentAccessor.getUserManager().getUserByKey(valor); 

def userAssignee = ComponentAccessor.userManager.getUserByName(user.getUsername())
issue.setAssignee(userAssignee)

issue.store()