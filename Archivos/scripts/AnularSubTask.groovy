import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.config.SubTaskManager
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.user.util.*;

//def issue = ComponentAccessor.getIssueManager().getIssueObject('SAMSSAP-662')
def constantManager = ComponentAccessor.getConstantsManager()
def issueService = ComponentAccessor.getIssueService()
UserManager userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");
SubTaskManager subTaskManager = ComponentAccessor.getSubTaskManager()
Collection subTasks = issue.getSubTaskObjects()
if (subTaskManager.subTasksEnabled && !subTasks.empty) {
    subTasks.each { Issue it ->
        
        def issueInputParameters = new IssueInputParametersImpl()
		log.warn("sub "+it.getStatus().name)
                
        if(it.getStatus().name=="Abierta"){
            
            def transitionValidationResult = issueService.validateTransition(admin, it.id, 31, issueInputParameters)
            
            if (transitionValidationResult.isValid()) {
                
                issueService.transition(admin, transitionValidationResult)                
                
            }else{
                log.warn("Ha ocurrido un error transicionando a Anulada la incidencia "+it.key)
            }
        }
        
        if(it.getStatus().name=="En progreso"){
            
            def transitionValidationResult = issueService.validateTransition(admin, it.id, 41, issueInputParameters)
            
            if (transitionValidationResult.isValid()) {
                
                issueService.transition(admin, transitionValidationResult)                
                
            }else{
                log.warn("Ha ocurrido un error transicionando a Anulada la incidencia "+it.key)
            }
        }
       
    }
}
