import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import java.sql.Timestamp
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption

def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
//def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

def myPropietario2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Urgencia 2"); 
def propietario2Ele = myPropietario2CF.getValue(issue)

def myPropietario1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Urgencia 1"); 
def propietario1Ele = myPropietario1CF.getValue(issue)

//si tiene propietario urgencia 1 y propietario urgencia 2
if(propietario2Ele != null  && propietario1Ele != null){
    log.info "ANGELES: SiVariosAutorizadoresUrgencia ================= TRUE existen dos autorizadores de urgencia" 
    return true
} else{
     log.info "ANGELES: SiVariosAutorizadoresUrgencia ================= FALSE NO existen dos autorizadores de urgencia" 
    return false
}
