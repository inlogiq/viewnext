import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.workflow.JiraWorkflow
import com.atlassian.jira.user.util.*;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue

log.info "EMPIEZA CONDICION SiVariosSuplentes 7777 " 
//Autorizador Suplente 2
def myCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 2"); 
def autorizador2Suplente = myCF.getValue(issue)

def mysuplente1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 1"); 
def autorizador1Suplente = mysuplente1CF.getValue(issue)

log.info "ESiVariosSuplentes autorizador1Suplente 777777"  + autorizador1Suplente
log.info "ESiVariosSuplentes autorizador2Suplente 777777 "  + autorizador2Suplente

//si tiene suplente 1 y suplente 2
if(autorizador2Suplente != null  && autorizador1Suplente != null){
    log.info "ANGELES: TERMINA SiVariosSuplentes ================= TRUE existen DOS SUPLENTES" 
    return true
}

log.info "ANGELES: TERMINA SiVariosSuplentes ================= FALSE NO existen DOS SUPLENTES" 
return false