import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.event.type.EventDispatchOption
import com.opensymphony.workflow.InvalidInputException
import com.onresolve.scriptrunner.runner.util.UserMessageUtil

def versionManager = ComponentAccessor.getVersionManager()
def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//miramos quien ha sido el primer autorizador, para controlar que no vuelva a autorizar el mismo autorizador en el caso que sean dos
Collection primerAutorizadorCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Primer Autorizador"); 
CustomField primerAutorizadorCF = primerAutorizadorCustomfield[0]

//Autorizador Urgencia 1
def my1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Urgencia 1"); 
def autorizador1Urgencia = my1CF.getValue(issue)

//Autorizador Urgencia 2
def my2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Urgencia 2"); 
def autorizador2Urgencia = my2CF.getValue(issue)

if (autorizador2Urgencia == userLogado && (userLogado.getName() != primerAutorizadorCF.getValue(issue).getName())){
    return true
} else if (autorizador1Urgencia == userLogado && (userLogado.getName() != primerAutorizadorCF.getValue(issue).getName())){
    return true
}
return false 