import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.user.util.*
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.Issue


//Insight 
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import com.onresolve.scriptrunner.runner.customisers.PluginModule

PrimerPropietario()

public Boolean PrimerPropietario(){

    @WithPlugin("com.riadalabs.jira.plugins.insight")    
    @PluginModule ObjectFacade objectFacade
     
    def issueManager = ComponentAccessor.getIssueManager() 

    log.error "ANGELES: SiPermisoUsuarioEspropietario1. =================" 
    
    // Objeto primer propietario
    def mypropietario1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 1");

    def objectmypropietario1CFEle = mypropietario1CF.getValue(issue)
    //siu no tiene valor el objeto propietariodevolvemos false
    if(objectmypropietario1CFEle == null){
        log.info "ANGELES: TERMINA SiPermisoUsuarioEspropietario1. ================= FALSE ES NULL EL PROPIETARIO 1" 
        return false
    } 
     
    def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
    //def myObjectBean = objectmypropietario1CFEle.getAt("0")
        
    def myIDObjectBean = objectmypropietario1CFEle[0].getId()
      
    def propietario1 = objectFacade.loadObjectAttributeBean(myIDObjectBean, "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del primer propietarios
    
	def propietario1Value = propietario1.getValue()

    def userPropietarioJira = ComponentAccessor.getUserManager().getUserByKey(propietario1Value); 

    if(userPropietarioJira.getName() == userLogado.getName()){
       log.error "ANGELES: TERMINA SiPermisoUsuarioEspropietario1. ================= TRUE SE TRATA DEL PROPIETARIO 1" 
        return true
    } else {
        log.error "ANGELES: TERMINA SiPermisoUsuarioEspropietario1. ================= FALSE NO SE TRATA DEL PROPIETARIO 1" 
      	return false 
    }   
}    
