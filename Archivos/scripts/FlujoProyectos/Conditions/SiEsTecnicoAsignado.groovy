import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.fields.CustomField

def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//comprobamos que el usuario logado sea el tecnico asignado para permitir esta accion
Collection tecnicoAsignadoCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Técnico Asignado"); 
CustomField tecnicoAsignadoCF = tecnicoAsignadoCustomfield[0]
//def tecnico = tecnicoAsignadoCF.getValue(issue)
//String tecnico = tecnicoAsignadoCF.getValueFromIssue(issue)

String tecnico = (String) tecnicoAsignadoCF.getValue(issue).getName()

log.error "ANGELES: SiEsTecnicoAsignado ================= tecnico: " + tecnico

if(userLogado.getName().equalsIgnoreCase(tecnico)){
    log.error "ANGELES: SiEsTecnicoAsignado ================= userLogado.getName(): " + userLogado.getName() + " DEVUELVE TRUE"
   return true
}else{
    log.error "ANGELES: SiEsTecnicoAsignado ================= userLogado.getName(): " + userLogado.getName() + " DEVUELVE FALSE"
   return false
}