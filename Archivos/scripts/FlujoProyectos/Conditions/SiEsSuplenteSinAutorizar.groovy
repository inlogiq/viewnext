import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//miramos quien ha sido el primer autorizador, para controlar que no vuelva a autorizar el mismo autorizador en el caso que sean dos
Collection primerAutorizadorCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Primer Autorizador"); 
CustomField primerAutorizadorCF = primerAutorizadorCustomfield[0]

//Autorizador suplente 1
def my1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 1"); 
def autorizador1Suplente = my1CF.getValue(issue)

//Autorizador Suplente 2
def my2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 2"); 
def autorizador2Suplente = my2CF.getValue(issue)

log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= autorizador2Suplente: " + autorizador2Suplente 
log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= autorizador1Suplente: " + autorizador1Suplente 

log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= userLogado: " + userLogado

log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= userLogado.getName(): " + userLogado.getName()

log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= primerAutorizadorCF.getValue(issue).getName(): " + primerAutorizadorCF.getValue(issue).getName()



if (autorizador2Suplente == userLogado && (userLogado.getName() != primerAutorizadorCF.getValue(issue).getName())){
        log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= TRUE ES suplente pendiente de autorizarr" 
    return true
} else if (autorizador1Suplente == userLogado && (userLogado.getName() != primerAutorizadorCF.getValue(issue).getName())){
      log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= TRUE ES suplente pendiente de autorizarr" 
    return true
}

log.error "ANGELES: SiEsSuplenteSinAutorizar. ================= FALSE NO ES SUPLENTE PENDIENTE DE AUTORIZAR" 
return false
