import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

log.error "VALIDACION NUEVA: SiEspropitetario sin autorizar"
/* Get Insight Object Facade from plugin accessor */
Class objectFacadeClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade");
def objectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectFacadeClass);

// Obtenemos el primer propietario
def mypropietario1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 1"); 
def objectmypropietario1CFEle = mypropietario1CF.getValue(issue)

def objectBean1 = mypropietario1CF.getValue(issue)[0]
def myObjectBean1 = objectmypropietario1CFEle[0]

// Obtenemos el segundo propietario
def mypropietario2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 2"); 
def objectmypropietario2CFEle = mypropietario2CF.getValue(issue)

def objectBean2 = mypropietario2CF.getValue(issue)[0]
def myObjectBean2 = objectmypropietario2CFEle[0]

//miramos quien ha sido el primer autorizador, para controlar que no vuelva a autorizar el mismo autorizador en el caso que sean dos
Collection primerAutorizadorCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Primer Autorizador"); 
CustomField primerAutorizadorCF = primerAutorizadorCustomfield[0]

def propietario1 = objectFacade.loadObjectAttributeBean(myObjectBean1.getId(), "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del primer propietario
def propietario1Value = propietario1.getValue()
def userPropietario1Jira = ComponentAccessor.getUserManager().getUserByKey(propietario1Value); 

def propietario2 = objectFacade.loadObjectAttributeBean(myObjectBean2.getId(), "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del segundo propietario
def propietario2Value = propietario2.getValue()
def userPropietario2Jira = ComponentAccessor.getUserManager().getUserByKey(propietario2Value); 

if((userPropietario1Jira.getName() == userLogado.getName()) && (userLogado.getName() != primerAutorizadorCF.getValue(issue).getName())){
    log.error "VALIDACION NUEVA: SiEspropitetario sin autorizar devuelve: TRUE" 
    return true
} else if ((userPropietario2Jira.getName() == userLogado.getName()) && (userLogado.getName() != primerAutorizadorCF.getValue(issue).getName())){
        log.error "VALIDACION NUEVA: SiEspropitetario sin autorizar devuelve: TRUE" 
   return true 
}    

log.error "VALIDACION NUEVA: SiEspropitetario sin autorizar devuelve: FALSE" 
return false
