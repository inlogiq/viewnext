import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.MutableIssue

def versionManager = ComponentAccessor.getVersionManager()
def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()


//Obtenemos el valor del campo Autorizador Urgencia 2
Collection autorizadorUrgencia2Customfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Autorizador Urgencia 2"); 
CustomField autorizadorUrgencia2CF = autorizadorUrgencia2Customfield[0]
def valorAutorizadorUrgencia2 = autorizadorUrgencia2CF.getValue(issue)

//si no existe el segundo autorizador de urgencia, debe salir este boton
if(valorAutorizadorUrgencia2 == null){
    log.info "ANGELES: SiUnSoloAutorizadorUrgencia ================= TRUE EXISTE SOLO UN AUTORIZADOR DE URGENCIA"  
    return true
}     
  
log.info "ANGELES: SiUnSoloAutorizadorUrgencia ================= FALSE NO EXISTE SOLO UN AUTORIZADOR DE URGENCIA"  
return false