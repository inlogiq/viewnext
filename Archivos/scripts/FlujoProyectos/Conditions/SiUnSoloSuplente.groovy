import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.workflow.JiraWorkflow
import com.atlassian.jira.user.util.*;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue

//Autorizador Suplente 2
def myCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 2"); 
def autorizador2Suplente = myCF.getValue(issue)

if (autorizador2Suplente == null){
    log.info "ANGELES: TERMINA  SiUnSoloSuplente->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEVUELVE TRUE"
    return true
} else {
     log.info "ANGELES: TERMINA SiUnSoloSuplente->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DEVUELVE FALSE"
    return false
}