import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.MutableIssue

def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

log.error "ANGELES: SiPermisoUsuarioEsPropietario ================="
/* Get Insight Object Facade from plugin accessor */
Class objectFacadeClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade");
def objectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectFacadeClass);

// Objeto primer propietario
def mypropietario1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 1"); 
def objectmypropietario1CFEle = mypropietario1CF.getValue(issue)

// Objeto segundo propietario
def mypropietario2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 2"); 
def objectmypropietario2CFEle = mypropietario2CF.getValue(issue)

//si no existe el propietario 1 ni el propietario 2 esta acción no debe existir
if(objectmypropietario1CFEle == null && objectmypropietario2CFEle == null ) {
    log.error "ANGELES: SiPermisoUsuarioEsPropietario ================= false pq los dos propietarios son nulos"
    return false
} else{
    //boolean esPropietario1 = false
    //boolean esPropietario2 = false
    //propietario 1
    if(objectmypropietario1CFEle != null ){
        def objectBean = mypropietario1CF.getValue(issue)[0]
        def myObjectBean = objectmypropietario1CFEle[0]

        //valor del propietario 1
        def propietario1 = objectFacade.loadObjectAttributeBean(myObjectBean.getId(), "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del segundo propietarios
        def propietario1Value = propietario1.getValue()
        def userPropietarioJira = ComponentAccessor.getUserManager().getUserByKey(propietario1Value); 
        //si se trata de primero o segundo propietario se muestra la aacción Autorizar
        if(userPropietarioJira.getName() == userLogado.getName()) {
            log.error "ANGELES: SiPermisoUsuarioEsPropietario ================= true pq coincide con el primer propietario"
            //esPropietario1 = true
            return true
        }
    }

    //propietario 2
    if(objectmypropietario2CFEle != null ){

        def objectBean2 = mypropietario2CF.getValue(issue)[0]
        def myObjectBean2 = objectmypropietario2CFEle[0]

        //valor del propietario 2
        def propietario2 = objectFacade.loadObjectAttributeBean(myObjectBean2.getId(), "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del segundo propietarios
        def propietario2Value = propietario2.getValue()
        def userPropietario2Jira = ComponentAccessor.getUserManager().getUserByKey(propietario2Value); 

        //si se trata de primero o segundo propietario se muestra la aacción Autorizar
        if(userPropietario2Jira.getName() == userLogado.getName() ){
            log.error "ANGELES: SiPermisoUsuarioEsPropietario ================= false pq no coincide con el segundo propiertario"
            //esPropietario2 = true
            return true
        }  
    }
    
    log.error "ANGELES NUEVO: SiPermisoUsuarioEsPropietario ================= FALSE NO ES NI PRIMER NI SEGUNDO PROIETARIO"
    return false
}
 