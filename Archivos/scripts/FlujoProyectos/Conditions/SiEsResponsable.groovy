import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import java.sql.Timestamp

def versionManager = ComponentAccessor.getVersionManager()
def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

/* Get Insight Object Facade from plugin accessor */
Class objectFacadeClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade");
def objectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectFacadeClass);


def myCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Gestores"); //Lista de responsables
def objectEle = myCF.getValue(issue)

log.error "ANGELES: SiEsgestor================= recorremos los gestores "
//si existen objetos responsables
if(objectEle.size() > 0) {
    
	int i=0
    //recorremos la lista de Objetos responsablkes para ver si alguno es el mismo que el usuario logado, para poder realizar la accion
    while (i < objectEle.size()){
        
        //log.info("usuario bucle: " + myCF.getValue(issue)[i])
        def objectBean = myCF.getValue(issue)[i]
               
        def usuario = objectFacade.loadObjectAttributeBean(objectBean.getId(), "Responsable").getObjectAttributeValueBeans()[0].getValue() //attribute Usuario del primer propietarios
        //log.info("usuario: " + usuario)

        def userResponsableJira = ComponentAccessor.getUserManager().getUserByKey(usuario); 

        if(userLogado == userResponsableJira){
            //log.error("entra, lo encuenta y devuelve true")
            //def userAssignee = ComponentAccessor.userManager.getUserByName(userPropietarioJira.getUsername())
            log.error "ANGELES: SiEsResponsable ================= TRUE COINCIDE"
            return true
            
        }    
        i++
    }
    //sale del bucle pq no ha encontrado que el usuario logado sea alguno d elos propietarios de la aplicacion
   // throw new InvalidInputException("Validation Error: no ha encontrado que el usuario logado sea alguno d elos propietarios de la aplicacion")
     //UserMessageUtil.error('No ha encontrado que el usuario logado sea alguno de los responsables de la aplicacion')
     log.error "ANGELES: SiEsResponsable ================= FALSE: No ha encontrado que el usuario logado sea alguno de los responsables de la aplicacion"
     //throw new InvalidInputException("Validation Error: no ha encontrado que el usuario logado sea alguno de  los propietarios de la aplicacion")
     return false

}else{
    //devuelve false pq no hay propietarios
    log.error "ANGELES: SiEsResponsable ================= FALSE: No existen responsables asociados a la aplicacion"
    //UserMessageUtil.error('No existen responsables asociados a la aplicacion')
    //throw new InvalidInputException("Validation Error: NO hay propietarios")
    return false
} 
