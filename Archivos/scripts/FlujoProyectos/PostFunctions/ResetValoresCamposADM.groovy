import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.*
import java.sql.Timestamp
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def mIssue= (MutableIssue) issue;
//def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

def changeHolder = new DefaultIssueChangeHolder()

//actualizo el campo Log Subida TESt
Collection LogSubidaTestCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Log de Subida Test"); 
CustomField LogSubidaTestCF = LogSubidaTestCustomfield[0]

//actualizamos el valor del campo en la tarea a null
mIssue.setCustomFieldValue(LogSubidaTestCF, null)

//actualizo el campo Log Subida TESt


Collection customSolucion = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Descripción Solución Técnica"); 
CustomField solucionTecnicaCf = customSolucion[0]

//actualizamos el valor del campo en la tarea a null
mIssue.setCustomFieldValue(solucionTecnicaCf, null)

//actualizamos la tarea
issueManager.updateIssue(user, mIssue, EventDispatchOption.DO_NOT_DISPATCH, false)