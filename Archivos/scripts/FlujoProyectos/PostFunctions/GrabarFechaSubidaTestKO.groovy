import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import java.sql.Timestamp
import com.atlassian.jira.event.type.EventDispatchOption

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

MutableIssue mIssue= (MutableIssue) issue;
// a date time field
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Fecha Subida a Test KO")
CustomField dateCf = cfs[0]

mIssue.setCustomFieldValue(dateCf, new Timestamp((new Date()).getTime()))

//actualizamos la issue
issueManager.updateIssue(user, mIssue, EventDispatchOption.ISSUE_UPDATED, false)
//issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)