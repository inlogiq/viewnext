import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.*
import java.sql.Timestamp
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def mIssue= (MutableIssue) issue;
//def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

def changeHolder = new DefaultIssueChangeHolder()
// a date time field
//def dateCf = customFieldManager.getCustomFieldObjectByName("VN: Fecha de Cancelación") // Date time fields require a Timestamp
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Fecha Autorización")
CustomField dateCf = cfs[0]

mIssue.setCustomFieldValue(dateCf, new Timestamp((new Date()).getTime()))

//actualizo la persona que ha autorizado y el numero de autorizaciones que se van realizando
Collection numAutorizadosCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("NumeroAutorizadas"); 
CustomField numeroAutorizacionesCF = numAutorizadosCustomfield[0]
Double numeroAutorizacionesValue = (Double)numeroAutorizacionesCF.getValue(mIssue)
Double numeroAutorizacionesValueNuevo = 0

if(numeroAutorizacionesValue == null){
    numeroAutorizacionesValueNuevo = 1
} else {
    numeroAutorizacionesValueNuevo = numeroAutorizacionesValue + 1
}

//actualizamos el valor del campo en la tarea
mIssue.setCustomFieldValue(numeroAutorizacionesCF, numeroAutorizacionesValueNuevo)

//actualizamos la tarea
issueManager.updateIssue(user, mIssue, EventDispatchOption.DO_NOT_DISPATCH, false)