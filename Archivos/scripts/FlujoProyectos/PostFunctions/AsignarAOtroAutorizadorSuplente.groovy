import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.user.ApplicationUser

def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

def changeHolder = new DefaultIssueChangeHolder()

//obtenemos el primer autorizador suplente
Collection primerAutorizadorSuplenCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Autorizador Suplente 1"); 
CustomField primerAutorizadorSuplenCF = primerAutorizadorSuplenCustomfield[0]

//obtenemos el segundo autorizador suplente
Collection segundoAutorizadorSuplenCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Autorizador Suplente 2"); 
CustomField segundoAutorizadorSuplenCF = segundoAutorizadorSuplenCustomfield[0]
//numeroAutorizacionesCF.updateValue(null, issue, new ModifiedValue(numeroAutorizacionesValue, numeroAutorizacionesValueNuevo), changeHolder)
//Object primerAutorizadorUrgencia = primerAutorizadorUrgenCF.getValue(issue)


def userPropietario2Jira = issue.getCustomFieldValue(segundoAutorizadorSuplenCF) as ApplicationUser 
def userPropietario1Jira = issue.getCustomFieldValue(primerAutorizadorSuplenCF) as ApplicationUser 

//si el usuario logado es el primer suplente se lo asigno al segundo suplente
if(userLogado == userPropietario1Jira){
   //le asigno el segundo propietario suplente
   issue.setAssignee(userPropietario2Jira)
} else {
   //le asigno al primer propietario suplente
   issue.setAssignee(userPropietario1Jira)
}


//grabamos los campos en la issue
MutableIssue mutIssue = (MutableIssue) issue;
issueManager.updateIssue(userLogado, mutIssue, EventDispatchOption.ISSUE_UPDATED, false)
