import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import java.sql.Timestamp
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.MutableIssue

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

MutableIssue mIssue= (MutableIssue) issue;

// a date time field
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Fecha Deshecho Cambios en Test")
CustomField dateCf = cfs[0]

mIssue.setCustomFieldValue(dateCf, new Timestamp((new Date()).getTime()))
//actualizamos la issue
ComponentAccessor.getIssueManager().updateIssue(user, mIssue, EventDispatchOption.ISSUE_UPDATED, false)


