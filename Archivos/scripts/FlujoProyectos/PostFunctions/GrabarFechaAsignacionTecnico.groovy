import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.*
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.event.type.EventDispatchOption
import java.sql.Timestamp

def mIssue= (MutableIssue) issue;

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

// a date time field
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Fecha Asignación")
CustomField dateCf = cfs[0]

mIssue.setCustomFieldValue(dateCf, new Timestamp((new Date()).getTime()))
//issueManager.updateIssue(user, mIssue, EventDispatchOption.DO_NOT_DISPATCH, false)
//actualizamos la issue
issueManager.updateIssue(user, mIssue, EventDispatchOption.ISSUE_UPDATED, false)
