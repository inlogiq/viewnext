import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import java.sql.Timestamp
import com.atlassian.jira.event.type.EventDispatchOption;

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

MutableIssue mIssue= (MutableIssue) issue;
// a date time field
//def dateCf = customFieldManager.getCustomFieldObjectByName("VN: Fecha de Cancelación") // Date time fields require a Timestamp
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Fecha Envío Subida Prod")
CustomField dateCf = cfs[0]

mIssue.setCustomFieldValue(dateCf, new Timestamp((new Date()).getTime()))

//actualizamos la issue
issueManager.updateIssue(user, mIssue, EventDispatchOption.ISSUE_UPDATED, false)