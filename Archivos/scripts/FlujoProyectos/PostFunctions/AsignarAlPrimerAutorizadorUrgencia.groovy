import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.user.ApplicationUser


def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

def changeHolder = new DefaultIssueChangeHolder()


//obtenemos el primer autorizador e urgencia, para asignarle la tarea
Collection primerAutorizadorUrgenCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Autorizador Urgencia 1"); 
CustomField primerAutorizadorUrgenCF = primerAutorizadorUrgenCustomfield[0]


def assignee = issue.getCustomFieldValue(primerAutorizadorUrgenCF) as ApplicationUser 
issue.setAssignee(assignee)

//actualizamos la tarea
//issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)

//grabamos los campos en la issue
MutableIssue mutIssue = (MutableIssue) issue;
issueManager.updateIssue(user, mutIssue, EventDispatchOption.ISSUE_UPDATED, false)