import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.MutableIssue

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

//asignamos la incidencia la tecnico seleccionado
Collection tecnicocfs = customFieldManager.getCustomFieldObjectsByName("VN: Técnico Asignado")
CustomField tecnicoAsignadoCf = tecnicocfs[0]
def userTecnicoAsignado = issue.getCustomFieldValue(tecnicoAsignadoCf) as ApplicationUser;

issue.assignee = userTecnicoAsignado

//actualizamos la issue
//issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)

//grabamos los campos en la issue
MutableIssue mutIssue = (MutableIssue) issue;
issueManager.updateIssue(user, mutIssue, EventDispatchOption.ISSUE_UPDATED, false)