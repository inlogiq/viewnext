import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.*
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import java.sql.Timestamp

def mIssue= (MutableIssue) issue;

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

// a date time field
//def dateCf = customFieldManager.getCustomFieldObjectByName("VN: Fecha de Cancelación") // Date time fields require a Timestamp
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Fecha de Cancelación")
CustomField dateCf = cfs[0]

mIssue.setCustomFieldValue(dateCf, new Timestamp((new Date()).getTime()))
//issueManager.updateIssue(user, mIssue, EventDispatchOption.DO_NOT_DISPATCH, false)
//actualizamos la issue
ComponentAccessor.getIssueManager().updateIssue(user, mIssue, EventDispatchOption.ISSUE_UPDATED, false)