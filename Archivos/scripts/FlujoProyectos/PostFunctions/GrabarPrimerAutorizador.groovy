import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.*
import java.sql.Timestamp
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue


def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def mIssue= (MutableIssue) issue;
//def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

def changeHolder = new DefaultIssueChangeHolder()

//grabamos el primer autorizador, para controlar que no vuelva a autorizar el mismo autorizador en el caso que sean dos
Collection primerAutorizadorCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Primer Autorizador"); 
CustomField primerAutorizadorCF = primerAutorizadorCustomfield[0]
//numeroAutorizacionesCF.updateValue(null, issue, new ModifiedValue(numeroAutorizacionesValue, numeroAutorizacionesValueNuevo), changeHolder)

def userAssignee = ComponentAccessor.userManager.getUserByName(user.getUsername())
//grabamos el primer autorizador 
mIssue.setCustomFieldValue(primerAutorizadorCF, userAssignee)

//actualizamos la tarea
issueManager.updateIssue(user, mIssue, EventDispatchOption.DO_NOT_DISPATCH, false)