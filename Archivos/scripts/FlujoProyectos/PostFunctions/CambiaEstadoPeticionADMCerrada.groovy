import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.index.IssueIndexManager
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.workflow.JiraWorkflow
import com.atlassian.jira.workflow.WorkflowManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.workflow.TransitionOptions;



def issueLinkManager = ComponentAccessor.getIssueLinkManager()
def issueIndexManager = ComponentAccessor.getComponent(IssueIndexManager)
def issueManager = ComponentAccessor.getIssueManager()
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def issueService = ComponentAccessor.getIssueService()
//Workflow
WorkflowManager workflowManager = ComponentAccessor.getWorkflowManager()

//recorremos la lista de peticiones ADM enlazada a la peticion GC que se encuentre en estado PENDIENTE DE SUBIDA A PROD

issueLinkManager.getInwardLinks(issue.id).each {issueLink ->
    
    log.warn(issueLink.issueLinkType.name)
    
    if (issueLink.issueLinkType.name == "Escalado") { 
		//esta es la peticion de GC
        Issue linkedIssue = issueLink.getDestinationObject()
        //este me da la peticion de DEsarrollo ADM
        Issue linkedIssue2 = issueLink.getSourceObject()
        
        //Status estado =linkedIssue2.getStatus()
        String statusId = linkedIssue2.getStatusId()
        log.warn ('statusId : **************************** ' + statusId )
        log.warn ('linkedIssue2.getStatusObject().getName() : **************************** ' + linkedIssue2.getStatusObject().getName() )
        
        
        if(linkedIssue2.getStatusObject().getName() == "Pendiente De Subida a PROD") {
        
            def mutableIssue = issueManager.getIssueObject(linkedIssue2.id)

            JiraWorkflow workflow = workflowManager.getWorkflow(linkedIssue2)
            //me devuelve el id de la transicion que quiero ejecutar
            def workflowActionId = workflowManager.getWorkflow(linkedIssue2).allActions.findByName("Subida a Producción")?.id
            def issueInputParameters = issueService.newIssueInputParameters()


            TransitionOptions transitionOptions = new TransitionOptions.Builder().skipConditions().skipPermissions().skipValidators().build();


            IssueService.TransitionValidationResult validationResult = issueService.validateTransition(userLogado, linkedIssue2.id, workflowActionId as Integer, issueInputParameters, transitionOptions)

            //def transitionValidationResult = issueService.validateTransition(userLogado, linkedIssue2.id, workflowActionId, issueInputParameters)
           // TransitionValidationResult validationResult = transitionValidationResult.isValid()
            def errorCollection = validationResult.errorCollection


               log.info('PROBANDO *******************************:')
             if ( !errorCollection.hasAnyErrors()) {
                    log.info('TRANSACCION SIN ERRORES')
                    //validamos la transicion a realizar a la peticon ADM asociada para cerrarla
                    if (validationResult.isValid()) {

                        log.info("Valido la transicion Subida a Producción de la incidencia ADM ASOCIADA"+ linkedIssue2.id)
                        def transitionResult = issueService.transition(userLogado, validationResult)
                        if(transitionResult.isValid()){
                             log.info("Valido la transicion Subida a Producción de la incidencia ADM ASOCIADA "+ transitionResult.isValid())
                        } else {
                             log.info("NO Valido la transicion Subida a Producción de la incidencia ADM ASOCIADA "+ transitionResult.isValid())
                        }


                     }else{
                        log.warn("Ha ocurrido un error transicionando Subida a Producción de la incidencia ADM ASOCIADA "+ linkedIssue2.id)
                     }


               } else {
                      log.warn('ERROR EN linkedIssue EN LA TRANSICION de ADM : ' + linkedIssue2.id)
                      log.warn('ERRORES: ' +errorCollection) 
                      } 
        }else{
             log.warn ('NO ENTRA PQ EL ESTADO NO ES PENDIENTE DE SUBIDA A PROD: ' + linkedIssue2.getStatusObject().getName() )
        }
        

    }
}
