import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.*
import java.sql.Timestamp
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue


def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def mIssue= (MutableIssue) issue;
//def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()


//asignamos la incidencia la tecnico seleccionado
Collection tecnicocfs = customFieldManager.getCustomFieldObjectsByName("VN: Técnico Asignado")
CustomField tecnicoAsignadoCf = tecnicocfs[0]

def userAssignee = ComponentAccessor.userManager.getUserByName(user.getUsername())
//grabamos el primer autorizador 
mIssue.setCustomFieldValue(tecnicoAsignadoCf, userAssignee)

//actualizamos la tarea
issueManager.updateIssue(user, mIssue, EventDispatchOption.DO_NOT_DISPATCH, false)