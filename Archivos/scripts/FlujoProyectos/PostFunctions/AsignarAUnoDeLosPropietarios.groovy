import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.fields.CustomField
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.workflow.JiraWorkflow
import com.atlassian.jira.user.util.*;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue


//Insight 
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;

//log.error(resultado)
AsignarPropietarios()

public AsignarPropietarios(){
    
    @WithPlugin("com.riadalabs.jira.plugins.insight")    
    @PluginModule ObjectFacade objectFacade

    def issueManager = ComponentAccessor.getIssueManager();
    def customFieldManager = ComponentAccessor.getCustomFieldManager()

        
    // Objeto primer propietario
    def mypropietario1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 1"); 
    def objectmypropietario1CFEle = mypropietario1CF.getValue(issue)

    // Objeto segundo propietario
    def mypropietario2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 2"); 
    def objectmypropietario2CFEle = mypropietario2CF.getValue(issue)

    // si hay valor en propietario1 o 2, obtenemos el usuario conectado para evr si es alguno de los propietarios
    def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
    
    //obtenemos los datos del primer propietario
    def myIDObjectBean1 = objectmypropietario1CFEle[0].getId()
    log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:ID  myObjectBean: " + myIDObjectBean1
      
    def propietario1 = objectFacade.loadObjectAttributeBean(myIDObjectBean1, "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del primer propietarios
    log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:propietario1: " + propietario1

	def propietario1Value = propietario1.getValue()
    log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:propietario1Value: " + propietario1Value
    def userPropietario1Jira = ComponentAccessor.getUserManager().getUserByKey(propietario1Value); 
    
    
     //obtenemos los datos del segundo propietario
    def myIDObjectBean2 = objectmypropietario2CFEle[0].getId()
    log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:ID  myObjectBean: " + myIDObjectBean2
      
    def propietario2 = objectFacade.loadObjectAttributeBean(myIDObjectBean2, "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del segundo propietario
    log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:propietario2: " + propietario2

	def propietario2Value = propietario2.getValue()
    log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:propietario2Value: " + propietario2Value
    def userPropietario2Jira = ComponentAccessor.getUserManager().getUserByKey(propietario2Value); 
    

    //si el usuario logado es el primer propietario se lo asigno al segundo propietario
    if(userLogado.getName() == userPropietario1Jira.getName()){
       //le asigno el segundo propietario
       issue.setAssignee(userPropietario2Jira)
    } else {
       //le asigno al primer propietario
       issue.setAssignee(userPropietario1Jira)
    }
    
    //grabamos los campos en la issue
    MutableIssue mutIssue = (MutableIssue) issue;
    issueManager.updateIssue(userLogado, mutIssue, EventDispatchOption.ISSUE_UPDATED, false)
} 


