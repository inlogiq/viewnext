import com.atlassian.jira.component.ComponentAccessor
import java.sql.Timestamp
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.issue.MutableIssue

def issueManager = ComponentAccessor.getIssueManager();
def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def customFieldManager = ComponentAccessor.getCustomFieldManager()

MutableIssue mIssue= (MutableIssue) issue;

//obtenemosel campo Fecha en Curso
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Fecha En Curso")
CustomField dateCf = cfs[0]
mIssue.setCustomFieldValue(dateCf, new Timestamp((new Date()).getTime()))


//actualizamos la issue
ComponentAccessor.getIssueManager().updateIssue(user, mIssue, EventDispatchOption.ISSUE_UPDATED, false)

//actualizamos la issue
//issueManager.updateIssue(user, issue, EventDispatchOption.DO_NOT_DISPATCH, false)