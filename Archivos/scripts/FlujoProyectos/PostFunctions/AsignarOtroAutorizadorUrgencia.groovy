import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.user.ApplicationUser

def issueManager = ComponentAccessor.getIssueManager();
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def changeHolder = new DefaultIssueChangeHolder()

//obtenemos el primer autorizador de urgencia
Collection primerAutorizadorUrgenCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Autorizador Urgencia 1"); 
CustomField primerAutorizadorUrgenCF = primerAutorizadorUrgenCustomfield[0]

//obtenemos el segundo autorizador e urgencia
Collection segundoAutorizadorUrgenCustomfield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Autorizador Urgencia 2"); 
CustomField segundoAutorizadorUrgenCF = segundoAutorizadorUrgenCustomfield[0]

def userPropietario2Jira = issue.getCustomFieldValue(segundoAutorizadorUrgenCF) as ApplicationUser 
def userPropietario1Jira = issue.getCustomFieldValue(primerAutorizadorUrgenCF) as ApplicationUser 

//si el usuario logado es el primer propietario se lo asigno al segundo propietario
if(userLogado == userPropietario1Jira){
   //le asigno el segundo propietario urgente
   issue.setAssignee(userPropietario2Jira)
} else {
   //le asigno al primer propietario urgente
   issue.setAssignee(userPropietario1Jira)
}


//grabamos los campos en la issue
MutableIssue mutIssue = (MutableIssue) issue;
issueManager.updateIssue(userLogado, mutIssue, EventDispatchOption.ISSUE_UPDATED, false)