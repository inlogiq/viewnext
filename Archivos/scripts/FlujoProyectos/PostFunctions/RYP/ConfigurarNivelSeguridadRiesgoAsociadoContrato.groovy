import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.workflow.JiraWorkflow
import com.atlassian.jira.user.util.*;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;


GruposUsuariosVisualizadores()

public String GruposUsuariosVisualizadores(){
    @WithPlugin("com.riadalabs.jira.plugins.insight")
    @PluginModule ObjectFacade objectFacade
    @PluginModule IQLFacade iqlFacade
    @WithPlugin("com.atlassian.servicedesk")
    
   // def issue = ComponentAccessor.getIssueManager().getIssueObject('RYP-55')
    //def issue = event.issue
    MutableIssue mIssue= (MutableIssue) issue
    def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
        
    def issueManager = ComponentAccessor.getIssueManager()
    def contrato = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Contratos").getValue(issue); 
    
    log.warn("EL VALOR DEL CONTRATO SELCCIONADO ES : " + contrato)
    
    if(contrato!=null){  
        
        Map<ApplicationUser,String> users = new HashMap<>();
                          
        def valorContrato = iqlFacade.findObjectsByIQLAndSchema(7, "objectType = \"Contratos\" AND ID = \""+contrato[0].name +"\"")
        
        def grupoUsers= objectFacade.loadObjectAttributeBean(valorContrato[0].getId(), 18131);
        
        if(grupoUsers!=null){
            
            int totalUsuarios= grupoUsers.getObjectAttributeValueBeans().size()
            log.warn("Total de usuarios visualizadores asocciados al Contrato " +totalUsuarios);

            //String[] usuarios= new String[totalUsuarios];
            //Usuarios del campo de Insight
            for(int i=0; i<totalUsuarios; i++){        
                def nombreUsuario = grupoUsers.getObjectAttributeValueBeans()[i].getValue()                                        
                
                def usuario = ComponentAccessor.getUserManager().getUserByKey(nombreUsuario)
                users.put(usuario, "")
            }    
        }
    	//Ahora añadimos los usuarios del grupo de administradores
			def groupManager = ComponentAccessor.getGroupManager()
			def groupJira_Administrator = groupManager.getGroup("jira-administrators")
        
    		Collection<ApplicationUser> usuarios_admin= groupManager.getUsersInGroup(groupJira_Administrator)
        
    		for(int i=0;i<usuarios_admin.size();i++){
                users.put(ComponentAccessor.getUserManager().getUserByKey(usuarios_admin[i].getKey()), "")
			}	
            
           // log.warn("****************" + users.size())
            def visualizadores = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Visualizadores");
            issue.setCustomFieldValue(visualizadores, users.keySet());
    		//actualizamos el nivel de Seguridad
    		issue.setSecurityLevelId(10400); 
            UserManager userManager = ComponentAccessor.getUserManager();
            def admin = userManager.getUserByName("0005810");
    		ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false)
     
            return "Guardado todos los usuarios"
        
    }else{
        return "El riesgo no tiene Contrato asociado"
    } 
}