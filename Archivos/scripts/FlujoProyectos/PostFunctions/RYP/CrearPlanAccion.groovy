import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.Issue

// the issue key of the parent issue
//final String parentIssueKey = "JRA-1"

// the issue type for the new issue - should be of type subtask
final String issueTypeName = "Plan de Acción"

// user with that user key will be the reporter of the issue
//final String reporterKey = "anuser"

// the summary of the new issue
final String summary = "Plan de Acción Nuevo"

// the priority of the new issue
final String priorityName = "Baja"

def issueService = ComponentAccessor.issueService
def constantsManager = ComponentAccessor.constantsManager
def loggedInUser = ComponentAccessor.jiraAuthenticationContext.loggedInUser

def issueManager = ComponentAccessor.getIssueManager()


MutableIssue mIssue= (MutableIssue) issue

log.warn("++++++++++++++++++++++++++++++++   el kye de la tare a padre es : " + issue.getKey())
def parentIssue = ComponentAccessor.issueManager.getIssueByCurrentKey(issue.getKey())

def subtaskIssueTypes = constantsManager.allIssueTypeObjects.findAll { it.subTask }
def subTaskIssueType = subtaskIssueTypes.findByName(issueTypeName)
log.warn("Encuentro la subtarea ")

// if we cannot find user with the specified key or this is null, then set as a  reporter the logged in user
def reporter = ComponentAccessor.userManager.getUserByKey(reporterKey) ?: loggedInUser

// if we cannot find the priority with the given name or if this is null, then set the default priority
def priority = constantsManager.priorities.findByName(priorityName) ?: constantsManager.defaultPriority

log.warn("Creando subtarea desde el script +++++++++++++++++++++++++++++")
def issueInputParameters = issueService.newIssueInputParameters().with {
    setProjectId(parentIssue.projectObject.id)
    setIssueTypeId(subTaskIssueType.id)
    setReporterId(reporter.key)
    setSummary(summary)
    setPriorityId(priority.id)
}

def validationResult = issueService.validateSubTaskCreate(loggedInUser, issue.id, issueInputParameters)
assert validationResult.valid : validationResult.errorCollection

def issueResult = issueService.create(loggedInUser, validationResult)
assert issueResult.valid : issueResult.errorCollection

def subtask = issueResult.issue
ComponentAccessor.subTaskManager.createSubTaskIssueLink(issue, subtask, loggedInUser)

log.warn("subtarea creada desde el script +++++++++++++++++++++++++++++")