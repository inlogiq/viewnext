import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption
import com.opensymphony.workflow.InvalidInputException
import com.onresolve.scriptrunner.runner.util.UserMessageUtil

def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()


log.error "EMPIEZA ValidaSiTienePermisoPrimerSuplente ********************************: "
//Autorizador Suplente 1
def myCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 1"); 
def autorizador1Suplente = myCF.getValue(issue)
//log.error "autorizador1Suplente ValidaSiTienePermisoPrimerSuplente ********************************: " + autorizador1Suplente


if (autorizador1Suplente == userLogado){
     log.error "TERMINA ValidaSiTienePermisoPrimerSuplente ********************************: TRUE"
    return true
} else {
   // UserMessageUtil.error('No tiene permiso para autorizar la aplicacion, no es autorizador suplente')
    log.error "TERMINA ValidaSiTienePermisoPrimerSuplente ********************************: FALSE"
    return false
    
}

