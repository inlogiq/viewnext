import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

def customFieldManager = ComponentAccessor.getCustomFieldManager()

// obtenemos el valor del campo Fecha Prevista de incio de la tarea
Collection customFechaPrevistaInicio = customFieldManager.getCustomFieldObjectsByName("VN: Fecha Prevista Inicio")
CustomField fechaprevistaInicioCf = customFechaPrevistaInicio[0]
//log.error(tipoProblemasCf.name)
//obtenemos el valor de la fecha previstoa de inicio de la tarea
Date fechaprevistaInicioValue = issue.getCustomFieldValue(fechaprevistaInicioCf)

if(fechaprevistaInicioValue == null ){
     //UserMessageUtil.error('Es obligatorio rellenar la Fecha Prevista de Inicio y Fin de la tarea')
    // invalidInputException = new InvalidInputException("Es obligatorio rellenar la Fecha Prevista de Inicio de la tarea");
      return false
}

//en otro caso se ha rellenado y devuelve true
return true

