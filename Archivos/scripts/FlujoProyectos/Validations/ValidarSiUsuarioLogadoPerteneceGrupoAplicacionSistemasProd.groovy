import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//Grupos usuario
def groupManager = ComponentAccessor.getGroupManager()
//def groupJira_PORTAL_TEST = groupManager.getGroup("Jira_PORTAL_TEST")

def groupJira_CONTENIDOSINTRANET_PROD = groupManager.getGroup("Jira_CONTENIDOS_INTRANET_PROD")

def usersInGroupJira_PORTAL_PROD = groupManager.getUsersInGroup("Jira_PORTAL_PROD")
def usersInGroupJira_CONTENIDOSINTRANET_PROD = groupManager.getUsersInGroup("Jira_CONTENIDOS_INTRANET_PROD")

/* Get Insight Object Facade from plugin accessor */
Class objectFacadeClass = ComponentAccessor.getPluginAccessor().getClassLoader().findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade");
def objectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectFacadeClass);

// Obtenemos la aplicacion asociada a la incidencia
def mypropietario1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Aplicación"); 
def objectmypropietario1CFEle = mypropietario1CF.getValue(issue)

def myObjectBean1 = objectmypropietario1CFEle[0]
def aplicacion = objectFacade.loadObjectAttributeBean(myObjectBean1.getId(), "Name").getObjectAttributeValueBeans()[0] //attribute Propietario del primer propietario
def aplicacionValue = aplicacion.getValue()

log.error "ANGELES: ValidarSiUsuarioLogadoPerteneceGrupoAplicacionSistemasProd VALOR DE aplicacionValue: " + aplicacionValue

//dependiendo de la aplicacion se permite a un grupo u otro de usuarios realizar la accion
//si se trata de a aplicacion PORTAL solo se permite realizar la accion si usuario logado pertenece al grupo Jira_PORTAL_TEST
if(aplicacionValue != null && aplicacionValue == "PORTAL"){
    
    if(usersInGroupJira_PORTAL_PROD.contains(userLogado)){
        log.error "ANGELES: ValidarSiUsuarioLogadoPerteneceGrupoAplicacionSistemasProd VALOR DE aplicacionValue: pertenece a PORTAL DEVUELVE TRUE " 
     return true
    }
}else if(aplicacionValue != null && aplicacionValue == "CONTENIDOS INTRANET"){ 
    
   if(usersInGroupJira_CONTENIDOSINTRANET_PROD.contains(userLogado)){
       log.error "ANGELES: ValidarSiUsuarioLogadoPerteneceGrupoAplicacionSistemasProd VALOR DE aplicacionValue: pertenece a CONTENIDOS INTRANET DEVUELVE TRUE " 
     return true
    }
}
//FALTAN TODOS LOS DEMAS DEMAS GRUPOS D ELAS DISNTABTAS APOLICACIONES QUE HAY
//VER COMO COGERLO DE FORMA DINAMICA LLAMADO AL GRUPO CON EL NOMBRE D ELA APLIACCION Y ASI MONTAR LA BUSQUEDA DE LOS USUARIOS DEL GRUPO DE FORMA DINAMICA
//si el usuario logado no pertenece al grupo de sistema de test devolvemos false
log.error "ANGELES: ValidarSiUsuarioLogadoPerteneceGrupoAplicacionSistemasProd false"
return false 
   