Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Información de las pruebas")
def myCF = cfs[0]

String idMotivoValue = issue.getCustomFieldValue(myCF)

//def regularExpression = "^.*[a-zA-Z0-9ÁÉÍÓÚáéíóúÑñ\\n\\r]+.*"
//|| !idMotivoValue.trim().matches(regularExpression)

if(idMotivoValue == null || idMotivoValue == "" || idMotivoValue.length() == 0 || idMotivoValue.trim().isEmpty() ){
    return false
} 
return true