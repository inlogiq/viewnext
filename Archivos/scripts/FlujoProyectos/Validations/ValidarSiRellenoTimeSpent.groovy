/*import com.atlassian.jira.issue.Issue

if (issue.getTimeSpent() == null){
 return false
} 

return true
*/
//****************** CONTROLANMDO EL USUARIO QUE HAYA HECHO IMPUTACION DE TIEMPO EN LA TAREA

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.worklog.*

def workLogManager = ComponentAccessor.getWorklogManager()
//get worklogs for issue
def logsForIssue = workLogManager.getByIssue(issue)
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

log.info "ANGELES: ValidarSiRellenoTimeSpent y userLogado: " + userLogado

//DUDA
// cogemos todas la imputaciones Worklog de la tarea
// no se si comprobar que exista alguna imputacion del usuario que esté haciendo la acción y sea en el día que está haciendo la acción,
//auqnue se me ocurren dudas, pq un usuari puede relaizar la acción un día y el trabajo lo haya hecho otro día distinto y haya imputado ese otro día
//por loq ue no sé si sólo controlar que por lo menos exista un workload del usuario que esté realizando la acción en ese momento, independientemente de la fecha que
//tenga ese worklog
for(Worklog worklog : logsForIssue)
{
   log.info("Tiempo: ")
   log.info(worklog.getAuthorObject().getDisplayName()) 
   log.info(worklog.getTimeSpent())
   log.info(worklog.getCreated().getDateString())
   log.info(worklog.getCreated().getDateTimeString())
   log.info(worklog.getCreated().getDay())   
   log.info "ANGELES: ValidarSiRellenoTimeSpent y userLogado.getName(): " + userLogado.getName()
        
   log.info "ANGELES: ValidarSiRellenoTimeSpent y worklog.getAuthorObject().getName(): " + worklog.getAuthorObject().getName()

    //se comprueba que exista una imputacion del usuario logado
    if(userLogado.getName().equalsIgnoreCase(worklog.getAuthorObject().getName())){
        log.info("Existe una imputacion del usuario logado")
        return true
    } 
}

log.info "ANGELES: ValidarSiRellenoTimeSpent y devuelvo FALSE "
//en otro caso si ha terminado el bucle es que no existe ninguna imputacion dle usuario
return false

