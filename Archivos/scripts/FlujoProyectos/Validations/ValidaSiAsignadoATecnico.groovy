import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser


//cogemos el campo VN: Técnico Asignado 
Collection tecnicoCustomField = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("VN: Técnico Asignado");
CustomField tecnicoCF = tecnicoCustomField[0]

def tecnicoCFobjectEle = issue.getCustomFieldValue(tecnicoCF) as ApplicationUser

if (! tecnicoCFobjectEle) {
    // si esta vacio el tecnico
    return false
}
return true