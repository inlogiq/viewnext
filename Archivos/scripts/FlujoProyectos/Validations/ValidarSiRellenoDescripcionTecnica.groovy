import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()

log.error "ENTRAMOS EN ValidarSiRellenoDescripcionTecnica: "
Collection customSolucion = customFieldManager.getCustomFieldObjectsByName("VN: Descripción Solución Técnica")
CustomField solucionTecnicaCf = customSolucion[0]

log.error "ENTRAMOS EN ValidarSiRellenoDescripcionTecnica: solucionTecnicaCf: " + solucionTecnicaCf 
String solucionTecnicaValue = issue.getCustomFieldValue(solucionTecnicaCf)

log.error "solucionTecnicaValue: " + solucionTecnicaValue

def regularExpression = "^.*[a-zA-Z0-9ÁÉÍÓÚáéíóúÑñ\\n\\r]+.*"

//if(solucionTecnicaValue == null || solucionTecnicaValue.equals("") || solucionTecnicaValue.length() == 0 || solucionTecnicaValue.trim().isEmpty() || !solucionTecnicaValue.trim().matches(regularExpression)){
if(solucionTecnicaValue == null || solucionTecnicaValue.equals("") || solucionTecnicaValue.length() == 0 || solucionTecnicaValue.trim().isEmpty()){
     return false
}


//en otro caso no es obligatorio rellenar la solucion tecnica y devuelve true
return true