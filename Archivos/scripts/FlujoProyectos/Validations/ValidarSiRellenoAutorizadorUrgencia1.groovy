Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Autorizador Urgencia 1")
def myCF = cfs[0]

def idAutorizadorUrgencia1 = issue.getCustomFieldValue(myCF)

//se comprueba que ha rellenado el primer autorizador de urgencia
if(idAutorizadorUrgencia1 == null){
    return false
} 

return true