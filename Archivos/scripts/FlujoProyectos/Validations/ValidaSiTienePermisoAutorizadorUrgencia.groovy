import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.event.type.EventDispatchOption
import com.opensymphony.workflow.InvalidInputException
import com.onresolve.scriptrunner.runner.util.UserMessageUtil

def versionManager = ComponentAccessor.getVersionManager()
def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//Autorizador Urgencia 1
def myCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Urgencia 1"); 
def autorizador1Urgencia = myCF.getValue(issue)

//Autorizador Urgencia 2
def my2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Urgencia 2"); 
def autorizador2Urgencia = my2CF.getValue(issue)

//si el usuario logado es el primer o segundo autorizador de urgencia se permite realizar la accion
if (autorizador1Urgencia == userLogado || autorizador2Urgencia == userLogado){
    
    log.error "ANGELES: ValidaSiTienePermisoAutorizadorUrgencia. ================= TRUE ES autorizador de urgencia" 
    return true
} else {
        log.error "ANGELES: ValidaSiTienePermisoAutorizadorUrgencia. ================= FALSE NO  ES autorizador de urgencia" 
    return false
}
