import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.event.type.EventDispatchOption
import com.opensymphony.workflow.InvalidInputException
import com.onresolve.scriptrunner.runner.util.UserMessageUtil

//def log = Logger.getLogger(getClass())

def versionManager = ComponentAccessor.getVersionManager()
def projectComponentManager = ComponentAccessor.getProjectComponentManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def userUtil = ComponentAccessor.getUserUtil()
def issueManager = ComponentAccessor.getIssueManager();
def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

//Autorizador Suplente 1
def myCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 1"); 
def autorizador1Suplente = myCF.getValue(issue)

//Autorizador Suplente 2
def my2CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Autorizador Suplente 2"); 
def autorizador2Suplente = my2CF.getValue(issue)

if (autorizador1Suplente == userLogado || autorizador2Suplente == userLogado){
    return true
} else {
    //UserMessageUtil.error('No dispone de permiso para autorizar como suplente la petición')
    return false
}