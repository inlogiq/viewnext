import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.user.util.*
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.Issue


//Insight 
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade
import com.onresolve.scriptrunner.runner.customisers.WithPlugin
import com.onresolve.scriptrunner.runner.customisers.PluginModule

PrimerPropietario()

public Boolean PrimerPropietario(){

    @WithPlugin("com.riadalabs.jira.plugins.insight")    
    @PluginModule ObjectFacade objectFacade
     
    def issueManager = ComponentAccessor.getIssueManager() 

    // Objeto primer propietario
    def mypropietario1CF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("VN: Propietario 1");

    def objectmypropietario1CFEle = mypropietario1CF.getValue(issue)
    //siu no tiene valor el objeto propietariodevolvemos false
    if(objectmypropietario1CFEle == null){
         //UserMessageUtil.error('No existen propietarios asociados a la aplicación, contacte con el administrador del sistema')
        return false
    } else {
        //log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************PQ NO ES NULL: " + objectmypropietario1CFEle
        def userLogado = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
        //log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************: userLogado: " + userLogado
        //def myObjectBean = objectmypropietario1CFEle.getAt("0")
        
        def myIDObjectBean = objectmypropietario1CFEle[0].getId()
        //log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:ID  myObjectBean: " + myIDObjectBean
        
        def propietario1 = objectFacade.loadObjectAttributeBean(myIDObjectBean, "Propietario").getObjectAttributeValueBeans()[0] //attribute Propietario del primer propietarios
        //log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:propietario1: " + propietario1

		def propietario1Value = propietario1.getValue()
        //log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:propietario1Value: " + propietario1Value
        def userPropietarioJira = ComponentAccessor.getUserManager().getUserByKey(propietario1Value); 
        //log.error "EMPIEZA VALIDAPERMISOUSUARIUOPRIMERPROPIETARIO ********************************:userPropietarioJira: " + userPropietarioJira
        if(userPropietarioJira.getName() == userLogado.getName()){
            log.error "TERMINA ES EL MISMO USUARIO *******DEVUELVE TRUE"
             return true
        } else {
             log.error "TERMINA NO ES EL MISMO USUARIO *******DEVUELVE FALSE"
            //UserMessageUtil.error('No tiene permiso para autorizar la aplicacion, ya que no es el primer porpietario.')
        	return false 
        }   
         
    }
    
}    

