log.error "ENTRAMOS EN ValidarSiRellenoDescripcionMotivoCierre "
Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Descripción Motivo Cierre")
def myCF = cfs[0]

String idMotivoValue = issue.getCustomFieldValue(myCF)

//def idMotivoValue = idMotivo.getValue(issue);
//log.error("idMotivoValue: " + idMotivoValue)
def regularExpression = "^.*[a-zA-Z0-9ÁÉÍÓÚáéíóúÑñ\\n\\r]+.*"

//if(idMotivoValue == null || idMotivoValue == "" || idMotivoValue.length() == 0 || idMotivoValue.trim().isEmpty() || !idMotivoValue.trim().matches(regularExpression)){
if(idMotivoValue == null || idMotivoValue == "" || idMotivoValue.length() == 0 || idMotivoValue.trim().isEmpty()){    
    return false
} 
return true
    