Collection cfs = customFieldManager.getCustomFieldObjectsByName("VN: Motivo de Test No OK")
def myCF = cfs[0]

String idMotivoValue = issue.getCustomFieldValue(myCF)

log.error "ENTRAMOS EN ValidarSiRellenoMotivoSubidaTestKO idMotivoValue:  " + idMotivoValue
def regularExpression = "^.*[a-zA-Z0-9ÁÉÍÓÚáéíóúÑñ\\n\\r]+.*"

//String idMotivoSin = idMotivoValue.trim()
//log.error "calculamos el resultado de aplicarle trim "+ idMotivoSin
log.error "calculamos el resultado de aplicarle la expresion regular "+ idMotivoValue.matches(regularExpression)
//|| !idMotivoValue.trim().matches(regularExpression)

//if(idMotivoValue == null || idMotivoValue == "" || idMotivoValue.length() == 0 || idMotivoValue.trim().isEmpty() || !idMotivoValue.trim().matches(regularExpression)){
if(idMotivoValue == null || idMotivoValue == "" || idMotivoValue.length() == 0 || idMotivoValue.trim().isEmpty() ){    
    return false
} 
return true