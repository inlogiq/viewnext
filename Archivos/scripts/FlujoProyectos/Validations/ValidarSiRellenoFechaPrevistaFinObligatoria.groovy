import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.Issue;
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField

def customFieldManager = ComponentAccessor.getCustomFieldManager()
def issueManager = ComponentAccessor.getIssueManager();


Collection customFechaPrevistaFin = customFieldManager.getCustomFieldObjectsByName("VN: Fecha Prevista Fin")
CustomField fechaprevistaFinCf = customFechaPrevistaFin[0]

//obtenemos el valor de la fecha previstoa de inicio de la tarea
Date fechaprevistaFinValue = issue.getCustomFieldValue(fechaprevistaFinCf)


if(fechaprevistaFinValue == null){
     //UserMessageUtil.error('Es obligatorio rellenar la Fecha Prevista de Inicio y Fin de la tarea')
    // invalidInputException = new InvalidInputException("Es obligatorio rellenar la Fecha Prevista de Inicio de la tarea");
     return false
}


//en otro caso se ha rellenado y devuelve true
return true