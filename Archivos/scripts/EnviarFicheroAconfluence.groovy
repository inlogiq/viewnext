import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.jira.issue.Issue
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import com.atlassian.jira.component.ComponentAccessor
//adjuntar
import com.atlassian.sal.api.net.RequestFilePart

def ApplicationLink getPrimaryConfluenceLink() {
    def applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService.class)
    final ApplicationLink conflLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class)
    conflLink
}

def issue = ComponentAccessor.getIssueManager().getIssueObject("PRUEBIQ-9239")
def confluenceLink = getPrimaryConfluenceLink()
assert confluenceLink

def authenticatedRequestFactory = confluenceLink.createImpersonatingAuthenticatedRequestFactory()

def pageTitle = issue.key + " Discussion"
def pageBody = """h3. ${issue.summary}

{quote}${issue.description}{quote}

Yada yada, use this page to discuss the above...
"""

def params = [
    type : "page",
    title: pageTitle,
    space: [
        key: "SLAG"
    ],
    /* // if you want to specify create the page under another, do it like this:
     ancestors: [
         [
             type: "page",
             id: "14123220",
         ]
     ],*/
    body : [
        storage: [
            value         : pageBody,
            representation: "wiki"
        ],
    ],
]
//busco archivo adjunto
def pathName = "/var/atlassian/application-data/jira/export/Saras_Sla_Global_parte_Final_2020-11-01_2020-11-30.csv"
File fileUpload = new File(pathName)
assert fileUpload
def part = new RequestFilePart(fileUpload, "file")
assert part
def partList = new ArrayList<RequestFilePart>()
partList.add(part)
assert partList
assert partList.size() == 1

authenticatedRequestFactory
    .createRequest(Request.MethodType.POST, "rest/api/content/1933507/child/attachment")
    .addHeader("X-Atlassian-Token", "nocheck")
    //.setRequestBody(new JsonBuilder(params).toString())
	//.setEntity(entityBuilder.build())
	.setFiles(partList)
    .execute(new ResponseHandler<Response>() {
        @Override
        void handle(Response response) throws ResponseException {
            if (response.statusCode != HttpURLConnection.HTTP_OK) {
                throw new Exception(response.getResponseBodyAsString())
            } else {
                def webUrl = new JsonSlurper().parseText(response.responseBodyAsString)["_links"]["webui"]
            }
        }
})