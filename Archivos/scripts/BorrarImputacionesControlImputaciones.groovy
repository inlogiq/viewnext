//Import imputaciones
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.Issue;
import java.text.SimpleDateFormat;
import java.util.Date.*;
import com.atlassian.jira.util.json.JSONObject;
import com.onresolve.scriptrunner.runner.util.UserMessageUtil
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl2;
//Insight 
import com.atlassian.jira.user.ApplicationUser
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
//fechas 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


log.warn("Imputa: "+FechaInRange(event.getWorklog().getStartDate(),event.getIssue()))
if(!FechaInRange(event.getWorklog().getStartDate(),event.getIssue())){
    def error= "Imputación incorrecta"
    def currentUser =ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    def worklog = new WorklogImpl2(event.getIssue(), event.getWorklog().getId(), event.getWorklog().getAuthor(), error, event.getWorklog().getStartDate(), null, null,event.getWorklog().getTimeSpent(),currentUser.getUsername(),event.getWorklog().getCreated(),event.getWorklog().getUpdated(), null)
    def worklogManager = ComponentAccessor.getWorklogManager();
    
    if(worklogManager.delete(issue.reporter, worklog, 0L, false)){
        UserMessageUtil.error('La imputación no ha podido guardarse debido a que la fecha indicada no está habilitada para el usuario: '+currentUser.getUsername()+', comuniquese con su administrador.')
    }else{
        UserMessageUtil.error("Ha ocurrido un error en la imputación")
    }
}

public boolean FechaInRange(Date fechaActual, Issue issue){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    //usuario actual 
    //def issue = ComponentAccessor.getIssueManager().getIssueObject('SAMSSAP-870')
    def currentUser =ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    def nombreProyecto = issue.getProjectObject().getName() 
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "Asignaciones" AND Usuario="'+currentUser.getEmailAddress()+'" AND Proyecto="'+nombreProyecto+'"');    
    
    def inicio, fin   
    if(resultados != null && resultados.size()!=0){
        
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
        
        inicio = objectFacade.loadObjectAttributeBean(resultados[0].getId(), 17459);
        inicio = inicio.getObjectAttributeValueBeans()[0];
        
        fin = objectFacade.loadObjectAttributeBean(resultados[0].getId(), 17460);
        fin = fin.getObjectAttributeValueBeans()[0];
        
        Date InicioC = sdformat.parse(inicio.getValue().toLocalDate().toString());
        Date FinC = sdformat.parse(fin.getValue().toLocalDate().toString());
        //Date fechaActual = new Date();        
        //seteo la hora        
        fechaActual.setHours(0);
        fechaActual.setMinutes(0);
        fechaActual.setSeconds(0);
        if(fechaActual.toString()==InicioC.toString() || fechaActual.after(InicioC)==true && (fechaActual.toString()==FinC.toString() || fechaActual.before(FinC)==true)){
            //si está en el rango del contrato            
            //Ahora consulto si está en el rango de retrasos
            def retrasos = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "Cliente" AND Proyectos="'+nombreProyecto+'"');    
            def Nretrasos= objectFacade.loadObjectAttributeBean(retrasos[0].getId(), 17688);
            String fechaFinalSeteada
            Date FechaRetrasos
            if(Nretrasos!=null){
                Nretrasos = Nretrasos.getObjectAttributeValueBeans()[0];
                Calendar calendar = Calendar.getInstance();
                def nuevaFecha=fechaActual
                calendar.setTime(nuevaFecha)
                calendar.add(Calendar.DAY_OF_MONTH, -Integer.parseInt(Nretrasos.value))
                def dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
                Date fechaSeteada= calendar.getTime()
                fechaFinalSeteada= dateFormat.format(fechaSeteada)
                FechaRetrasos= dateFormat.parse(fechaFinalSeteada)
                
            }else{
                def dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
                fechaFinalSeteada= dateFormat.format(fechaActual)
                FechaRetrasos= dateFormat.parse(fechaFinalSeteada)

            }
            
            if(fechaActual.toString()==FechaRetrasos.toString() || FechaRetrasos.before(fechaActual)==true){
                return true
            }else{
                return false
            } 
        }else{
            return false
        }        
        
    }
    return true
}

//retrasos
//consultar retrasos
    def retrasos = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "Cliente" AND Proyectos="'+nombreProyecto+'"');    
    def Nretrasos= objectFacade.loadObjectAttributeBean(retrasos[0].getId(), 17688);
    Nretrasos = Nretrasos.getObjectAttributeValueBeans()[0];
    Calendar calendar = Calendar.getInstance();
    def nuevaFecha=fechaActual
    calendar.setTime(nuevaFecha)
    calendar.add(Calendar.DAY_OF_MONTH, -Integer.parseInt(Nretrasos.value))
    def dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
    Date fechaSeteada= calendar.getTime()
    String fechaFinalSeteada= dateFormat.format(fechaSeteada)
    log.warn("Fecha "+fechaFinalSeteada)
    