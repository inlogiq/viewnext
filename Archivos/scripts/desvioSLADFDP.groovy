import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade
@WithPlugin("com.atlassian.servicedesk")
import java.util.concurrent.TimeUnit
import com.onresolve.scriptrunner.canned.jira.admin.CopyProject
import  java.util.Date.*
import com.atlassian.jira.issue.index.IssueIndexingService
import java.text.SimpleDateFormat
import java.sql.Timestamp
import java.time.DayOfWeek
import java.io.*;
import java.util.*;

def customFieldManager = ComponentAccessor.getCustomFieldManager()
def issue = (MutableIssue) issue
def userManager = ComponentAccessor.getUserManager()
def currentUser = userManager.getUserByName("admin");
def issueManager = ComponentAccessor.getIssueManager()

//Nombre del proyecto y fecha del dia en el que se hace la transicion
def currentProject = issue.projectObject.name
def fechaHoy = new java.sql.Timestamp(new Date().getTime())
log.warn("Proyecto: "+ currentProject)




//Fecha fin: la fecha prevista de entrega
def fechaFin = (ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Fin Pre DF/DP").getValue(issue) as Timestamp).toLocalDateTime().toLocalDate()
//  Fecha de inicio: fecha desde la que se empieza a contar, en este caso el paso al estado pendiente de aprobacion
def fechaInicio = (fechaHoy as Timestamp).toLocalDateTime().toLocalDate() 
//definicion de los dias no hábiles
def weekend = EnumSet.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY)
//variable para guardar los dias de servicio
def diasDeServicio = 0

//Mientras que fecha inicio sea menor que fecha fin, si no es fin de semana, suma 1 al dia de servicio
while (fechaInicio.isBefore(fechaFin)) {
    if (!(fechaInicio.dayOfWeek in weekend)) {
        diasDeServicio += 1
    }

    fechaInicio = fechaInicio.plusDays(1)
}
//Campos para guardar valores de insight
def valorProyecto
def horas
def desvio
//Consulta a Insight por el cliente
def proyectoIN = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = \"Proyecto\" AND \"proyectoJira\" = \"" + currentProject + "\" ")
//Valor del campo cliente
def proyectoCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Proyecto").getValue(issue)
log.warn("Cliente Insight: "+ proyectoIN)
log.warn("Cliente CF: "+ proyectoCF)

//Si hay algo en el campo cliente usamos eso, si no, hacemos la busqueda de insight
if(proyectoCF != null){
    valorProyecto = proyectoCF
}else if(proyectoIN != null){
    valorProyecto = proyectoIN
}

//Si el cliente no es null, sacamos sus atributos de fechas y porcentaje de desvio. Si no tienen, por defecto 8h y 0%
if(valorProyecto[0] != null){
    horas = objectFacade.loadObjectAttributeBean(valorProyecto[0].getId(), 17868);//Id del atributo horas
    if(horas!=null){
        horas = horas.getObjectAttributeValueBeans()[0];
        horas = horas.getValue()
        log.warn("Horas de servicio: "+horas)
    }else{
        horas = 8
    }

    desvio = objectFacade.loadObjectAttributeBean(valorProyecto[0].getId(), 17870);//Id del atributo desvio
    if(desvio!=null){
        desvio = desvio.getObjectAttributeValueBeans()[0];
        desvio = desvio.getValue()
        log.warn("Porcentaje de desvio: "+desvio)
    }else{
        desvio = 0
    }

}



//campo de TTS donde guardamos el tiempo objetivo
def campoDesvio = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("SLAConDesvioDFDP")

//Los dias de servicio son los dias entre las dos fechas sin contar los fines de semana
log.warn("Dias de servicio: " + diasDeServicio)
//Las horas de servicio totales seran las horas por dia (variable horas) x los días de servicio
def horasDeServicio = diasDeServicio * horas
log.warn("Horas de servicio: " + horasDeServicio)

//Las horas con desvío seran las horas de servicio mas el porcentaje de desvío indicado en cmdb
def horasConDesvío = (horasDeServicio + (horasDeServicio*desvio)/100)
log.warn("Horas con desvio: " + horasConDesvío)

//Como el campo de TTS pide un String, formateamos las horas con desvio al formato h-m usando la funcion formatearHoras
def horasfinal = formatearHoras(horasConDesvío.toString())
log.warn(horasfinal)

//Guardamos el valor y actualizamos la issue
issue.setCustomFieldValue(campoDesvio,horasfinal)
issueManager.updateIssue(currentUser, issue, EventDispatchOption.DO_NOT_DISPATCH, false)





//separa las horas en entero y decimal y convierte el decimal en minutos para ponerlo en el formato correcto (ej.: 3h 17m)
public String formatearHoras(String time){    
    def valor= time.split("\\.")    
    if(valor.size()>1){
        int min = Integer.parseInt(valor[1]);
        if(min>0){
            BigDecimal multiplicacion= Math.pow(10, valor[1].size());
            def minutos = (min*60)/multiplicacion
            return valor[0]+"h"+" "+Math.round(minutos)+"m"
        }else{

            return valor[0]+"h";
        }
    }else{
        return time+"h";
    }
}