import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface
import groovy.sql.Sql
import java.sql.Connection
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser
import java.nio.file.Files
import java.nio.file.Paths
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.DecimalFormat;
import com.atlassian.jira.issue.*;
//Insight 
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade
import com.riadalabs.jira.plugins.insight.services.model.factory.ObjectAttributeBeanFactory
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
//isssues rechazadas
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
//confluence
import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import com.atlassian.sal.api.net.RequestFilePart

def ApplicationLink getPrimaryConfluenceLink() {
    def applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService.class)
    final ApplicationLink conflLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class)
    conflLink
}
//consulto los Clientes Estandar
def ClientesIterar= ClientesProyectos()
//itero sobre cada uno, para generar su informe de sla global.
for(int i=0;i<ClientesIterar.size();i++){
    
    //Informe
    GenerarInforme(ClientesIterar[i].name)
    //envio a confluence
    
    def confluenceLink = getPrimaryConfluenceLink()
    assert confluenceLink
    def authenticatedRequestFactory = confluenceLink.createImpersonatingAuthenticatedRequestFactory()
    File fileUpload = new File(ruta(ClientesIterar[i].name))
    assert fileUpload
    def part = new RequestFilePart(fileUpload, "file")
    assert part
    def partList = new ArrayList<RequestFilePart>()
    partList.add(part)
    assert partList
    assert partList.size() == 1
    authenticatedRequestFactory
    .createRequest(Request.MethodType.POST, "rest/api/content/"+RutaConfluence(ClientesIterar[i].name)+"/child/attachment")
    .addHeader("X-Atlassian-Token", "nocheck")
    .setFiles(partList)
    .execute(new ResponseHandler<Response>() {
        @Override
        void handle(Response response) throws ResponseException {
            if (response.statusCode != HttpURLConnection.HTTP_OK) {
                throw new Exception(response.getResponseBodyAsString())
            } else {
                def webUrl = new JsonSlurper().parseText(response.responseBodyAsString)["_links"]["webui"]
            }
        }
    })
    
}
public List ClientesProyectos(){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = SlaGlobal AND Estatus=TRUE");    
    if(resultados.size()>0){
         return resultados
    }
}
public String GenerarInforme(String cliente){
    
    //Variables del informe
    def indicadores= Indicadores(cliente)
    def servicios = Servicio(cliente)
    def prioridad= Prioridad(cliente)
    def CGlobal= CumplimientoGlobal(cliente)
    def peso= Peso(cliente)
    def slaId= SlaId(cliente)    
    def slaIdMigrado= SlaIdMigrado(cliente)
    def filePath = ruta(cliente)
    new File(filePath).createNewFile()
    PrintWriter writer = new PrintWriter(filePath, "UTF-8")    
    writer.println(Cabecera())
    for(int i=0; i<indicadores.size();i++){
        def totalSla
        def separarResultados
        def totalSlaMigrado
        def separarResultadosMigrado
        int TotalS
        int ok
        int ko
        //pregunto si este cliente tiene activo consultar sla de migración
        if(HasMigration(cliente)){
            //SLA orgánicos
            totalSla= TotalSlaSql(slaId[i],indicadores[i],cliente)
        	separarResultados = totalSla.split("-");
        	
            //SLA migrados
            totalSlaMigrado=TotalSlaMigrados(slaIdMigrado[i],servicios[i],prioridad[i],cliente)
            separarResultadosMigrado = totalSlaMigrado.split("-");
            
            TotalS=  (Integer.parseInt(separarResultados[3])+Integer.parseInt(separarResultadosMigrado[3]));
            ok=(Integer.parseInt(separarResultados[1])+Integer.parseInt(separarResultadosMigrado[1]))
            ko=(Integer.parseInt(separarResultados[2])+Integer.parseInt(separarResultadosMigrado[2]))
        }else{
            totalSla= TotalSlaSql(slaId[i],indicadores[i],cliente)
        	separarResultados = totalSla.split("-");
        	TotalS=  Integer.parseInt(separarResultados[3]);
            ok=Integer.parseInt(separarResultados[1])
            ko=Integer.parseInt(separarResultados[2])
        }
        
        if(TotalS==0){
            //el porcentaje de cumplimiento es 100% y la aportación el mismo valor del peso
            writer.println(servicios[i]+";"+indicadores[i]+";"+prioridad[i]+";"+CGlobal[i]+"%;"+peso[i]+"%;100%;"+peso[i]+"%;"+TotalS+";"+ok+";"+ko)
        }else{
            BigDecimal calaculo_success= ((100*ok)/TotalS)            
        	int calculoIndividual= Integer.valueOf (calaculo_success.intValue())
            //int cumple = Integer.parseInt(separarResultados[0]);        
            //aportación
            int peso_entero = Integer.parseInt(peso[i]); 
            int ponderacion=  Integer.parseInt(CGlobal[i]);

            BigDecimal aporte=((calaculo_success*peso_entero)/ponderacion)        
            writer.println(servicios[i]+";"+indicadores[i]+";"+prioridad[i]+";"+CGlobal[i]+"%;"+peso[i]+"%;"+calculoIndividual+"%;"+aporte.round(2)+"%;"+TotalS+";"+ok+";"+ko)
        }
        
    }
    writer.close()
    //para adjunatr en la incidencia
    //uploadAttachment(FechaDesde(),FechaHasta(),NombreFichero(cliente))
    
}
//ruta servidor
public String ruta(String Cliente){
    return "/var/atlassian/application-data/jira/export/"+NombreFichero(Cliente)+"_"+Fechas("FROM")+"_"+Fechas("TO")+".csv"
}
//cabecera archivo
public String Cabecera(){
    return "Servicio;Indicador;Prioridad;Objetivo de complimiento Global;Peso;Cumplimiento individual;Aportacion;Totales;OK;KO"
}
//indicadores
public String[] Indicadores(String ObjectType){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    @PluginModule ObjectAttributeBeanFactory objectAttributeBeanFactory
	@PluginModule ObjectTypeAttributeFacade objectTypeAttributeFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "'+ObjectType+'"');    
    try {
        def ind
        //variables
        String[] indicadores= new String[resultados.size()];
        if(resultados != null){  
            //obtengo el id del atributo
            def attributeID = resultados[0].objectAttributeBeans.find{
                objectTypeAttributeFacade.loadObjectTypeAttributeBean(it.objectTypeAttributeId).name == "Indicador"
            }
        	int atributo= attributeID.objectTypeAttributeId
            for(int i=0;i<resultados.size();i++){
                ind = objectFacade.loadObjectAttributeBean(resultados[i].getId(), atributo);
                ind = ind.getObjectAttributeValueBeans()[0];           
                indicadores[i]=ind.getValue()


            } 
            return indicadores
        }
    }catch (SearchException e) {
        String[] indicadores= new String[1];
        log.warn("Ha ocurrido el siguiente error en los indicadores "+e)
        return indicadores
    }
}
//servicios
public String[] Servicio(String ObjectType){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    @PluginModule ObjectAttributeBeanFactory objectAttributeBeanFactory
	@PluginModule ObjectTypeAttributeFacade objectTypeAttributeFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "'+ObjectType+'"');
    try {
        
        def Serv
        String[] servicio= new String[resultados.size()];
        if(resultados != null){  
            //obtengo el id del atributo
            def attributeID = resultados[0].objectAttributeBeans.find{
                objectTypeAttributeFacade.loadObjectTypeAttributeBean(it.objectTypeAttributeId).name == "Servicio"
            }
        	int atributo= attributeID.objectTypeAttributeId
            for(int i=0;i<resultados.size();i++){
                Serv = objectFacade.loadObjectAttributeBean(resultados[i].getId(), atributo);
                Serv = Serv.getObjectAttributeValueBeans()[0];           
                servicio[i]=Serv.getValue()


            } 
            return servicio
        }
    }catch (SearchException e) {
         String[] servicio= new String[1];
         log.warn("Ha ocurrido el siguiente error en los servicios"+e)
        return servicio
     }
}
//prioridades
public String[] Prioridad(String ObjectType){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    @PluginModule ObjectAttributeBeanFactory objectAttributeBeanFactory
	@PluginModule ObjectTypeAttributeFacade objectTypeAttributeFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "'+ObjectType+'"');
    try {
        def pri
        String[] prioridad= new String[resultados.size()];
        if(resultados != null){
            //obtengo el id del atributo
            def attributeID = resultados[0].objectAttributeBeans.find{
                objectTypeAttributeFacade.loadObjectTypeAttributeBean(it.objectTypeAttributeId).name == "Prioridad"
            }
        	int atributo= attributeID.objectTypeAttributeId
            for(int i=0;i<resultados.size();i++){
                pri = objectFacade.loadObjectAttributeBean(resultados[i].getId(), atributo);
                pri = pri.getObjectAttributeValueBeans()[0];           
                prioridad[i]=pri.getValue()


            }
            return prioridad
        }
    }catch (SearchException e) {
        String[] prioridad= new String[1];
        log.warn("Ha ocurrido el siguiente error en las prioridades "+e)
        return prioridad
    }
}

//Cumplimiento
public String[] CumplimientoGlobal(String ObjectType){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    @PluginModule ObjectAttributeBeanFactory objectAttributeBeanFactory
	@PluginModule ObjectTypeAttributeFacade objectTypeAttributeFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "'+ObjectType+'"');
    try {
        def cump
        String[] Cumplimiento= new String[resultados.size()];
        if(resultados != null){ 
            //obtengo el id del atributo
            def attributeID = resultados[0].objectAttributeBeans.find{
                objectTypeAttributeFacade.loadObjectTypeAttributeBean(it.objectTypeAttributeId).name == "CumplimientoGlobal"
            }
        	int atributo= attributeID.objectTypeAttributeId
            for(int i=0;i<resultados.size();i++){
                cump = objectFacade.loadObjectAttributeBean(resultados[i].getId(), atributo);
                cump = cump.getObjectAttributeValueBeans()[0];           
                Cumplimiento[i]=cump.getValue()

            }
            return Cumplimiento
        }
    }catch (SearchException e) {
        String[] Cumplimiento= new String[1];
        log.warn("Ha ocurrido el siguiente error en el CumplimientoGlobal "+e)
        return Cumplimiento
    }
}
//Peso
public String[] Peso(String ObjectType){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    @PluginModule ObjectAttributeBeanFactory objectAttributeBeanFactory
	@PluginModule ObjectTypeAttributeFacade objectTypeAttributeFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "'+ObjectType+'"');
    try {
        def pes
        String[] Peso= new String[resultados.size()];
        if(resultados != null){  
            //obtengo el id del atributo
            def attributeID = resultados[0].objectAttributeBeans.find{
                objectTypeAttributeFacade.loadObjectTypeAttributeBean(it.objectTypeAttributeId).name == "Peso"
            }
        	int atributo= attributeID.objectTypeAttributeId
            for(int i=0;i<resultados.size();i++){
                pes = objectFacade.loadObjectAttributeBean(resultados[i].getId(), atributo);
                pes = pes.getObjectAttributeValueBeans()[0];           
                Peso[i]=pes.getValue()

            }
            return Peso
        }
    }catch (SearchException e) {
        String[] Peso= new String[1];
        log.warn("Ha ocurrido el siguiente error en el peso "+e)
        return Peso
    }
}

//ID SLA
public String[] SlaId(String ObjectType){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    @PluginModule ObjectAttributeBeanFactory objectAttributeBeanFactory
	@PluginModule ObjectTypeAttributeFacade objectTypeAttributeFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "'+ObjectType+'"');
    try {
        def sla
        String[] Sla_id= new String[resultados.size()];
        if(resultados != null){   
            //obtengo el id del atributo
            def attributeID = resultados[0].objectAttributeBeans.find{
                objectTypeAttributeFacade.loadObjectTypeAttributeBean(it.objectTypeAttributeId).name == "Sla_id"
            }
            int atributo= attributeID.objectTypeAttributeId
            for(int i=0;i<resultados.size();i++){
                sla = objectFacade.loadObjectAttributeBean(resultados[i].getId(), atributo);
                sla = sla.getObjectAttributeValueBeans()[0];           
                Sla_id[i]=sla.getValue()

            }
            return Sla_id
        }
    }catch (SearchException e) {
        String[] Sla_id= new String[1];
        log.warn("Ha ocurrido el siguiente error en los SLA "+e)
        return Sla_id
    }
}
public String[] SlaIdMigrado(String ObjectType){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    @PluginModule ObjectAttributeBeanFactory objectAttributeBeanFactory
	@PluginModule ObjectTypeAttributeFacade objectTypeAttributeFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = "'+ObjectType+'"');
    try {
        def sla
        String[] Sla_id= new String[resultados.size()];
        if(resultados != null){   
            //obtengo el id del atributo
            def attributeID = resultados[0].objectAttributeBeans.find{
                objectTypeAttributeFacade.loadObjectTypeAttributeBean(it.objectTypeAttributeId).name == "Sla_id_migracion"
            }
            int atributo= attributeID.objectTypeAttributeId
            for(int i=0;i<resultados.size();i++){
                sla = objectFacade.loadObjectAttributeBean(resultados[i].getId(), atributo);
                if(sla!=null){
                    sla = sla.getObjectAttributeValueBeans()[0];           
                    Sla_id[i]=sla.getValue()
                }else{
                    Sla_id[i]=""
                }

            }
            return Sla_id
        }
    }catch (SearchException e) {
        String[] Sla_id= new String[1];
        log.warn("Ha ocurrido el siguiente error en los SLA "+e)
        return Sla_id
    }
}
public String TotalSlaSql(String ID, String indicador,String Cliente){
    
    if(ID!="" && ID!=null && ID!="null" ){
        
        def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
        String helperName = delegator.getGroupHelperName("default")
        def objectAttrId;
        Connection conn = ConnectionFactory.getConnection(helperName)
        Sql sql = new Sql(conn)
        def sqlStmt2 = 'SELECT "AO_C5D949_TTS_ISSUE_SLA".* FROM "jiraissue" join "AO_C5D949_TTS_ISSUE_SLA" on("jiraissue".id="AO_C5D949_TTS_ISSUE_SLA"."ISSUE_ID") where \"SLA_ID\" in('+ID+') and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"resolutiondate\" >=\''+Fechas("FROM")+'\' AND \"resolutiondate\" <=\''+Fechas("TO")+'\' AND "jiraissue".issuestatus in(\'10010\',\'10012\') AND "jiraissue".project in('+IdProyecto(Cliente)+')'
        StringBuffer sb2 = new StringBuffer()
        int i=0
        sql.eachRow(sqlStmt2){
            i++
        }
        String[] solicitud= new String[i];
        String[] sla_id= new String[i];
        String[] description= new String[i];
        int j=0
        sql.eachRow(sqlStmt2){

            solicitud[j]= it.INDICATOR;
            sla_id[j] = it.SLA_ID;
            description[j] = it.ISSUE_KEY;
            j++
        }
        int success=0
        int exceed=0
        int cont_prioridad=0

        for(int k=0;k<solicitud.size();k++){

            if(ExluirSla(description[k])!='Si'){           

                if(solicitud[k]=="SUCCESS"){
                    success++
                }else{
                    exceed++ 
                }
                cont_prioridad++

            }
        }
        sql.close()
        if(cont_prioridad!=0){
            BigDecimal calaculo_success= ((100*success)/cont_prioridad)            
            int calculoIndividual= Integer.valueOf (calaculo_success.intValue())
            return calculoIndividual+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }
        
    }else{
        
        //rechazadas
        if(indicador=="Incidencias Rechazadas"){
            //Servicio calidad de entrega
            int rechazos= rechazos(NombreProyecto(Cliente))            
            int totalIssues= totalIncidencias(NombreProyecto(Cliente))
            int totalSinRechazos=(totalIssues-rechazos)	
            int calculoIndividual
            if(totalIssues==0){
                calculoIndividual=100
            }else{
                BigDecimal calaculo_success= ((100*totalSinRechazos)/totalIssues)
            	calculoIndividual= Integer.valueOf (calaculo_success.intValue())
            }            
            return calculoIndividual+"-"+totalSinRechazos+"-"+rechazos+"-"+totalIssues
        }
        //Enlazadas
        if(indicador=="Incidencias Colaterales"){
            //Servicio calidad de entrega
            int Enlazadas= enlazadasKO(NombreProyecto(Cliente))           
            int totalIssues= enlazadas(NombreProyecto(Cliente))
            int totalSinEnlaces=(totalIssues-Enlazadas)
            int calculoIndividual
            if(totalIssues==0){
                calculoIndividual=100
            }else{
                BigDecimal calaculo_success= ((100*totalSinEnlaces)/totalIssues)
            	calculoIndividual= Integer.valueOf (calaculo_success.intValue())
            }            
            return calculoIndividual+"-"+totalSinEnlaces+"-"+Enlazadas+"-"+totalIssues           
        }
        // por defecto retorna 0
        return "0-0-0-0"
        
    }
}
public String IdProyecto(String Cliente){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name= "'+Cliente+'"');
    
    def project
    if(resultados != null){   
        String Valor
        for(int i=0;i<resultados.size();i++){            
            project = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17401);  

            for(int j=0;j<project.getObjectAttributeValueBeans().size();j++){   
                if(j==0){
                    Valor=project.getObjectAttributeValueBeans()[j].getValue()
                }else{ 
                    Valor+=","+project.getObjectAttributeValueBeans()[j].getValue()
                }
            }	

        }
        return Valor
    }else{
        //retorno el id de Makro - AMS SAP
        return "11105"
    }
    
}

//Nombre proyecto
public String NombreProyecto(String Cliente){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name= "'+Cliente+'"');    
    def project
    if(resultados.size()>0){        
        project = objectFacade.loadObjectAttributeBean(resultados[0].getId(), 17401);             
        long idProyecto = project.getObjectAttributeValueBeans()[0].getValue()            
        def NombreProyecto= ComponentAccessor.getProjectManager().getProjectObj(idProyecto)
        
        return NombreProyecto.getName()
    }
}
//Nombre del fichero
public String NombreFichero(String Cliente){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name= "'+Cliente+'"');
    def fichero
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){            
            fichero = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17402);
            fichero = fichero.getObjectAttributeValueBeans()[0];           
         }
        return fichero.getValue()
    }else{
        //retorno un nombre predeterminado
       return "SlaGlobal"
    }  
}
public String ExluirSla(String key){
    
    def ComponentIssue= ComponentAccessor.getIssueManager()
    def customFieldManager = ComponentAccessor.getCustomFieldManager()
    def excluir = customFieldManager.getCustomFieldObject(12735)
    
    Issue issue= ComponentIssue.getIssueObject(key)
    
    return issue.getCustomFieldValue(excluir).toString()
    
    
}

static void uploadAttachment(String FechaDesde,String FechaHasta, String Nombre){    

    ApplicationUser user = ComponentAccessor.getUserManager().getUserByName("admin")
    def issue = ComponentAccessor.getIssueManager().getIssueObject("PRUEBIQ-9239")
    File archivo = new File("/var/atlassian/application-data/jira/export/"+Nombre+"_"+FechaDesde+"_"+FechaHasta+".csv")
    def attachmentManager = ComponentAccessor.getAttachmentManager()
    def bean = new CreateAttachmentParamsBean.Builder().copySourceFile(true)
    .file(archivo)
    .filename(archivo.name)
    .author(user)
    .issue(issue)
    .build()
    attachmentManager.createAttachment(bean)
}
public String Fechas(String tipo){
    
    Calendar cal = Calendar.getInstance();
    cal.add(cal.MONTH, -1)
    String fecha
    
    if(tipo=="FROM"){
        
        def dateFormat = new java.text.SimpleDateFormat("yyyy-MM");
        Date fechaSeteada= cal.getTime()
        fecha= dateFormat.format(fechaSeteada)+"-01 "+HoraDesde()
        
    }
    if(tipo=="TO"){
        def dateFormat = new java.text.SimpleDateFormat("yyyy-MM");
        Date fechaSeteada= cal.getTime()
        def fechaFormato= dateFormat.format(fechaSeteada)
        def valorMes= fechaFormato.split("-")
        fecha = fechaFormato+"-"+day(valorMes[1])+" "+HoraHasta()
    }
    
    return fecha
}
public String HoraDesde(){
    return "00:00:00"
}
public String HoraHasta(){
    return "23:59:59"
}
public String day(String dia){

	/*Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("MM");
    def dia =dateFormat.format(date)*/
    
    if(dia=='01'){
        return '31'
    }
    if(dia=='02'){
        return '28'
    }
    if(dia=='03'){
        return '31'
    }
    if(dia=='04'){
        return '30'
    }
    if(dia=='05'){
        return '31'
    }
    if(dia=='06'){
        return '30'
    }
    if(dia=='07'){
        return '31'
    }
    if(dia=='08'){
        return '31'
    }
    if(dia=='09'){
        return '30'
    }
    if(dia=='10'){
        return '31'
    }
    if(dia=='11'){
        return '30'
    }
    if(dia=='12'){
        return '31'
    }
    return '30'
}
public int rechazos(String proyecto){

    final String jqlSearch = "project in ('"+proyecto+"')  AND Rechazos > 0 and resolved > startOfMonth(-1) and resolved <=endOfMonth(-1) and issuetype not in subtaskIssueTypes() AND status in (Confirmada, Cancelada) AND ('¿Excluir SLA?' is EMPTY or '¿Excluir SLA?' = No)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}
public int totalIncidencias(String proyecto){
    
    final String jqlSearch = "project in ('"+proyecto+"') and resolved > startOfMonth(-1) and resolved <=endOfMonth(-1) and issuetype not in subtaskIssueTypes() AND status in (Confirmada, Cancelada) AND ('¿Excluir SLA?' is EMPTY or '¿Excluir SLA?' = No)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}

public int enlazadas(String proyecto){

    final String jqlSearch = "project in ('"+proyecto+"') and resolved > startOfMonth(-1) and resolved <=endOfMonth(-1) and issuetype not in subtaskIssueTypes() AND issuetype in (Correctivo,'Evolutivo Menor','Evolutivo Mayor') AND status in (Confirmada, Cancelada) AND ('¿Excluir SLA?' is EMPTY or '¿Excluir SLA?' = No)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}

public int enlazadasKO(String proyecto){

    final String jqlSearch = "project in ('"+proyecto+"') and resolved > startOfMonth(-1) and resolved <=endOfMonth(-1) and issuetype not in subtaskIssueTypes() AND issuetype in (Correctivo,'Evolutivo Menor','Evolutivo Mayor') AND status in (Confirmada, Cancelada) AND issueLinkType in (causes) AND ('¿Excluir SLA?' is EMPTY or '¿Excluir SLA?' = No)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}
public String RutaConfluence(String nombre){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name="'+nombre+'"');
    def fichero
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){            
            fichero = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17400);
            fichero = fichero.getObjectAttributeValueBeans()[0];           
         }
        return fichero.getValue()
    }else{
        //retorno un nombre predeterminado
       return "1933507"
    }  
}

//Isuue migradas
public boolean HasMigration(String Cliente){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name= "'+Cliente+'"');
    
    def migration
    if(resultados != null){   
        migration = objectFacade.loadObjectAttributeBean(resultados[0].getId(), 17678);
        if(migration!=null && migration!=""){
            return migration.getObjectAttributeValueBeans()[0].getValue()
        }else{
            return false
        }
    }else{
        return false
    }
    
}

public String TotalSlaMigrados(String ID, String Servicio,String prioridad,String Cliente){
    
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = 'SELECT "AO_C5D949_ISSUE_SLA"."BREACHED","AO_C5D949_ISSUE_SLA"."STATE","jiraissue".ID as issueId FROM "jiraissue" join "AO_C5D949_ISSUE_SLA" on("jiraissue".id="AO_C5D949_ISSUE_SLA"."ISSUE_ID") join "AO_C5D949_TTS_SLA" on("AO_C5D949_TTS_SLA"."PROXIED_SLA_ID"="AO_C5D949_ISSUE_SLA"."SLA_ID") WHERE "AO_C5D949_TTS_SLA"."ID" IN('+ID+') AND "AO_C5D949_ISSUE_SLA"."STATE"=\'STOPPED\' AND \"resolutiondate\" >=\''+Fechas("FROM")+'\' AND \"resolutiondate\" <=\''+Fechas("TO")+'\' AND "jiraissue".issuestatus in(\'10010\',\'10012\') AND "jiraissue".project in('+IdProyecto(Cliente)+')'
    //def sqlStmt2 = 'SELECT "AO_C5D949_ISSUE_SLA"."BREACHED","AO_C5D949_ISSUE_SLA"."STATE","jiraissue".ID as issueId FROM "jiraissue" join "AO_C5D949_ISSUE_SLA" on("jiraissue".id="AO_C5D949_ISSUE_SLA"."ISSUE_ID") join "AO_C5D949_TTS_SLA" on("AO_C5D949_TTS_SLA"."PROXIED_SLA_ID"="AO_C5D949_ISSUE_SLA"."SLA_ID") WHERE "AO_C5D949_TTS_SLA"."ID" IN(183)'

    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    String[] Cumple= new String[i];
    String[] IssueId= new String[i];
    int j=0
    sql.eachRow(sqlStmt2){
		
        Cumple[j]= it.BREACHED;
        IssueId[j]= it.issueId;
        j++
    }
    int success=0
    int exceed=0
    int cont_prioridad=0
	
    for(int k=0;k<Cumple.size();k++){
        //consulto si el servicio y prioridad corresponde con el de cmdb  
        if(prioridad!="All"){
            def TotalPrioridades= prioridad.split("/")
            def totalServicios=FormateandoTipoDeIssue(Servicio).split("_")
            for(int p=0;p<TotalPrioridades.size();p++){
                for(int z=0;z<totalServicios.size();z++){
                    
                    if(hasServicePriority(totalServicios[z],TotalPrioridades[p],IssueId[k],"Servicio-Prioridad")){

                    	if(Cumple[k]=="false"){
                            success++
                        }else{
                            exceed++ 
                        }
                        cont_prioridad++
                    }
                }
                
            }
        }else{
            
            long IdIssue= Long.parseLong(IssueId[k]);
            def incidencia=ComponentAccessor.getIssueManager().getIssueObject(IdIssue)
            if(Servicio=="-"){
                
                if(ExluirSla(incidencia.key)!='Si'){ 
            	
                    if(Cumple[k]=="false"){
                        success++
                     }else{
                        exceed++ 
                     }
                     cont_prioridad++
                }
            }else{
                
                def totalServicios=FormateandoTipoDeIssue(Servicio).split("-")
                for(int h=0;h<totalServicios.size();h++){
                    
                    if(hasServicePriority(totalServicios[h],prioridad[k],IssueId[k],"Servicio")){

                        if(Cumple[k]=="false"){
                            success++
                        }else{
                            exceed++ 
                        }
                        cont_prioridad++
                    }
                }               
                
            }
            
        }
        
    }
    sql.close()
    if(cont_prioridad!=0){
        BigDecimal calaculo_success= ((100*success)/cont_prioridad)            
        int calculoIndividual= Integer.valueOf (calaculo_success.intValue())
        return calculoIndividual+"-"+success+"-"+exceed+"-"+cont_prioridad
    }else{
        return "0-0-0-0-0"
    }
}
public boolean hasServicePriority(String servicio, String prioridad, String Issue_id, String TipoConsulta){
    
    final String jqlSearch = "id="+Issue_id+" AND ('¿Excluir SLA?' is EMPTY or '¿Excluir SLA?' = No)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)
                //Consulto por servicio y prioridad
                if(TipoConsulta=="Servicio-Prioridad"){
                    if(servicio=="-"){
                        if(cleanTextContent(issue.getPriority().name)==cleanTextContent(prioridad)){
                            entero++
                        }
                    }else{
                        if(servicio!="Evolutivo Mayor" && servicio!="Evolutivo Menor"){
                           	if(cleanTextContent(issue.getIssueType().name)==cleanTextContent(servicio) && cleanTextContent(issue.getPriority().name)==cleanTextContent(prioridad)){
                                entero++
                            } 
                        }else{
                            if(cleanTextContent(issue.getIssueType().name)==servicio && cleanTextContent(issue.getPriority().name)==cleanTextContent(prioridad)){
                                entero++
                            }
                        }
                        
                    }
                    
                }
                //Consulto solo servicio
                if(TipoConsulta=="Servicio"){
                    if(servicio!="Evolutivo Mayor" && servicio!="Evolutivo Menor"){
                        if(cleanTextContent(issue.getIssueType().name)==cleanTextContent(servicio)){
                            entero++
                        }
                    }else{
                        if(cleanTextContent(issue.getIssueType().name)==servicio){
                            entero++
                        }
                    }
                    
                }
                
                
            }
            if(entero>0){
                return true
            }else{
                return false
            }
            
        } catch (SearchException e) {
            return false
        }
    } else {
        
        return false
    }
    
    
}
public String cleanTextContent(String text){
    
    text = text.replaceAll("á", "a");
    text = text.replaceAll("Á", "A");    
    text = text.replaceAll("é", "e");
    text = text.replaceAll("É", "E");
    text = text.replaceAll("í", "i");
    text = text.replaceAll("Í", "I");
    text = text.replaceAll("ó", "o");
    text = text.replaceAll("Ó", "O");
    text = text.replaceAll("ú", "u");
    text = text.replaceAll("Ú", "U");
    text = text.replaceAll("ñ", "n");
    text = text.replaceAll("Ñ", "N");
    text = text.replaceAll("\"", "'");
    text = text.replaceAll("\\\\", "/");

    // strips off all non-ASCII characters
    text = text.replaceAll("[^\\x00-\\x7F]", "");
 
    // erases all the ASCII control characters
    text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");
     
    // removes non-printable characters from Unicode
    text = text.replaceAll("\\p{C}", "");

    return text.trim();
}
public String FormateandoTipoDeIssue(String tipo){
    if(tipo=="Soporte no planificado"){
        return "Soporte"
    }else if(tipo=="Evolutivo"){
        
        return "Evolutivo Mayor_Evolutivo Menor"
        
    }else{
        return tipo
    }
}