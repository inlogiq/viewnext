import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.workflow.JiraWorkflow
import com.atlassian.jira.user.util.*;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.Issue

GruposUsuarios()

public String GruposUsuarios(){
    @WithPlugin("com.riadalabs.jira.plugins.insight")
    @PluginModule ObjectFacade objectFacade
    @PluginModule IQLFacade iqlFacade
    @WithPlugin("com.atlassian.servicedesk")
    
    //def issue = ComponentAccessor.getIssueManager().getIssueObject('GESTAMP-11')
    def issue = event.issue
    def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
        
    def issueManager = ComponentAccessor.getIssueManager()
    def modulo = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Módulo").getValue(issue);    
    if(modulo!=null){  
        log.warn("Módulo "+modulo[0].name)
        def valorModulo = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = \"Módulo SAP\" AND Name = \""+modulo[0].name+"\" AND \"Proyecto\" = \"Gestamp\" ")
        
        def grupoUsers= objectFacade.loadObjectAttributeBean(valorModulo[0].getId(), 17267);
        if(grupoUsers!=null){
            int totalUsuarios= grupoUsers.getObjectAttributeValueBeans().size()
            String[] usuarios= new String[totalUsuarios];
            for(int i=0; i<totalUsuarios;i++){        
                usuarios[i]=grupoUsers.getObjectAttributeValueBeans()[i].getValue()
            }
            //guardo los usuarios
            GuardarUser(usuarios, issue)
            return "Guardado todos los usuarios"
        }else{
            //Guardo el usuario ADMIN
            UserManager userManager = ComponentAccessor.getUserManager();
    		def admin = userManager.getUserByName("admin");
            String[] usuarios= new String[1];
            usuarios[0]= admin.key
            GuardarUser(usuarios, issue)
            return "Guardado solo el Admin"
        }
        
        
    }else{
        return "No tiene Módulo"
    }
    
    
    
}

static void GuardarUser(String [] usuarios, Issue incidencia){
    def issue = incidencia
    //def issue = ComponentAccessor.getIssueManager().getIssueObject('GESTAMP-11')
    def modulo = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("destinatariosGestamp");
    UserManager userManager = ComponentAccessor.getUserManager();
    def admin = userManager.getUserByName("admin");
    
    List<ApplicationUser> users;
    users = new ArrayList<>();
    for(int i=0;i<usuarios.size();i++){
    	users.add(ComponentAccessor.getUserManager().getUserByKey(usuarios[i]))
	}	
    issue.setCustomFieldValue(modulo, users);
    ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false);

    
}
