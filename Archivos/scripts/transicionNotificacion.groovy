import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.event.type.EventDispatchOption

def workflow = ComponentAccessor.getWorkflowManager().getWorkflow(issue)
def wfd = workflow.getDescriptor()
def issueManager = ComponentAccessor.getIssueManager()
def customFieldManager = ComponentAccessor.getCustomFieldManager()
def mi = (MutableIssue) issue
def userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");
def actionName = wfd.getAction(transientVars["actionId"] as int).getId()
log.warn("Current action name: $actionName")
String IdTransicion= String.valueOf(actionName);
def transicion = customFieldManager.getCustomFieldObject(12750)
mi.setCustomFieldValue(transicion, IdTransicion)
issueManager.updateIssue(admin, mi, EventDispatchOption.DO_NOT_DISPATCH, false)