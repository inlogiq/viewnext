import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.*
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import static java.lang.Math.*
import java.sql.Timestamp


def mIssue= (MutableIssue) issue;
def issueManager = ComponentAccessor.getIssueManager()
def userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");

// guardamos el CF
def fecha1 = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Ini Real Entrega")
def fecha2 = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Fin Real Entrega")
def fecha3 = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Ini Real DF/DP")
def fecha4 = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Fin Real DF/DP")

mIssue.setCustomFieldValue(fecha1 , null);
mIssue.setCustomFieldValue(fecha2 , null);
mIssue.setCustomFieldValue(fecha3 , null);
mIssue.setCustomFieldValue(fecha4 , null);



ComponentAccessor.getIssueManager().updateIssue(admin, mIssue, EventDispatchOption.ISSUE_UPDATED, false)