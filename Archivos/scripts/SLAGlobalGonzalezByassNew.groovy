import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface
import groovy.sql.Sql
import java.sql.Connection
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser
import java.nio.file.Files
import java.nio.file.Paths
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.DecimalFormat;
//isssues rechazadas
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.*;

InformeParteUno()

//función para obtener la primera parte del INFORME (OK,KO) por servicios e Indicadores
public String InformeParteUno(){

    //def filePath = "/var/atlassian/application-data/jira/export/GonzalezByasss_Sla_Global_parte_1_"+FechaDesde()+"_"+FechaHasta()+".csv"
    //new File(filePath).createNewFile()
    //PrintWriter writer = new PrintWriter(filePath, "UTF-8")
    def someInfo = [1, 2, 3]
    def servicio = ["Correctivo", "Evolutivo", "Soporte no planificado","Soporte planificado","Calidad Entrega","Calidad Entrega"]
    def prioridad_correctivo = ["Bloqueante", "Alta", "Media","Baja"]
    def prioridad_evolutivo = ["-", "-", "-","-","-"]
    def prioridad_noPlanificado = ["Bloqueante", "Alta", "Media","Baja"]
    def prioridad_Planificadoo = ["-", "-"]
    def prioridad_rechazos = ["-"]
    def prioridad_enlazadas = ["-"]
    def objetivo = ["80%", "80%", "80%"]
    def peso = ["10", "5", "5","5","5","5","5","10","10","10","5","5","5","5","5","10","5"]
    def indicador=["Tiempo de resolucion","Tiempo de resolucion","Tiempo de resolucion","Tiempo de resolucion","Entrega de Valoracion y Planificacion Menor","Entrega de Valoracion y Planificacion Mayor","Entrega de diseño funcional y D. pruebas","Entrega de Solicitud Mayor","Entrega de Solicitud Menor","Tiempo de Resolucion","Tiempo de Resolucion","Tiempo de resolucion","Tiempo de resolucion","Entrega de Valoracion y Planificacion","Entrega de Solicitud","Incidencias Rechazadas","Incidencias Colaterales"]
    DecimalFormat FormatoD = new DecimalFormat("#.00");
    //writer.println("Servicio;Indicador;Prioridad;Objetivo de complimiento Global;Peso;OK;KO")
    int cont_total=0
    //INFORME SLA FINAL
    int totalAportacion= 17
    String[] aportacionFinal= new String[totalAportacion];
    String[] individualFinal= new String[totalAportacion];
    String[] totalFinal= new String[totalAportacion];
    String[] okFinal= new String[totalAportacion];
    String[] koFinal= new String[totalAportacion];
    for(int i=0;i<servicio.size();i++){
          
        //CORRECTIVO
        if(i==0){
            int ponderacion
            for(int j=0;j<prioridad_correctivo.size();j++){
                
                if(j==0){
                    ponderacion= 92
                }else{
                    ponderacion= 90
                }
                
                def porcentaje_correctivo=Correctivo(prioridad_correctivo[j])
                def parts_correctivo = porcentaje_correctivo.split("-");
                int cumple = Integer.parseInt(parts_correctivo[0]);   
                
                //aportación
                int peso_entero = Integer.parseInt(peso[j]); 
                BigDecimal aporte=((cumple*peso_entero)/ponderacion)
                //FormatoD.format(aporte)

                aportacionFinal[j]=aporte.round(2)
                individualFinal[j]=cumple
                totalFinal[j]=parts_correctivo[4]
                okFinal[j]=parts_correctivo[2]
                koFinal[j]=parts_correctivo[3]

                //writer.println(servicio[i]+";"+indicador[cont_total]+";"+prioridad_correctivo[j]+";"+ponderacion+"%;"+peso[cont_total]+"%;"+parts_correctivo[2]+";"+parts_correctivo[3])
                cont_total++
            }
        }
        //Evolutivo
        if(i==1){
            
            for(int j=0;j<prioridad_evolutivo.size();j++){
                def porcentaje_evo=Evolutivo(prioridadesEvolutivos(j))
                def parts_evo = porcentaje_evo.split("-");
                int cumple = Integer.parseInt(parts_evo[0]);   
                
                //aportación
                int peso_entero = Integer.parseInt(peso[cont_total]); 
                BigDecimal aporte=((cumple*peso_entero)/90)
                //FormatoD.format(aporte)
                //ARRAY PARA EL INFORME FINAL
                aportacionFinal[cont_total]=aporte.round(2)
                individualFinal[cont_total]=cumple
                totalFinal[cont_total]=parts_evo[4]
                okFinal[cont_total]=parts_evo[2]
                koFinal[cont_total]=parts_evo[3]

                //writer.println(servicio[i]+";"+indicador[cont_total]+";"+prioridad_evolutivo[j]+";90%;"+peso[cont_total]+"%;"+parts_evo[2]+";"+parts_evo[3])
                cont_total++
            }
        }
        //Soporte no planificado
        if(i==2){
            int ponderacion
            for(int j=0;j<prioridad_noPlanificado.size();j++){
                if(j==0){
                    ponderacion= 92
                }else{
                    ponderacion= 90
                }
                def porcentaje_noPlan=NoPlanificado(prioridad_noPlanificado[j])
                def parts_noPlan = porcentaje_noPlan.split("-");
                int cumple = Integer.parseInt(parts_noPlan[0]);   
                    
                //aportación
                int peso_entero = Integer.parseInt(peso[cont_total]); 
                BigDecimal aporte=((cumple*peso_entero)/ponderacion)
                //FormatoD.format(aporte)
                //ARRAY PARA EL INFORME FINAL
                aportacionFinal[cont_total]=aporte.round(2)
                individualFinal[cont_total]=cumple
                totalFinal[cont_total]=parts_noPlan[4]
                okFinal[cont_total]=parts_noPlan[2]
                koFinal[cont_total]=parts_noPlan[3]

                //writer.println(servicio[i]+";"+indicador[cont_total]+";"+prioridad_noPlanificado[j]+";"+ponderacion+"%;"+peso[cont_total]+"%;"+parts_noPlan[2]+";"+parts_noPlan[3])
                cont_total++
            }
        }
        //Soporte planificado
        if(i==3){
            
            for(int j=0;j<prioridad_Planificadoo.size();j++){

                def porcentaje_Plan=Planificado(prioridadesPlanificado(j))
                def parts_Plan = porcentaje_Plan.split("-");
                int cumple = Integer.parseInt(parts_Plan[0]);   
                    
                //aportación
                int peso_entero = Integer.parseInt(peso[cont_total]); 
                BigDecimal aporte=((cumple*peso_entero)/90)
                //FormatoD.format(aporte)
                //ARRAY PARA EL INFORME FINAL
                aportacionFinal[cont_total]=aporte.round(2)
                individualFinal[cont_total]=cumple
                totalFinal[cont_total]=parts_Plan[4]
                okFinal[cont_total]=parts_Plan[2]
                koFinal[cont_total]=parts_Plan[3]

                //writer.println(servicio[i]+";"+indicador[cont_total]+";"+prioridad_Planificadoo[j]+";90%;"+peso[cont_total]+"%;"+parts_Plan[2]+";"+parts_Plan[3])
                cont_total++
            }
        }
        //Rechazos
        if(i==4){
            
            for(int j=0;j<prioridad_rechazos.size();j++){
                
                int rechazos= rechazos()            
                int totalIssues= totalIncidencias()
                int totalSinRechazos=(totalIssues-rechazos)
                int calaculo_success
                int calculo_error
                if(rechazos==0){
                    calaculo_success= 100 
                    calculo_error= 0
                }else{              
                    calculo_error= ((100*rechazos)/totalIssues)
                    calaculo_success= (100-calculo_error)
                }                        
                
                //aportación   
                int cumpliGlobalCliente= (100-6)
                int peso_entero = Integer.parseInt(peso[cont_total]); 
                BigDecimal aporte=((calaculo_success*peso_entero)/cumpliGlobalCliente)
                //FormatoD.format(aporte)
                //ARRAY PARA EL INFORME FINAL
                aportacionFinal[cont_total]=aporte.round(2)
                individualFinal[cont_total]=calaculo_success
                totalFinal[cont_total]=totalIssues
                okFinal[cont_total]=totalSinRechazos
                koFinal[cont_total]=rechazos

                //writer.println(servicio[i]+";"+indicador[cont_total]+";"+prioridad_rechazos[j]+";6%;"+peso[cont_total]+"%;"+totalSinRechazos+";"+rechazos)
                cont_total++
            }
        }
        //Enlazadas
        if(i==5){
            
            for(int j=0;j<prioridad_enlazadas.size();j++){
                
                int rechazos= enlazadasKO()           
                int totalIssues= enlazadas()
                int totalSinRechazos=(totalIssues-rechazos)
                int calaculo_success
                int calculo_error
                if(rechazos==0){
                    calaculo_success= 100 
                    calculo_error= 0
                }else{              
                    calculo_error= ((100*rechazos)/totalIssues)
                    calaculo_success= (100-calculo_error)
                }                        
                
                //aportación  
                int cumpliGlobalCliente= (100-12)
                int peso_entero = Integer.parseInt(peso[cont_total]); 
                BigDecimal aporte=((calaculo_success*peso_entero)/cumpliGlobalCliente)
                //FormatoD.format(aporte)
                //ARRAY PARA EL INFORME FINAL
                aportacionFinal[cont_total]=aporte.round(2)
                individualFinal[cont_total]=calaculo_success
                totalFinal[cont_total]=totalIssues
                okFinal[cont_total]=totalSinRechazos
                koFinal[cont_total]=rechazos

                //writer.println(servicio[i]+";"+indicador[cont_total]+";"+prioridad_enlazadas[j]+";12%;"+peso[cont_total]+"%;"+totalSinRechazos+";"+rechazos)
                cont_total++
            }
        }
        
        
    }
    //writer.close()
    //uploadAttachment(FechaDesde(),FechaHasta(),"GonzalezByasss_Sla_Global_parte_1")

    //CREACIÓN DEL INFORME FINAL
    InformeParteFinal(okFinal,koFinal,totalFinal,individualFinal,aportacionFinal)
}

//ESTE INFORME SE GENERA POR INDICADOR Y TIENE LOS CONCEPTOS DE ACEPTACIÓN
public String InformeParteFinal(String [] okFinal, String [] koFinal, String [] totalFinal, String [] individualFinal, String [] aportacionFinal){

    def filePath = "/var/atlassian/application-data/jira/export/GonzalezByasss_Sla_Global_parte_Final_"+FechaDesde()+"_"+FechaHasta()+".csv"
    //def filePath = "/var/atlassian/application-data/jira/export/Saras_Sla_Global_parte_1_"+FechaDesde()+"_"+FechaHasta()+".csv"
    new File(filePath).createNewFile()
    PrintWriter writer = new PrintWriter(filePath, "UTF-8")
    DecimalFormat FormatoD = new DecimalFormat("#.##");

    //PESOS E INDICADORES
    def peso = ["10", "5", "5","5","5","5","5","10","10","10","5","5","5","5","5","10","5"]
    def indicador=["Tiempo de resolucion","Tiempo de resolucion","Tiempo de resolucion","Tiempo de resolucion","Entrega de Valoracion y Planificacion","Entrega de Valoracion y Planificacion Mayor","Entrega de diseño funcional y D. pruebas","Entrega de Solicitud","Entrega de Solicitud Menor","Tiempo de Resolucion","Tiempo de Resolucion","Tiempo de resolucion","Tiempo de resolucion","Entrega de Valoracion y Planificacion","Entrega de Solicitud","Incidencias Rechazadas","Incidencias Colaterales"]
    def servicio = ["Correctivo","Correctivo","Correctivo","Correctivo","Evolutivo","Evolutivo","Evolutivo","Evolutivo","Evolutivo","Soporte no planificado","Soporte no planificado","Soporte no planificado","Soporte no planificado","Soporte planificado","Soporte planificado","Calidad Entrega","Calidad Entrega"]
    def cumplimientoGlobal=["92","90","90","90","90","90","90","90","90","92","90","90","90","90","90","94","88"]
    def prioridadGlobal=["Bloqueante","Alta","Media","Baja","-","-","-","-","-","Bloqueante","Alta","Media","Baja","-","-","-","-"]
    //CABECERA DEL INFORME
    writer.println("Servicio;Indicador;Prioridad;Objetivo de complimiento Global;Peso;Cumplimiento individual;Aportacion;Totales;OK;KO")
    
    for(int i=0;i<indicador.size();i++){
        
        if(i==4 || i==5 || i==7 || i==8){
            //entrega de valoración y planificación se deja en un solo registro
            if(i==4){

                int totalOK= SumarValoresEntregaV(okFinal[i],okFinal[i+1])
                int totalKO= SumarValoresEntregaV(koFinal[i],koFinal[i+1])
                int totalV= SumarValoresEntregaV(totalFinal[i],totalFinal[i+1])
                int calaculo_success                

                if(totalV==0){
                    calaculo_success=100
                    writer.println(servicio[i]+";"+indicador[i]+";"+prioridadGlobal[i]+";"+cumplimientoGlobal[i]+"%;5%;"+calaculo_success+"%;5%;"+totalV+";"+totalOK+";"+totalKO)
                    
                }else{
                    calaculo_success= ((100*totalOK)/totalV)
                    int peso_entero = Integer.parseInt(peso[i]); 
                    BigDecimal aporte=((calaculo_success*peso_entero)/90)
                    //FormatoD.format(aporte)

                    writer.println(servicio[i]+";"+indicador[i]+";"+prioridadGlobal[i]+";"+cumplimientoGlobal[i]+"%;5%;"+calaculo_success+"%;"+aporte.round(2)+"%;"+totalV+";"+totalOK+";"+totalKO)
                    
                }
                
                
            }
             //entrega de solicitud se deja en un solo registro
            if(i==7){

                int totalOK= SumarValoresEntregaV(okFinal[i],okFinal[i+1])
                int totalKO= SumarValoresEntregaV(koFinal[i],koFinal[i+1])
                int totalV= SumarValoresEntregaV(totalFinal[i],totalFinal[i+1])
                int calaculo_success                

                if(totalV==0){
                    calaculo_success=100
                    writer.println(servicio[i]+";"+indicador[i]+";"+prioridadGlobal[i]+";"+cumplimientoGlobal[i]+"%;10%;"+calaculo_success+"%;10%;"+totalV+";"+totalOK+";"+totalKO)
                    
                }else{
                    calaculo_success= ((100*totalOK)/totalV)
                    int peso_entero = Integer.parseInt(peso[i]); 
                    BigDecimal aporte=((calaculo_success*peso_entero)/90)
                    //FormatoD.format(aporte)

                    writer.println(servicio[i]+";"+indicador[i]+";"+prioridadGlobal[i]+";"+cumplimientoGlobal[i]+"%;10%;"+calaculo_success+"%;"+aporte.round(2)+"%;"+totalV+";"+totalOK+";"+totalKO)
                    
                }
                
                
            }
            //los registros 5 y 8 se omiten
        }else{
            //Si no hay muestra, la Aportación coincide con el Peso, y el Cumplimiento Individual es 100%
            int totalV= Integer.parseInt(totalFinal[i])         
            if(totalV==0){
                int calaculo_success=100                
                writer.println(servicio[i]+";"+indicador[i]+";"+prioridadGlobal[i]+";"+cumplimientoGlobal[i]+"%;"+peso[i]+"%;"+calaculo_success+"%;"+peso[i]+"%;"+totalFinal[i]+";"+okFinal[i]+";"+koFinal[i])
            }else{              
                writer.println(servicio[i]+";"+indicador[i]+";"+prioridadGlobal[i]+";"+cumplimientoGlobal[i]+"%;"+peso[i]+"%;"+individualFinal[i]+"%;"+aportacionFinal[i]+"%;"+totalFinal[i]+";"+okFinal[i]+";"+koFinal[i])
            }

            
        }
        
    }
    writer.close()
    //para adjunatr en la incidencia
    uploadAttachment(FechaDesde(),FechaHasta(),"GonzalezByasss_Sla_Global_parte_Final")
}
public int SumarValoresEntregaV(String valA, String valB){

        int a = Integer.parseInt(valA);
        int b = Integer.parseInt(valB);
        return a+b
}

public String Correctivo(String prioridad){
    
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = 'SELECT "AO_C5D949_TTS_ISSUE_SLA".* FROM "jiraissue" join "AO_C5D949_TTS_ISSUE_SLA" on("jiraissue".id="AO_C5D949_TTS_ISSUE_SLA"."ISSUE_ID") where \"SLA_ID\" in(36,37,38,39) and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"resolutiondate\" >=\''+FechaDesde()+'\' AND \"resolutiondate\" <=\''+FechaHasta()+'\' AND "jiraissue".issuestatus in(\'10010\',\'10012\') AND "jiraissue".project=10903'
    //def sqlStmt2 = 'SELECT * FROM "AO_C5D949_TTS_ISSUE_SLA" where \"SLA_ID\" in(36,37,38,39) and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"ORIGIN_DATE\" >=\''+FechaDesde()+'\' AND \"ORIGIN_DATE\" <=\''+FechaHasta()+'\''
    log.warn(sqlStmt2)
    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    String[] solicitud= new String[i];
    String[] sla_id= new String[i];
    String[] description= new String[i];
    int j=0
    sql.eachRow(sqlStmt2){

        solicitud[j]= it.INDICATOR;
        sla_id[j] = it.SLA_ID;
        description[j] = it.ISSUE_KEY;
        j++
    }
    int success=0
    int exceed=0
    int cont_prioridad=0
    
    if(prioridad=="Bloqueante"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="36"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                } 
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    
    if(prioridad=="Alta"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="37"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                } 
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    
    if(prioridad=="Media"){
        
        for(int k=0;k<solicitud.size();k++){
            
           if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="38"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
           }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    
    if(prioridad=="Baja"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="39"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)           
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    
    
    sql.close()
}
public String prioridadesEvolutivos(int keys){

    if(keys==0){
        return "menor1"
    }
    if(keys==1){
        return "mayor1"
    }
    if(keys==2){
        return "DF/DP"
    }
    if(keys==3){
        return "mayor2"
    }

    if(keys==4){
        return "menor2"
    }
}
public String prioridadesPlanificado(int keys){

    if(keys==0){
        return "p1"
    }
    if(keys==1){
        return "p2"
    }
}
public String NoPlanificado(String prioridad){
    
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = 'SELECT "AO_C5D949_TTS_ISSUE_SLA".* FROM "jiraissue" join "AO_C5D949_TTS_ISSUE_SLA" on("jiraissue".id="AO_C5D949_TTS_ISSUE_SLA"."ISSUE_ID") where \"SLA_ID\" in(48,49,50,51) and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"resolutiondate\" >=\''+FechaDesde()+'\' AND \"resolutiondate\" <=\''+FechaHasta()+'\' AND "jiraissue".issuestatus in(\'10010\',\'10012\') AND "jiraissue".project=10903'
    //def sqlStmt2 = 'SELECT * FROM "AO_C5D949_TTS_ISSUE_SLA" where \"SLA_ID\" in(48,49,50,51) and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"ORIGIN_DATE\" >=\''+FechaDesde()+'\' AND \"ORIGIN_DATE\" <=\''+FechaHasta()+'\''    
    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    String[] solicitud= new String[i];
    String[] sla_id= new String[i];
    String[] description= new String[i];
    int j=0
    sql.eachRow(sqlStmt2){

        solicitud[j]= it.INDICATOR;
        sla_id[j] = it.SLA_ID;
        description[j] = it.ISSUE_KEY;
        j++
    }
    int success=0
    int exceed=0
    int cont_prioridad=0
    
    if(prioridad=="Bloqueante"){
        
        for(int k=0;k<solicitud.size();k++){
            
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="48"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                } 
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    
    if(prioridad=="Alta"){
        
        for(int k=0;k<solicitud.size();k++){
            
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="49"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    if(prioridad=="Media"){
        
        for(int k=0;k<solicitud.size();k++){
            
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="50"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    if(prioridad=="Baja"){
        
        for(int k=0;k<solicitud.size();k++){
            
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="51"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    sql.close()
}

public String Evolutivo(String prioridad){
    
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = 'SELECT "AO_C5D949_TTS_ISSUE_SLA".* FROM "jiraissue" join "AO_C5D949_TTS_ISSUE_SLA" on("jiraissue".id="AO_C5D949_TTS_ISSUE_SLA"."ISSUE_ID") where \"SLA_ID\" in(40,41,52,55,54) and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"resolutiondate\" >=\''+FechaDesde()+'\' AND \"resolutiondate\" <=\''+FechaHasta()+'\' AND "jiraissue".issuestatus in(\'10010\',\'10012\') AND "jiraissue".project=10903'
    //def sqlStmt2 = 'SELECT * FROM "AO_C5D949_TTS_ISSUE_SLA" where \"SLA_ID\" in(40,41,52,55,54) and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"ORIGIN_DATE\" >=\''+FechaDesde()+'\' AND \"ORIGIN_DATE\" <=\''+FechaHasta()+'\''    
    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    String[] solicitud= new String[i];
    String[] sla_id= new String[i];
    String[] description= new String[i];
    int j=0
    sql.eachRow(sqlStmt2){

        solicitud[j]= it.INDICATOR;
        sla_id[j] = it.SLA_ID;
        description[j] = it.ISSUE_KEY;
        j++
    }
    int success=0
    int exceed=0
    int cont_prioridad=0
    
    if(prioridad=="menor1"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="40"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    
    if(prioridad=="menor2"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="55"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    if(prioridad=="mayor1"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="41"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                } 
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }    
    if(prioridad=="mayor2"){
        
        for(int k=0;k<solicitud.size();k++){
            
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="54"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    if(prioridad=="DF/DP"){
        
        for(int k=0;k<solicitud.size();k++){
            
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="52"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    sql.close()
}

public String Planificado(String prioridad){
    
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = 'SELECT "AO_C5D949_TTS_ISSUE_SLA".* FROM "jiraissue" join "AO_C5D949_TTS_ISSUE_SLA" on("jiraissue".id="AO_C5D949_TTS_ISSUE_SLA"."ISSUE_ID") where \"SLA_ID\" in(45,57) and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"resolutiondate\" >=\''+FechaDesde()+'\' AND \"resolutiondate\" <=\''+FechaHasta()+'\' AND "jiraissue".issuestatus in(\'10010\',\'10012\') AND "jiraissue".project=10903'
    //def sqlStmt2 = 'SELECT * FROM "AO_C5D949_TTS_ISSUE_SLA" where \"SLA_ID\" in(45,57) and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"ORIGIN_DATE\" >=\''+FechaDesde()+'\' AND \"ORIGIN_DATE\" <=\''+FechaHasta()+'\''    
    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    String[] solicitud= new String[i];
    String[] sla_id= new String[i];
    String[] description= new String[i];
    int j=0
    sql.eachRow(sqlStmt2){

        solicitud[j]= it.INDICATOR;
        sla_id[j] = it.SLA_ID;
        description[j] = it.ISSUE_KEY;
        j++
    }
    int success=0
    int exceed=0
    int cont_prioridad=0
    
    if(prioridad=="p1"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="45"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }
    
    if(prioridad=="p2"){
        
        for(int k=0;k<solicitud.size();k++){
           
            if(ExluirSla(description[k])!='Si'){
                if(sla_id[k]=="57"){

                    if(solicitud[k]=="SUCCESS"){
                        success++
                    }else{
                       exceed++ 
                    }
                    cont_prioridad++
                }   
            }
        }
        if(cont_prioridad!=0){
            int calaculo_success= ((100*success)/cont_prioridad)
            int calaculo_exceed= ((100*exceed)/cont_prioridad)
            return calaculo_success+"-"+calaculo_exceed+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }        
        
    }    
    sql.close()
}

static void uploadAttachment(String FechaDesde,String FechaHasta, String Nombre){    

    ApplicationUser user = ComponentAccessor.getUserManager().getUserByName("admin")
    def issue = ComponentAccessor.getIssueManager().getIssueObject("PRUEBIQ-9239")
    File archivo = new File("/var/atlassian/application-data/jira/export/"+Nombre+"_"+FechaDesde+"_"+FechaHasta+".csv")
    def attachmentManager = ComponentAccessor.getAttachmentManager()
    def bean = new CreateAttachmentParamsBean.Builder().copySourceFile(true)
    .file(archivo)
    .filename(archivo.name)
    .author(user)
    .issue(issue)
    .build()
    attachmentManager.createAttachment(bean)
}
public String FechaDesde(){
    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
    //DateFormat dateFormat = new SimpleDateFormat("yyyy");
    String fechaDesde= dateFormat.format(date)+"-01"
    //String fechaDesde= dateFormat.format(date)+"-09-01"
    return fechaDesde
}
public String FechaHasta(){
    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
    //DateFormat dateFormat = new SimpleDateFormat("yyyy");
    String fechaHasta= dateFormat.format(date)+"-"+day()
    //String fechaHasta= dateFormat.format(date)+"-09-30"
    return fechaHasta
}
public int rechazos(){

    final String jqlSearch = "project = 'Gonzalez Byass - AMS SAP'  AND Rechazos > 0 and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND status in (Confirmada, Cancelada)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}
public int totalIncidencias(){
    
    final String jqlSearch = "project = 'Gonzalez Byass - AMS SAP' and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND status in (Confirmada, Cancelada)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}

public int enlazadas(){

    final String jqlSearch = "project = 'Gonzalez Byass - AMS SAP' and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND issuetype in (Correctivo,'Evolutivo Menor','Evolutivo Mayor') AND status in (Confirmada, Cancelada)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}

public int enlazadasKO(){

    final String jqlSearch = "project = 'Gonzalez Byass - AMS SAP' and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND issuetype in (Correctivo,'Evolutivo Menor','Evolutivo Mayor') AND status in (Confirmada, Cancelada) AND issueLinkType in (causes)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}

public String ExluirSla(String key){
    
    def ComponentIssue= ComponentAccessor.getIssueManager()
    def customFieldManager = ComponentAccessor.getCustomFieldManager()
    def excluir = customFieldManager.getCustomFieldObject(12735)
    
    Issue issue= ComponentIssue.getIssueObject(key)
    
    return issue.getCustomFieldValue(excluir).toString()
    
    
}

public String day(){

    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("MM");
    def dia =dateFormat.format(date)
    
    if(dia=='01'){
        return '31'
    }
    if(dia=='02'){
        return '28'
    }
    if(dia=='03'){
        return '31'
    }
    if(dia=='04'){
        return '30'
    }
    if(dia=='05'){
        return '31'
    }
    if(dia=='06'){
        return '30'
    }
    if(dia=='07'){
        return '31'
    }
    if(dia=='08'){
        return '31'
    }
    if(dia=='09'){
        return '30'
    }
    if(dia=='10'){
        return '31'
    }
    if(dia=='11'){
        return '30'
    }
    if(dia=='12'){
        return '31'
    }
    return '30'
}