import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade
@WithPlugin("com.atlassian.servicedesk")

//def issue = (MutableIssue) ComponentAccessor.getIssueManager().getIssueObject('RIES-93')
def issue = (MutableIssue) issue
def userManager = ComponentAccessor.getUserManager()
def currentUser = userManager.getUserByName("admin");
def issueManager = ComponentAccessor.getIssueManager()
def portal = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("portal").getValue(issue);
def tipoIssue = issue.getIssueType().getId()
//variables para insight
def dirSisHW
def dirSisSW
def resAreaHW
def resAreaSW
def directores = []
def responsables = []
def currentUse = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
//campos de cada objeto
def dirSisCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Director de sistemas")
def resAreaCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Responsable de área")
    
//valores de los campos que manda exalate
def dispositivoHW = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Dispositivos Hardware").getValue(issue);
def dispositivoSW = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Dispositivos Software").getValue(issue);


log.warn("El tipoIssue es: " + tipoIssue)
log.warn("El dispositivoHW es: " + dispositivoHW)
log.warn("La dispositivoSW es: " + dispositivoSW)


if(tipoIssue.toString() == "11601"){ //Evento riesgo
    if(dispositivoHW != null){
        dirSisHW = objectFacade.loadObjectAttributeBean(dispositivoHW[0].getId(), 18115);//Id del atributo director de sistemas

        if(dirSisHW !=null){
            //saco el valor del atributo director de sistemas, que es un objeto de otro esquema
            dirSisHW = dirSisHW.getObjectAttributeValueBeans()[0];
            dirSisHW = dirSisHW.getValue()
            //busco el id del director de sistemas en su esquema
            dirSisHW = iqlFacade.findObjectsByIQLAndSchema(14, "key = \"" + "US-"+ dirSisHW + "\"")
            dirSisHW = objectFacade.loadObjectAttributeBean(dirSisHW[0].getId(), 18137)
            dirSisHW = dirSisHW.getObjectAttributeValueBeans()[0];       
            dirSisHW = dirSisHW.getValue()
            dirSisHW = ComponentAccessor.getUserUtil().getUserByKey(dirSisHW.toString())
            log.warn("el director de sistemas HW es: " + dirSisHW)
            directores.add(dirSisHW)
        }
    }
    if(dispositivoSW != null){
        dirSisSW = objectFacade.loadObjectAttributeBean(dispositivoSW[0].getId(), 18133);//Id del atributo director de sistemas
		
        if(dirSisSW !=null){
            //saco el valor del atributo director de sistemas, que es un objeto de otro esquema
            dirSisSW = dirSisSW.getObjectAttributeValueBeans()[0];       
            dirSisSW = dirSisSW.getValue()
            //busco el id del director de sistemas en su esquema
            dirSisSW = iqlFacade.findObjectsByIQLAndSchema(14, "key = \"" + "US-"+ dirSisSW + "\"")
            dirSisSW = objectFacade.loadObjectAttributeBean(dirSisSW[0].getId(), 18137)
            dirSisSW = dirSisSW.getObjectAttributeValueBeans()[0];       
            dirSisSW = dirSisSW.getValue()
            
            dirSisSW = ComponentAccessor.getUserUtil().getUserByKey(dirSisSW.toString())
            log.warn("el director de sistemas SW es: " + dirSisSW)
            directores.add(dirSisSW)
        }
    }
}else if(tipoIssue == "11600"){ //plan de accion
    if(dispositivoHW != null){
        resAreaHW = objectFacade.loadObjectAttributeBean(dispositivoHW[0].getId(), 18116);//Id del atributo responsable de area

        if(resAreaHW !=null){
            //saco el valor del atributo responsable de area, que es un objeto de otro esquema
            resAreaHW = resAreaHW.getObjectAttributeValueBeans()[0];
            resAreaHW = resAreaHW.getValue()
            //busco el id del responsable de area en su esquema
            resAreaHW = iqlFacade.findObjectsByIQLAndSchema(14, "key = \"" + "US-"+ resAreaHW + "\"")
            resAreaHW = objectFacade.loadObjectAttributeBean(resAreaHW[0].getId(), 18137)
            resAreaHW = resAreaHW.getObjectAttributeValueBeans()[0];       
            resAreaHW = resAreaHW.getValue()
            
            resAreaHW = ComponentAccessor.getUserUtil().getUserByKey(resAreaHW.toString())
            log.warn("el responsable de area HW es: " + resAreaHW)
            responsables.add(resAreaHW)
        }
    }
    if(dispositivoSW != null){
        resAreaSW = objectFacade.loadObjectAttributeBean(dispositivoSW[0].getId(), 18134);//Id del atributo responsable de area SW

        if(resAreaSW !=null){
            //saco el valor del atributo responsable de area, que es un objeto de otro esquema
            resAreaSW = resAreaSW.getObjectAttributeValueBeans()[0];
            resAreaSW = resAreaSW.getValue()
            //busco el id del responsable de area en su esquema
            resAreaSW = iqlFacade.findObjectsByIQLAndSchema(14, "key = \"" + "US-"+ resAreaSW + "\"")
            resAreaSW = objectFacade.loadObjectAttributeBean(resAreaSW[0].getId(), 18137)
            resAreaSW = resAreaSW.getObjectAttributeValueBeans()[0];       
            resAreaSW = resAreaSW.getValue()
            
            resAreaSW = ComponentAccessor.getUserUtil().getUserByKey(resAreaSW.toString())
            log.warn("el responsable de area SW es: " + resAreaSW)
            responsables.add(resAreaSW)
        }
    }
}else{
    log.warn("algo va mal")
}

if(tipoIssue.toString() == "11601"){
    if((currentUse == dirSisSW) || (currentUse == dirSisHW)){
         passesCondition = true
    }else{
         passesCondition = false
    }
}else if(tipoIssue == "11600"){
    if((currentUse == resAreaHW) || (currentUse == resAreaSW)){
         passesCondition = true
    }else{
         passesCondition = false
    }
}