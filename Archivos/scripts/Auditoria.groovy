import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface
import groovy.sql.Sql
import java.sql.Connection
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser
import java.nio.file.Files
import java.nio.file.Paths
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.DecimalFormat;
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.user.util.*;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
//Insight 
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
//isssues rechazadas
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.*;
//------
import com.atlassian.jira.issue.index.IssueIndexingService
//import com.atlassian.jira.issue.Issue
import com.atlassian.jira.util.ImportUtils
import org.apache.log4j.Category

def minHoras = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Mínimo de horas").getValue(issue);
def mes = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Mes a auditar").getValue(issue);
def año = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Año").getValue(issue);
log.warn("min "+minHoras.class)
//Eliminar incidencias Anteriores
AuditoriaSi()
//Marcar incidencias nuevas pra auditoria
IncidenciasAleatorias(mes.toString(),minutos(minHoras),issue,año.toString())
public String IncidenciasAleatorias(String mes, int minutos,Issue incidencia, String year){
    
    //int random=TotalIncidenciasAleatorias(mes,minutos)
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = "SELECT sum(worklog.timeworked),jiraissue.* FROM jiraissue join worklog on(jiraissue.id=worklog.issueid) where jiraissue.project in("+proyectos()+") AND worklog.startdate>=\'"+FechaDesde(year,mes)+"\' AND worklog.startdate<=\'"+FechaHasta(year,mes)+"\' group by jiraissue.id HAVING SUM(worklog.timeworked) >=("+minutos+"*60)  order by RANDOM()"
    log.warn(sqlStmt2)
    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    
    String[] solicitud= new String[i];
    BigDecimal[] TotalIncurridas= new BigDecimal[i]
    int j=0   
    sql.eachRow(sqlStmt2){ 
        
        solicitud[j]= it.id.toString();
        TotalIncurridas[j]= it.sum
        j++
        
   	}
    
    sql.close()
    log.warn("Total incidencias sin estado "+solicitud.size())
    //filtrar los estados
    int p=0
	String[] solicitudEstados= new String[i];
    BigDecimal[] TotalIncurridasEstados= new BigDecimal[i]
    for(int k=0;k<solicitud.size();k++){
        
        //obtengo la clave de la tarea padre
        
        String ClavePadre= ClaveTarea(solicitud[k],incidencia)
        //se filtran los estatus
        if(status(ClavePadre,incidencia)){
            solicitudEstados[p]= solicitud[k];
            TotalIncurridasEstados[p]= TotalIncurridas[k]
            p++
        }  
    }
    log.warn("Issues con filtro de estado aplicado "+solicitudEstados)
    //obtengo el numero de issue con todos los filtros aplicados
    int Testados=0
    
    for(int n=0;n<solicitudEstados.size();n++){
        if(solicitudEstados[n]!=null){
            Testados++
        }
    }
    
    log.warn("Total issue con filtro de estado aplicado "+Testados)
    //saco el porcentaje 
    int porcentaje=totalAleatotio(Testados)
    log.warn("Total incidencias con el porcentaje aplicado"+porcentaje)
    
    BigDecimal horasIncurridasMuestra=0
    //Actualizo el campo QA
    for(int e=0;e<porcentaje;e++){
        log.warn("ID "+solicitudEstados[e])
        
        String SubTareas= KeySubTask(IdIncidenciaPadre(solicitudEstados[e],incidencia))
        //sumo el total de todas la subtareas asociada a esta tarea
        BigDecimal HorasIncurridasSumada= horasGlobalesIncurridasSubtask(mes,year,SubTareas)
        
        horasIncurridasMuestra+=HorasIncurridasSumada
        ActualizarCampoQA(solicitudEstados[e],incidencia)
    }     
    //actualizo los totales de la incidencia de auditoria
   
    def HorasIncurridasT= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13024');
    def HorasIncurridasConFiltro= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13025');
    def HorasIncurridasMuestra= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13026');
    
    //actualizo en la incidencia las horas incurridas
    UserManager userManager = ComponentAccessor.getUserManager();
    def admin = userManager.getUserByName("admin");
    //horas globales
    String IncurridasTotales=horasGlobalesIncurridas(mes,year)+"H"
    log.warn("El total de horas globales son: "+IncurridasTotales)
    incidencia.setCustomFieldValue(HorasIncurridasT,IncurridasTotales);
    //horas con filtros aplicados
    //horasIncurridasMuestra=(horasIncurridasMuestra/60)/60
    String incurridasWitFilter= horasIncurridasMuestra.round(2)+"H"
    log.warn("El total de horas con los filtros aplicados es "+incurridasWitFilter)
    incidencia.setCustomFieldValue(HorasIncurridasConFiltro,incurridasWitFilter);
    //Porcentaje
    BigDecimal PorcentajeFiltro = horasGlobalesIncurridas(mes,year);
    BigDecimal PorcentajeCalculo= (horasIncurridasMuestra*100)/PorcentajeFiltro
    String PorcentajeTotalCalculado= PorcentajeCalculo.round(2)+"%"
    incidencia.setCustomFieldValue(HorasIncurridasMuestra,PorcentajeTotalCalculado);    
    ComponentAccessor.getIssueManager().updateIssue(admin, incidencia, EventDispatchOption.DO_NOT_DISPATCH, false);
}
//Clave de la tarea
public String ClaveTarea(String id, Issue Incidencia){
        
    def issueManager = ComponentAccessor.getIssueManager()
    long clave = Long.parseLong(id);     
    def issue = ComponentAccessor.getIssueManager().getIssueObject(clave)      
    def IssuePadre=issue.getParentObject()?.getKey()
    if(IssuePadre==null){
        return issue.key
    }else{
        return IssuePadre
    }
    
}

public String ActualizarCampoQA(String id, Issue Incidencia){
    
    UserManager userManager = ComponentAccessor.getUserManager();
    def admin = userManager.getUserByName("admin");
    def issueManager = ComponentAccessor.getIssueManager()
    long clave = Long.parseLong(id);
    def QA= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_12810');    
    def issue = ComponentAccessor.getIssueManager().getIssueObject(clave)      
    def IssuePadre=issue.getParentObject()?.getKey()
    def IncidenciaPadre
    if(IssuePadre==null){
        IncidenciaPadre=issue       
    }else{
        IncidenciaPadre=ComponentAccessor.getIssueManager().getIssueObject(IssuePadre)
    }
        
    def optionsManager = ComponentAccessor.getOptionsManager()
    def fieldConfig = QA.getRelevantConfig(IncidenciaPadre)
    def options = optionsManager.getOptions(fieldConfig)
    def option = options.find {it.value == "SI"}
    IncidenciaPadre.setCustomFieldValue(QA,option);    
    ComponentAccessor.getIssueManager().updateIssue(admin, IncidenciaPadre, EventDispatchOption.DO_NOT_DISPATCH, false);
    
    //Reindexo la issue
    def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
    boolean wasIndexing = ImportUtils.isIndexIssues();
    ImportUtils.setIndexIssues(true);        
    issueIndexingService.reIndex(issueManager.getIssueObject(IncidenciaPadre.id));
    ImportUtils.setIndexIssues(wasIndexing);
    /*if(status(IssuePadre,Incidencia)){
        
        
    }else{
        
        def optionsManager = ComponentAccessor.getOptionsManager()
        def fieldConfig = QA.getRelevantConfig(IncidenciaPadre)
        def options = optionsManager.getOptions(fieldConfig)
        def option = options.find {it.value == "NO"}
        IncidenciaPadre.setCustomFieldValue(QA,option);    
    	ComponentAccessor.getIssueManager().updateIssue(admin, IncidenciaPadre, EventDispatchOption.DO_NOT_DISPATCH, false);
        def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)

        boolean wasIndexing = ImportUtils.isIndexIssues();
        ImportUtils.setIndexIssues(true);        
        issueIndexingService.reIndex(issueManager.getIssueObject(IncidenciaPadre.id));
        ImportUtils.setIndexIssues(wasIndexing);
    }*/
    //issueManager.updateIssue(admin, IncidenciaPadre, EventDispatchOption.DO_NOT_DISPATCH, false)
    
}

public String FechaDesde(String year,String mes){
    
    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy");
    String fechaDesde= year+"-"+Mes(mes)+"-01"
    return fechaDesde
}
public String FechaHasta(String year,String mes){
    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy");
    String fechaHasta= year+"-"+Mes(mes)+"-"+day(mes)
    return fechaHasta
}

public String Mes(String mes){
   
    if(mes=='Enero'){
        return '01'
    }
    if(mes=='Febrero'){
        return '02'
    }
    if(mes=='Marzo'){
        return '03'
    }
    if(mes=='Abril'){
        return '04'
    }
    if(mes=='Mayo'){
        return '05'
    }
    if(mes=='Junio'){
        return '06'
    }
    if(mes=='Julio'){
        return '07'
    }
    if(mes=='Agosto'){
        return '08'
    }
    if(mes=='Septiembre'){
        return '09'
    }
    if(mes=='Octubre'){
        return '10'
    }
    if(mes=='Noviembre'){
        return '11'
    }
    if(mes=='Diciembre'){
        return '12'
    }
    return '01'
}

public String day(String mes){

        
    if(mes=='01'){
        return '31'
    }
    if(mes=='02'){
        return '28'
    }
    if(mes=='03'){
        return '31'
    }
    if(mes=='04'){
        return '30'
    }
    if(mes=='05'){
        return '31'
    }
    if(mes=='06'){
        return '30'
    }
    if(mes=='07'){
        return '31'
    }
    if(mes=='08'){
        return '31'
    }
    if(mes=='09'){
        return '30'
    }
    if(mes=='10'){
        return '31'
    }
    if(mes=='11'){
        return '30'
    }
    if(mes=='12'){
        return '31'
    }
    return '30'
}

public int minutos(double horas){
    return horas*60
}

public boolean status(String key, Issue incidencia){
    
    
    //long clave = Long.parseLong(id);        
    def issue = ComponentAccessor.getIssueManager().getIssueObject(key)
    //issue a actualizar QA
    def estado_tarea=issue.getStatusObject().getId()    
    //Estados seleccionados en la issue de calidad
    String estadoSeleccionado=estadosTarea(incidencia)
    //convierto los estados en array
    String[] parts = estadoSeleccionado.split(",");
        
    int cont=0
    for(int i=0;i<parts.size();i++){
        //log.warn("Estados seleccionados"+parts[i])
        //log.warn("Estados tarea"+estado_tarea)
        if(parts[i]==estado_tarea){
            cont++
        }
    }
    
    if(cont==0){
        return true
    }else{
        return false
    }
}

public String estadosTarea(Issue issue){
    
    @WithPlugin("com.riadalabs.jira.plugins.insight")
    @PluginModule ObjectFacade objectFacade
    @PluginModule IQLFacade iqlFacade
    //def issue = ComponentAccessor.getIssueManager().getIssueObject('CAL-6')
    
    def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
    def currentProject = issue.projectObject.name
    def issueManager = ComponentAccessor.getIssueManager()
    def estado = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Estado").getValue(issue);
    def estadotxt
    def estadoT
    def id
    def mes = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Mes a auditar").getValue(issue);
    def minHoras = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Mínimo de horas").getValue(issue);
    if(estado!=null){
        for(int i=0; i<estado.size; i++){
            
            if(i==0){
                    
                estadotxt =  estado[i].getName().toString()
                id = objectFacade.loadObjectAttributeBean(estado[i].getId(), 17099);
                id = id.getObjectAttributeValueBeans()[0];
                id = id.getValue();
                estadotxt =  id.toString()
            }else{
                estadotxt += ","+ ((objectFacade.loadObjectAttributeBean(estado[i].getId(), 17099)).getObjectAttributeValueBeans()[0].getValue()).toString()
            }        
        }
        
        return estadotxt;
        
    }else{
        return "No hay estados"
    }
}

public boolean AuditoriaSi(){

	final String jqlSearch = "QA-Au = SI"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    def QA= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_12810'); 
    UserManager userManager = ComponentAccessor.getUserManager();
    def admin = userManager.getUserByName("admin");
    def issueManager = ComponentAccessor.getIssueManager()
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                
                Issue issue= ComponentIssue.getIssueObject(it.key) 
                def optionsManager = ComponentAccessor.getOptionsManager()
                def fieldConfig = QA.getRelevantConfig(issue)
                def options = optionsManager.getOptions(fieldConfig)
                def option = options.find {it.value == "NO"}
                issue.setCustomFieldValue(QA,option);    
    	        ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
       			
                def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
                boolean wasIndexing = ImportUtils.isIndexIssues();
                ImportUtils.setIndexIssues(true);        
                issueIndexingService.reIndex(issueManager.getIssueObject(issue.id));
                ImportUtils.setIndexIssues(wasIndexing);
                
            }
            return true
        } catch (SearchException e) {
            return false
        }
    } else {
        
        return false
    }

}

public int totalAleatotio(int total){
    
    def porcentajes = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Porcentaje").getValue(issue);    
    int porcentaje= (int) porcentajes
    int valor= ((total*porcentaje)/100)
    return valor
    
}

public String project(){
    
    return "11104,11105,11106,11107,11109,11112,11305,11306,11307,11300,11302,10903,11311"
}

public String proyectos(){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = Proyecto AND QA=TRUE");
    def project
    
    if(resultados != null){
         //String[] proyectos= new String[resultados.size()];
         String Valor
        for(int i=0;i<resultados.size();i++){
            project = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 58);
            if(project!=null)
            {
                if(i==0){
                    project = project.getObjectAttributeValueBeans()[0];           
                    //proyectos[i]=project.getValue()
                    Valor=project.getValue()
                }else{
                    project = project.getObjectAttributeValueBeans()[0];           
                    //proyectos[i]=project.getValue()
                    Valor+=","+project.getValue()
                }


            } 
        }
        //log.warn("Proyectos "+Valor)
		return Valor
    }
}
public BigDecimal horasGlobalesIncurridas(String mes, String year){
    
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = "SELECT sum(worklog.timeworked) FROM jiraissue join worklog on(jiraissue.id=worklog.issueid) where jiraissue.project in("+proyectos()+") AND worklog.startdate>=\'"+FechaDesde(year,mes)+"\' AND worklog.startdate<=\'"+FechaHasta(year,mes)+"\' group by jiraissue.id order by RANDOM()"
    log.warn("consulta 2 "+sqlStmt2)
    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    BigDecimal totalMes=0 
    sql.eachRow(sqlStmt2){       
        totalMes+=it.sum
    }
    sql.close()
    totalMes=(totalMes/60)/60
    return totalMes.round(2)
    
}

public Long IdIncidenciaPadre(String id, Issue Incidencia){
    
    UserManager userManager = ComponentAccessor.getUserManager();
    def admin = userManager.getUserByName("admin");
    def issueManager = ComponentAccessor.getIssueManager()
    long clave = Long.parseLong(id);  
    def issue = ComponentAccessor.getIssueManager().getIssueObject(clave)      
    def IssuePadre=issue.getParentObject()?.getId()
    return IssuePadre
}

public String KeySubTask(Long id){
    
    UserManager userManager = ComponentAccessor.getUserManager();
    def admin = userManager.getUserByName("admin");
    def issueManager = ComponentAccessor.getIssueManager()
   //long clave = Long.parseLong(id);  
    def issue = ComponentAccessor.getIssueManager().getIssueObject(id) 
    
    if(issue==null){
        return id.toString()
    }else if(issue.getSubTaskObjects()==null){ 
    	return id.toString()
    }else{    
        def subTask= issue.getSubTaskObjects()    
        String Valor
        for(int i=0;i<subTask.size();i++){

            if(i==0){

                Valor=subTask[i].id
            }else{

                Valor+=","+subTask[i].id
            }
        }
        return Valor
    }
    
}

public BigDecimal horasGlobalesIncurridasSubtask(String mes, String year, String tareas){
    
    def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
    String helperName = delegator.getGroupHelperName("default")
    def objectAttrId;
    Connection conn = ConnectionFactory.getConnection(helperName)
    Sql sql = new Sql(conn)
    def sqlStmt2 = "SELECT sum(worklog.timeworked),jiraissue.* FROM jiraissue join worklog on(jiraissue.id=worklog.issueid) where jiraissue.project in("+proyectos()+") AND worklog.startdate>=\'"+FechaDesde(year,mes)+"\' AND worklog.startdate<=\'"+FechaHasta(year,mes)+"\' AND jiraissue.id in("+tareas+") group by jiraissue.id"
    
    StringBuffer sb2 = new StringBuffer()
    int i=0
    sql.eachRow(sqlStmt2){
        i++
    }
    BigDecimal totalMes=0 
    sql.eachRow(sqlStmt2){       
        totalMes+=it.sum
    }
    sql.close()
    totalMes=(totalMes/60)/60
    log.warn("Total subtask "+totalMes.round(2))
    return totalMes.round(2)
    
}