import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.*
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import static java.lang.Math.*
import java.sql.Timestamp


def mIssue= (MutableIssue) issue;
def issueManager = ComponentAccessor.getIssueManager()
def userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");

// guardamos el CF
def periodo = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Período")
def facAN = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("H. Facturadas AN")
def facCS = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("H. Facturadas CS")
def facPG = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("H. Facturadas PG")

mIssue.setCustomFieldValue(periodo , null);
mIssue.setCustomFieldValue(facAN , null);
mIssue.setCustomFieldValue(facCS , null);
mIssue.setCustomFieldValue(facPG , null);



ComponentAccessor.getIssueManager().updateIssue(admin, mIssue, EventDispatchOption.ISSUE_UPDATED, false)