import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.*
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import static java.lang.Math.*
import java.sql.Timestamp


def mIssue= (MutableIssue) issue;
def issueManager = ComponentAccessor.getIssueManager()
def userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");
// guardamos el CF
def fecha1 = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Ini Real Val-Plan")
def fecha2 = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Fin Real Val-Plan")
//guardamos la fecha actual
def today = new java.sql.Timestamp(new Date().getTime())
//actualizamos los campos
mIssue.setCustomFieldValue(fecha2 ,null);
mIssue.setCustomFieldValue(fecha1 , today);

//actualizamos la issue
ComponentAccessor.getIssueManager().updateIssue(admin, mIssue, EventDispatchOption.ISSUE_UPDATED, false)



