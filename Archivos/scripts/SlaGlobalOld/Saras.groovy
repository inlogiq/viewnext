import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface
import groovy.sql.Sql
import java.sql.Connection
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser
import java.nio.file.Files
import java.nio.file.Paths
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.DecimalFormat;
import com.atlassian.jira.issue.*;
//Insight 
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
//isssues rechazadas
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
//confluence
import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import com.atlassian.sal.api.net.RequestFilePart

//Informe
GenerarInforme()
//envio a confluence
def ApplicationLink getPrimaryConfluenceLink() {
    def applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService.class)
    final ApplicationLink conflLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class)
    conflLink
}
def confluenceLink = getPrimaryConfluenceLink()
assert confluenceLink
def authenticatedRequestFactory = confluenceLink.createImpersonatingAuthenticatedRequestFactory()
File fileUpload = new File(ruta())
assert fileUpload
def part = new RequestFilePart(fileUpload, "file")
assert part
def partList = new ArrayList<RequestFilePart>()
partList.add(part)
assert partList
assert partList.size() == 1
authenticatedRequestFactory
.createRequest(Request.MethodType.POST, "rest/api/content/"+RutaConfluence()+"/child/attachment")
.addHeader("X-Atlassian-Token", "nocheck")
.setFiles(partList)
.execute(new ResponseHandler<Response>() {
    @Override
    void handle(Response response) throws ResponseException {
        if (response.statusCode != HttpURLConnection.HTTP_OK) {
            throw new Exception(response.getResponseBodyAsString())
        } else {
            def webUrl = new JsonSlurper().parseText(response.responseBodyAsString)["_links"]["webui"]
        }
    }
})
public String GenerarInforme(){
    
    //Variables del informe
    def indicadores= Indicadores()
    def servicios = Servicio()
    def prioridad= Prioridad()
    def CGlobal= CumplimientoGlobal()
    def peso= Peso()
    def slaId= SlaId()    
    def filePath = ruta()
    new File(filePath).createNewFile()
    PrintWriter writer = new PrintWriter(filePath, "UTF-8")    
    writer.println(Cabecera())
    for(int i=0; i<indicadores.size();i++){
        
        def totalSla= TotalSlaSql(slaId[i],indicadores[i])
        def separarResultados = totalSla.split("-");
        int TotalS=  Integer.parseInt(separarResultados[3]);
        if(TotalS==0){
            //el porcentaje de cumplimiento es 100% y la aportación el mismo valor del peso
            writer.println(servicios[i]+";"+indicadores[i]+";"+prioridad[i]+";"+CGlobal[i]+"%;"+peso[i]+"%;100%;"+peso[i]+"%;"+TotalS+";"+separarResultados[1]+";"+separarResultados[2])
        }else{
            int cumple = Integer.parseInt(separarResultados[0]);        
            //aportación
            int peso_entero = Integer.parseInt(peso[i]); 
            int ponderacion=  Integer.parseInt(CGlobal[i]);

            BigDecimal aporte=((cumple*peso_entero)/ponderacion)        
            writer.println(servicios[i]+";"+indicadores[i]+";"+prioridad[i]+";"+CGlobal[i]+"%;"+peso[i]+"%;"+cumple+"%;"+aporte.round(2)+"%;"+TotalS+";"+separarResultados[1]+";"+separarResultados[2])
        }
        
    }
    writer.close()
    //para adjunatr en la incidencia
    uploadAttachment(FechaDesde(),FechaHasta(),NombreFichero())
    
}
//ruta servidor
public String ruta(){
    return "/var/atlassian/application-data/jira/export/"+NombreFichero()+"_"+FechaDesde()+"_"+FechaHasta()+".csv"
}
//cabecera archivo
public String Cabecera(){
    return "Servicio;Indicador;Prioridad;Objetivo de complimiento Global;Peso;Cumplimiento individual;Aportacion;Totales;OK;KO"
}
//indicadores
public String[] Indicadores(){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = Saras");    
    def ind
    //variables
    String[] indicadores= new String[resultados.size()];
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){
            ind = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17439);
            ind = ind.getObjectAttributeValueBeans()[0];           
            indicadores[i]=ind.getValue()


        } 
        return indicadores
    }
}
//servicios
public String[] Servicio(){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = Saras");
    def Serv
    String[] servicio= new String[resultados.size()];
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){
            Serv = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17438);
            Serv = Serv.getObjectAttributeValueBeans()[0];           
            servicio[i]=Serv.getValue()


        } 
        return servicio
    }
}
//prioridades
public String[] Prioridad(){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = Saras");
    def pri
    String[] prioridad= new String[resultados.size()];
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){
            pri = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17440);
            pri = pri.getObjectAttributeValueBeans()[0];           
            prioridad[i]=pri.getValue()


        }
        return prioridad
    }
}

//Cumplimiento
public String[] CumplimientoGlobal(){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = Saras");
    def cump
    String[] Cumplimiento= new String[resultados.size()];
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){
            cump = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17441);
            cump = cump.getObjectAttributeValueBeans()[0];           
            Cumplimiento[i]=cump.getValue()

        }
        return Cumplimiento
    }
}
//Cumplimiento
public String[] Peso(){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = Saras");
    def pes
    String[] Peso= new String[resultados.size()];
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){
            pes = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17442);
            pes = pes.getObjectAttributeValueBeans()[0];           
            Peso[i]=pes.getValue()

        }
        return Peso
    }
}

//ID SLA
public String[] SlaId(){
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, "objectType = Saras");
    def sla
    String[] Sla_id= new String[resultados.size()];
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){
            sla = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17444);
            sla = sla.getObjectAttributeValueBeans()[0];           
            Sla_id[i]=sla.getValue()

        }
        return Sla_id
    }
}

public String TotalSlaSql(String ID, String indicador){
    
    if(ID!="" && ID!=null && ID!="null" ){
        
        def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface)
        String helperName = delegator.getGroupHelperName("default")
        def objectAttrId;
        Connection conn = ConnectionFactory.getConnection(helperName)
        Sql sql = new Sql(conn)
        def sqlStmt2 = 'SELECT "AO_C5D949_TTS_ISSUE_SLA".* FROM "jiraissue" join "AO_C5D949_TTS_ISSUE_SLA" on("jiraissue".id="AO_C5D949_TTS_ISSUE_SLA"."ISSUE_ID") where \"SLA_ID\" in('+ID+') and \"FINISHED\"=\'true\' and \"INDICATOR\"!=\'STILL\' AND \"INDICATOR\"!=\'INVALID\' AND \"resolutiondate\" >=\''+FechaDesde()+'\' AND \"resolutiondate\" <=\''+FechaHasta()+'\' AND "jiraissue".issuestatus in(\'10010\',\'10012\') AND "jiraissue".project='+IdProyecto()
        StringBuffer sb2 = new StringBuffer()
        int i=0
        sql.eachRow(sqlStmt2){
            i++
        }
        String[] solicitud= new String[i];
        String[] sla_id= new String[i];
        String[] description= new String[i];
        int j=0
        sql.eachRow(sqlStmt2){

            solicitud[j]= it.INDICATOR;
            sla_id[j] = it.SLA_ID;
            description[j] = it.ISSUE_KEY;
            j++
        }
        int success=0
        int exceed=0
        int cont_prioridad=0

        for(int k=0;k<solicitud.size();k++){

            if(ExluirSla(description[k])!='Si'){           

                if(solicitud[k]=="SUCCESS"){
                    success++
                }else{
                    exceed++ 
                }
                cont_prioridad++

            }
        }
        sql.close()
        if(cont_prioridad!=0){
            BigDecimal calaculo_success= ((100*success)/cont_prioridad)            
            int calculoIndividual= Integer.valueOf (calaculo_success.intValue())
            return calculoIndividual+"-"+success+"-"+exceed+"-"+cont_prioridad
        }else{
            return "0-0-0-0-0"
        }
        
    }else{
        
        //rechazadas
        if(indicador=="Incidencias Rechazadas"){
            //Servicio calidad de entrega
            int rechazos= rechazos()            
            int totalIssues= totalIncidencias()
            int totalSinRechazos=(totalIssues-rechazos)	
            int calculoIndividual
            if(totalIssues==0){
                calculoIndividual=100
            }else{
                BigDecimal calaculo_success= ((100*totalSinRechazos)/totalIssues)
            	calculoIndividual= Integer.valueOf (calaculo_success.intValue())
            }            
            return calculoIndividual+"-"+totalSinRechazos+"-"+rechazos+"-"+totalIssues
        }
        //Enlazadas
        if(indicador=="Incidencias Colaterales"){
            //Servicio calidad de entrega
            int Enlazadas= enlazadasKO()           
            int totalIssues= enlazadas()
            int totalSinEnlaces=(totalIssues-Enlazadas)
            int calculoIndividual
            if(totalIssues==0){
                calculoIndividual=100
            }else{
                BigDecimal calaculo_success= ((100*totalSinEnlaces)/totalIssues)
            	calculoIndividual= Integer.valueOf (calaculo_success.intValue())
            }            
            return calculoIndividual+"-"+totalSinEnlaces+"-"+Enlazadas+"-"+totalIssues           
        }
        // por defecto retorna 0
        return "0-0-0-0"
        
    }
}
public String IdProyecto(){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name= "Saras"');
    def project
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){            
            project = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17401);
            project = project.getObjectAttributeValueBeans()[0];           
         }
        return project.getValue()
    }else{
        //retorno el id predertminado del entorno de prueba
       return "11307"
    }
   
    
}
//Nombre del fichero
public String NombreFichero(){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name= "Saras"');
    def fichero
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){            
            fichero = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17402);
            fichero = fichero.getObjectAttributeValueBeans()[0];           
         }
        return fichero.getValue()
    }else{
        //retorno un nombre predeterminado
       return "LaboratoriosKinSlaGlobal"
    }  
}
public String ExluirSla(String key){
    
    def ComponentIssue= ComponentAccessor.getIssueManager()
    def customFieldManager = ComponentAccessor.getCustomFieldManager()
    def excluir = customFieldManager.getCustomFieldObject(12735)
    
    Issue issue= ComponentIssue.getIssueObject(key)
    
    return issue.getCustomFieldValue(excluir).toString()
    
    
}

static void uploadAttachment(String FechaDesde,String FechaHasta, String Nombre){    

    ApplicationUser user = ComponentAccessor.getUserManager().getUserByName("admin")
    def issue = ComponentAccessor.getIssueManager().getIssueObject("PRUEBIQ-9239")
    File archivo = new File("/var/atlassian/application-data/jira/export/"+Nombre+"_"+FechaDesde+"_"+FechaHasta+".csv")
    def attachmentManager = ComponentAccessor.getAttachmentManager()
    def bean = new CreateAttachmentParamsBean.Builder().copySourceFile(true)
    .file(archivo)
    .filename(archivo.name)
    .author(user)
    .issue(issue)
    .build()
    attachmentManager.createAttachment(bean)
}
public String FechaDesde(){
    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
    String fechaDesde= dateFormat.format(date)+"-01"
    return fechaDesde
}
public String FechaHasta(){
    Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
    String fechaHasta= dateFormat.format(date)+"-"+day()
    return fechaHasta
}
public String day(){

	Date date = new Date();
    DateFormat dateFormat = new SimpleDateFormat("MM");
    def dia =dateFormat.format(date)
    
    if(dia=='01'){
        return '31'
    }
    if(dia=='02'){
        return '28'
    }
    if(dia=='03'){
        return '31'
    }
    if(dia=='04'){
        return '30'
    }
    if(dia=='05'){
        return '31'
    }
    if(dia=='06'){
        return '30'
    }
    if(dia=='07'){
        return '31'
    }
    if(dia=='08'){
        return '31'
    }
    if(dia=='09'){
        return '30'
    }
    if(dia=='10'){
        return '31'
    }
    if(dia=='11'){
        return '30'
    }
    if(dia=='12'){
        return '31'
    }
    return '30'
}
public int rechazos(){

	final String jqlSearch = "project = 'SARAS - AMS SAP'  AND Rechazos > 0 and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND status in (Confirmada, Cancelada)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}
public int totalIncidencias(){
	
	final String jqlSearch = "project = 'SARAS - AMS SAP' and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND status in (Confirmada, Cancelada)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}

public int enlazadas(){

	final String jqlSearch = "project = 'SARAS - AMS SAP' and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND issuetype in (Correctivo,'Evolutivo Menor','Evolutivo Mayor') AND status in (Confirmada, Cancelada)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}

public int enlazadasKO(){

	final String jqlSearch = "project = 'SARAS - AMS SAP' and resolved > startOfMonth() and resolved <=endOfMonth() and issuetype not in subtaskIssueTypes() AND issuetype in (Correctivo,'Evolutivo Menor','Evolutivo Mayor') AND status in (Confirmada, Cancelada) AND issueLinkType in (causes)"
    def user = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def searchService = ComponentAccessor.getComponentOfType(SearchService)
    SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlSearch)
    def ComponentIssue= ComponentAccessor.getIssueManager()
    int entero=0
    if (parseResult.isValid()) {
        try {
            def results = searchService.search(user, parseResult.query, PagerFilter.unlimitedFilter)
            def issues = results.results
            issues.each {
                Issue issue= ComponentIssue.getIssueObject(it.key)     
                entero++
            }
            return entero
        } catch (SearchException e) {
            return 0
        }
    } else {
        
        return 0
    }

}
public String RutaConfluence(){
    
    @PluginModule ObjectFacade objectFacade
	@WithPlugin("com.riadalabs.jira.plugins.insight")
	@PluginModule IQLFacade iqlFacade
    
    iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade.class);
    def resultados = iqlFacade.findObjectsByIQLAndSchema(1, 'objectType = SlaGlobal and Name= "Saras"');
    def fichero
    if(resultados != null){        
        for(int i=0;i<resultados.size();i++){            
            fichero = objectFacade.loadObjectAttributeBean(resultados[i].getId(), 17400);
            fichero = fichero.getObjectAttributeValueBeans()[0];           
         }
        return fichero.getValue()
    }else{
        //retorno un nombre predeterminado
       return "1933507"
    }  
}