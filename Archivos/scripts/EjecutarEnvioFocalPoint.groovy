import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser
import java.nio.file.Files
import java.nio.file.Paths
import com.atlassian.jira.issue.attachment.CreateAttachmentParamsBean
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.DecimalFormat;
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.user.util.*;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.issue.search.SearchException
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.jira.issue.index.IssueIndexingService
//import com.atlassian.jira.issue.Issue
import com.atlassian.jira.util.ImportUtils

UserManager userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");
def issueManager = ComponentAccessor.getIssueManager()
def Focal= ComponentAccessor.getCustomFieldManager().getCustomFieldObject('customfield_13004');    
def issue = event.issue

def optionsManager = ComponentAccessor.getOptionsManager()
def fieldConfig = Focal.getRelevantConfig(issue)
def options = optionsManager.getOptions(fieldConfig)
def option = options.find {it.value == "SI"}
issue.setCustomFieldValue(Focal,option);    
ComponentAccessor.getIssueManager().updateIssue(admin, issue, EventDispatchOption.DO_NOT_DISPATCH, false);
log.warn("Se cambia el valor")

//Reindexo la issue
def issueIndexingService = ComponentAccessor.getComponent(IssueIndexingService)
boolean wasIndexing = ImportUtils.isIndexIssues();
ImportUtils.setIndexIssues(true);        
issueIndexingService.reIndex(issueManager.getIssueObject(issue.id));
ImportUtils.setIndexIssues(wasIndexing);
log.warn("Se reindexa "+issue.id)
log.warn("Se reindexa "+issue)
