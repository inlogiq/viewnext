import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.atlassian.servicedesk.api.requesttype.RequestTypeService
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.project.Project
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.link.IssueLinkType
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.link.LinkCollection
import com.atlassian.jira.issue.link.IssueLink;
@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade
@WithPlugin("com.atlassian.servicedesk")
import static java.lang.Math.*
import java.sql.Timestamp
import java.text.SimpleDateFormat;
import java.util.Date;
def missue = (MutableIssue) issue

def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def cfSeguimiento = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Seguimiento")

def cfSeguimientoSC = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(14315)

//def seguimiento = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Añadir Seguimiento").getValue(issue).toString()  14314
def seguimiento = issue.getCustomFieldValue(cfSeguimientoSC)
log.warn("el mensaje es" + seguimiento)
def valorSeguimiento = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Seguimiento").getValue(issue)
def texto
def today = new java.sql.Timestamp(new Date().getTime())
log.warn(today)
SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
def fecha = sdf3.format(today).toString()

if(valorSeguimiento != null){
    texto = valorSeguimiento+ "\n"+ fecha +" - "+seguimiento
}else{
    texto = fecha+" - "+seguimiento  
}
missue.setCustomFieldValue(cfSeguimiento, texto)
missue.setCustomFieldValue(cfSeguimientoSC, null)


ComponentAccessor.getIssueManager().updateIssue(user, missue, EventDispatchOption.ISSUE_UPDATED, false)