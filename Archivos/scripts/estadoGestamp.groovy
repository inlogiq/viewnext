import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.project.Project
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.link.IssueLinkType
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.link.LinkCollection
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.MutableIssue


def userManager = ComponentAccessor.getUserManager();
def admin = userManager.getUserByName("admin");
def issueManager = ComponentAccessor.getIssueManager()

def estado = ComponentAccessor.getIssueManager().getIssueObject(issue.getKey()).getStatus().getName()
log.warn(estado)

def estadoCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Estado Gestamp")

def mIssue = (MutableIssue) issue;
//COPIAMOS LOS VALORES
mIssue.setCustomFieldValue(estadoCF , estado);

//ACTUALIZAMOS LA issue
ComponentAccessor.getIssueManager().updateIssue(admin, mIssue, EventDispatchOption.ISSUE_UPDATED, false)
log.warn("se ha cambiado el valor " + estado) 



