import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.component.ComponentAccessor
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.onresolve.scriptrunner.runner.customisers.WithPlugin;
import com.onresolve.scriptrunner.runner.customisers.PluginModule;
import com.atlassian.jira.event.type.EventDispatchOption;

@WithPlugin("com.riadalabs.jira.plugins.insight")
@PluginModule ObjectFacade objectFacade
@PluginModule IQLFacade iqlFacade

def user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()

CustomField cfSistema = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("Sistema")[0]
CustomField cfNorden = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("Nº Orden")[0]
CustomField cfDesc = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("Descripción OT")[0]
CustomField cfOrdenes = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName("Órdenes de transporte")[0]

def sistemaObject = cfSistema.getValue(issue)
String sistema = objectFacade.loadObjectAttributeBean(sistemaObject[0].getId(), 192).getObjectAttributeValueBeans()[0].getValue()
String norden = cfNorden.getValue(issue)
String desc = cfDesc.getValue(issue)
String ordenes = cfOrdenes.getValue(issue)


if(ordenes){
    ordenes = ordenes+ "\n"+sistema+norden+" - "+desc 
}else{
    ordenes = sistema+norden+" - "+desc 
}

issue.setCustomFieldValue(cfOrdenes, ordenes)
issue.setCustomFieldValue(cfSistema, null)
issue.setCustomFieldValue(cfNorden, null)
issue.setCustomFieldValue(cfDesc, null)

ComponentAccessor.getIssueManager().updateIssue(user, issue, EventDispatchOption.ISSUE_UPDATED, false)

